import {Observable} from 'rxjs';
import {nowStr} from './functions';

/**
 * Ответ от сервера приходит обернытым в такой объект
 */
export interface JsonResponse<T> {
    /**
     * Успешный или нет ответ
     * false - произошла какая-то ошибка
     * true - все нормально
     */
    status: boolean;
    /**
     * Результат в случае когда status == true
     * Описание ошибки когда status == false
     */
    data: T;
}

/**
 * Интерфейс для объектов где требуется спросить о желании сохранить данные
 * при переходе на другой роутер
 */
export interface CanComponentDeactivate {
    canDeactivate(): Observable<boolean>;
}

/**
 * Справочники
 */
export interface Dict {
    value: string | number;
    label: string;
}

/**
 * Словарь с сокращенным значением
 */
export interface DictS extends Dict {
    short: string;
}

/**
 * Словарь с названием и инедтификатором организации
 */
export interface DictOrg extends Dict {
    orgId: string;
    orgName: string;
}

/**
 * Словарь для адреса
 */
export interface AddressDict extends DictS {
    /**
     * Название адресного объекта
     */
    type: string;
    /**
     * Идентификатор уровня
     */
    levelId: string;
    /**
     * Название уровня
     */
    level: string;
    /**
     * Порядковый номер типа
     */
    order: number;
    /**
     * Дочерний ардес
     * используется дла дома (корпус, строение)
     */
    child?: AddressDict;
    /**
     * Если true - значить менять его нельзя в выборе адреса
     */
    nochg?: boolean;
}

/**
 * Описание полного адреса, как квалифицированного так и нет
 */
export interface Address {
    /**
     * Уровни адреса, например 'Страна' - 'Россия', 'Область' - 'Земляничная'
     */
    levels: AddressDict[];
    /**
     * идентификатор страны
     */
    countryId?: string;
    /**
     * название страны для отображения
     */
    country?: string;
    /**
     * Страна для неклассифицированного ардеса
     */
    countryText?: string;
    /**
     * адрес для неклассифицированного адреса
     */
    addressText?: string;
    /**
     * Номера квартир
     */
    flat?: string;
}

/**
 * Адрес регистрации или фактический адрес
 */
export interface DwellingPlace extends Address {
    id?: string;

    /**
     * Номера комнат
     */
    room?: string;

    /**
     * Тип регистрации
     */
    regType?: string;
    /**
     * Идентификатор регистрации
     */
    regId?: string;
    /**
     * Тип адреса для отображения в таблице
     */
    regTypeStr?: string;

    /**
     * Тип адреса
     */
    addressType?: string;

    /**
     *  Дата начала регистрации
     */
    startDate?: string;

    /**
     *  Дата оканчания регистрации (для 'Регистрация по месту пребывания')
     */
    endDate?: string;

    /**
     *  Дата убытия (только для отображения в таблице)
     */
    eventDate?: string;

    /**
     * Причина прибытия
     */
    arrivalCauseId?: string;

    /**
     * Основание проживание
     */
    lifeGroundId?: string;

    /**
     * Дата собственника
     */
    ownerDate?: string;

    /**
     * Доля собстенника числитель
     */
    numerator?: number;

    /**
     * Доля собстенника знаменатель
     */
    denominator?: number;
}

/**
 * Определение разлиных свойств кнопки в навигации окна поиска и в различных реестрах
 * custody-nav а так же на главной странице
 */
export interface Action {
    title: string;
    link: string;
    clazz: string;
    modal?: boolean;
    main?: boolean;
    /**
     * Возможен ли переход без id
     */
    woid?: boolean;
    /**
     * Внешняя ссылка
     */
    external?: boolean;
    flink?: (params?: any) => string;

    queryParams?: { [key: string]: any };
    /**
     * Хвост ссылки, после id
     */
    tail?: string;
}

/**
 * Действия для левого меню
 */
export interface MenuAction {
    /**
     * путь
     */
    url: string | string[];

    /**
     * название
     */
    title: string;

    /**
     * если true то этот пункт будет выделен
     * по умолчанию, для тех случаев, когда перешли не по конкретному адресу, а по общему
     */
    default?: boolean;

    /**
     * Разрешается переход только если есть общий id
     */
    withIdOnly?: boolean;

    /**
     * Можно переходит без id, при этом общий id при постоении конечного пути использоваться не будет
     */
    withoutId?: boolean;

    /**
     * Если есть, то вызывается этот метод перед переходом, если вернул true, то осуществляется переход,
     * если не true, то ничего не делается
     * Иногда нужна проверка перед тем как перейти по определенному пути
     * если возвращаем Observable то нужно следить за тем чтобы это был Observable с единичным результатом,
     * например, ответ от сервера, в принимающей стороне не делается никаких проверок
     */
    beforeNav?: () => boolean | Observable<boolean>;

    /**
     * Дополнительные параметры
     */
    queryParams?: { [key: string]: any };

    /**
     * Часть хэш пути, для прокручивания страницы до определенного маркера
     */
    fragment?: string;

    /**
     * При построении пути добавляется в конец
     */
    tail?: string;
}

/**
 * Левое меню
 */
export interface Menu {
    /**
     * Название меню
     */
    title: string;
    /**
     * Действия, пункты меню
     */
    actions: MenuAction[];
}

/**
 * Базовый функционал для всех фильтров, используемых в реестрах
 */
export abstract class AbstractFilter<T> {
    // $ сзади - это поле не посылаем на сервер
    readonly kind$: T;

    protected init(data?: any) {
        if (data) {
            const initProps = Object.getOwnPropertyNames(data);
            // Чтобы это работало нужно чтобы все поля были инициализированы
            Object.keys(this).forEach(it => {
                if (initProps.indexOf(it) > -1) {
                    this[it] = data[it];
                }
            });
        }
    }
}

/**
 * Родственное отношение в форме enum
 */
export enum BioRole {
    Mother = 'MOTHER',
    Father = 'FATHER',
    Sister = 'SISTER',
    Brother = 'BROTHER',
    Uncle = 'UNCLE',
    Aunt = 'AUNT',
    Grandma = 'GRANDMA',
    Grandpa = 'GRANDPA'
}

/**
 * Описание кнопки в верхнем меню
 */
export interface Button {
    title: string;
    clazz?: string;
    mclazz?: string;
    click?: () => void;
    menu?: Button[];
}

/**
 * Возможные типы кнопок верхего меню
 */
export enum ButtonType {
    Add = 'add',
    Cancel = 'undo',
    Delete = 'delete',
    Edit = 'edit',
    Print = 'print',
    Save = 'save',
    Home = 'home',
    Exit = 'exit',
    LastTen = 'last',
    Search = 'search'
}

/**
 * Данные для отображения строки в меню перехода в ЛД одного из последних 10-и просмотренных
 */
export interface LastPf {
    title: string;
    id: string;
}

/**
 * Поля, необходимые для перехода в карточку учета
 */
export interface RegFields {
    /**
     * Идентификатор личности
     */
    id?: string;

    /**
     * День рождения
     */
    birthDate?: string;
}

/**
 * Данные для отображения в таблице найденных Личных дел
 */
export interface LdTableRow extends RegFields {
    fio?: string;
    sex?: string;
    deathDate?: string;
    documentType?: string;
    seria?: string;
    number?: string;
    role?: BioRole;
    actual?: number;
    snils?: string;
    factAdr?: string;
    regAdr?: string;

    /**
     * Ребенок или нет
     */
    kind?: boolean;
}

export interface Relation extends LdTableRow {
    /**
     * идентификатор члена семьи (familymemberid из family_members)
     */
    memberId?: string;

    /**
     * Родственное отношение
     */
    role: BioRole;
    roleId?: string;

    /**
     * Дата окончания родственного отношения
     */
    endDate?: string;
}

/**
 * Типы учета
 */
export enum RegisteredType {
    Underage = 'UNDERAGE_REG', // Учет несовершеннолетних
    Patron = 'PATRON_REG', // Учет нуждающихся в патронаже
    Incapable = 'INCAPABLE_REG' // Учет совершеннолетних недееспособных
}

/**
 * Фильтр для поиска по различным учетам
 */
export class RegFilter {
    /**
     * Дата постановки 'с'
     */
    startRegDate: string;

    /**
     * Дата постановки 'по'
     */
    endRegDate: string;

    /**
     * Дата снятия 'с'
     */
    startUnregDate: string;

    /**
     * Дата снятия 'по'
     */
    endUnregDate: string;

    /**
     * Дата рождения 'с'
     */
    startBirthDate: string;

    /**
     * Дата рождения 'по'
     */
    endBirthDate: string;

    /**
     * Фамилия
     */
    lastName: string;

    /**
     * Имя
     */
    firstName: string;

    /**
     * Отчество
     */
    middleName: string;

    /**
     * Причина постановки на учет
     */
    regReasonId: string;

    /**
     * Причина снятия с учета
     */
    unregReasonId: string;

    /**
     * Номер личного дела
     */
    registerNum: string;

    /**
     * Пол
     */
    sex: number;

    /**
     * ООП, поставивший на учет ребенка
     */
    guardBodyId: string;

    /**
     * Показывать всех или нет
     * null - всех
     * true - только Состоящих на учете
     * false - только Снятых с учета
     */
    registered = '';

    /**
     * Тип учета
     */
    type: RegisteredType;

    /**
     * Актуальная форма устройства ребенка
     */
    curEventIds: string[];

    /**
     * Актуальное движение (для детей не используется)
     */
    curEvent: string;

    constructor(type: RegisteredType, data?: any) {
        this.type = type;
        if (data && typeof data === 'object') {
            for (const o in data) {
                if (data.hasOwnProperty(o)) {
                    this[o] = data[o];
                }
            }
        }
    }

    /**
     * Возвращает true если есть данные, с помощью которых можно поставить на учет
     */
    canRegister() {
        return this.lastName || this.firstName || this.middleName || this.startBirthDate || this.endBirthDate;
    }
}

/**
 * Фильтр для поиска личных дел
 */
export class LdFilter {
    birthDate: string;
    lastName: string;
    firstName: string;
    middleName: string;
    docNumber: string;
    snils?: string;
    relativeId: BioRole;
    /**
     * Используется когда родственное отношение выражено id а не константой
     */
    relationId: string;
    child: boolean;
    actual = true;
    sex?: number;
    /**
     * Поиск среди тех, кто поставлен на учет
     */
    registered?: RegisteredType;

    /**
     * Не стоит на учете
     */
    noreg = false;

    /**
     * Поиск из обращения при подборе
     */
    plea?: boolean;

    /**
     * Поиск опекунов для опекаемого
     */
    caredId: string;

    /**
     *  Исключить недееспособных
     */
    capable: boolean;

    constructor(ld?: LdTableRow, relativeId?: BioRole, child?: boolean, reg?: RegFilter, sex?: number, registered?: RegisteredType,
                plea?: boolean, caredId?: string, capable?: boolean) {
        if (ld) {
            const names = ld.fio.split(' ');
            this.lastName = names[0];
            this.firstName = names[1];
            this.middleName = names[2];
            this.docNumber = ld.number;
            this.snils = ld.snils;
            this.birthDate = ld.birthDate;
        }
        if (reg) {
            this.lastName = reg.lastName;
            this.firstName = reg.firstName;
            this.middleName = reg.middleName;
            this.birthDate = reg.startBirthDate || reg.endBirthDate;
        }
        if (relativeId) {
            if (Object.keys(BioRole).some(k => BioRole[k] === relativeId)) {
                this.relativeId = relativeId;
            } else {
                this.relationId = relativeId;
            }
        }
        if (!isNaN(sex)) {
            this.sex = sex;
        }
        if (registered) {
            this.registered = registered;
        }
        this.plea = plea;
        this.caredId = caredId;
        this.child = child;
        this.capable = capable;
    }

    copy(): LdFilter {
        const res = new LdFilter();
        const s = this;
        res.birthDate = s.birthDate;
        res.lastName = s.lastName;
        res.firstName = s.firstName;
        res.middleName = s.middleName;
        res.docNumber = s.docNumber;
        res.relativeId = s.relativeId;
        res.relationId = s.relationId;
        res.child = s.child;
        res.plea = s.plea;
        res.caredId = s.caredId;
        res.sex = s.sex;
        res.registered = s.registered;
        res.snils = s.snils;
        res.noreg = s.noreg;
        res.capable = s.capable;
        return res;
    }

    notEmpty() {
        const s = this;
        return s.birthDate || s.lastName || s.firstName || s.middleName || s.docNumber || s.relativeId || s.relationId || s.snils;
    }
}

/**
 * Страница для постраничного вывода найденных данных
 */
export interface Page<T> {
    /**
     * Найденные данные
     */
    data: T[];

    /**
     * Максимально допустимый размер на странице
     */
    size: number;

    /**
     * Всего найдено
     */
    count: number;

    /**
     * Номер страницы, начинается с 1
     */
    page: number;

    /**
     * Кол-во страниц
     */
    pages: number;

    /**
     * Первая страница
     */
    first: boolean;

    /**
     * Последняя страница
     */
    last: boolean;
}

/**
 * Пути для роутинга
 */
export enum Path {
    IncRegistered = 'incompetent-registry',
    IncCitizens = 'incompetent-citizens',
    CandidateRegistry = 'candidate-registry',
    AppealsRegistry = 'appeals-registry',
    FamilyRegistry = 'family-registry',
    FamilyArrangements = 'family-arrangements',
    FamilyArrangement = 'family-arrangement',
    BioFamily = 'biological-family',
    Reports = 'reporting-forms',
    PremisesRegistry = 'premises-registry',
    Premises = 'premises',
    RegistryLists = 'registry-lists',
    RegistryNpa = 'registry-npa',
    Npa = 'npa',
    PfManInfo = 'man-info',
    PfRegistration = 'registration',
    PfZags = 'zags',
    PfDocChange = 'changes-document',
    PfInvInc = 'invalid-incapable',
    PfAddReq = 'added-req',
    RegRegistration = 'registration',
    RegRelations = 'relations',
    RegAddInfo = 'add-info',
    RegResources = 'resources',
    RegReports = 'reports',
    RegProperty = 'property',

    PlAddons = 'addons',
    PlSubject = 'subject',
    PlDecision = 'decision',
    PlSmev = 'smev',
    PlTravel = 'travel',
    PlGp = 'livearea',
    PlContact = 'contacts',

    OpekaAct = 'act',

    IncExams = 'inc-exams',
    CareOrg = 'careorg',
    RegistryCareOrg = 'registry-careorg',

    Main = 'main',
    Count = 'count'
}

/**
 * Типы обращений
 */
export enum PleaType {
    Underage = 'UNDERAGE_PLEA', // Опека над несовершеннолетним
    Adult = 'ADULT_PLEA', // Опека над совершеннолетним
    AssignAssistant = 'ASSIGN_ASSISTANT', // Назначение помощника
    Travel = 'TRAVEL_PLEA' // Предоставление выплаты
}


/**
 * Строка в таблице найденных детей в Реестре детей, поставленных на учет
 */
export interface RegChildTableRow extends RegFields {
    /**
     * id - systemmanId ребенка
     */

    /**
     * Фамилия
     */
    lastName: string;

    /**
     * Имя
     */
    firstName: string;

    /**
     * Отчество
     */
    middleName: string;

    /**
     * Пол
     */
    sex: string;

    /**
     * Номер личного дела
     */
    registerNum: string;

    /**
     * Дата постановки
     */
    regDate: string;

    /**
     * Причина постановки на учет
     */
    regReason: string;

    /**
     * Актуальное движение
     * Движение с максимальным номером без даты окончания
     */
    actualEvent: string;

    /**
     * Дата снятия с учета
     */
    unregDate: string;

    /**
     * Причина снятия с учета
     */
    unregReason: string;

    /**
     * ООП, поставивший на учет ребенка
     */
    guardBody: string;

    /**
     * Движение
     */
    events: TutEvent[];

    /**
     * Есть открытая постановка на учёт (не снят с учёта)
     */
    regOnly: boolean;
}

/**
 * Для отображения движения в таблице
 */
export interface TutEvent {
    /**
     * идентификатор движения tutelageeventid из таблицы tutelageevents
     */
    id: string;

    /**
     * Номер
     */
    number: number;

    /**
     * Наименование типа
     */
    typeName: string;
    typeId: string;

    /**
     * Причина
     */
    reason: string;
    /**
     * Дата начала
     */
    startDate: string;
    /**
     * Дата окончания
     */
    endDate: string;
    /**
     * Сотрудник
     */
    official: string;

    /**
     * Есть ли устройство в организацию
     */
    hasArOrg: boolean;

    // Для движения семейного устройства
    /**
     * Идентификатор семейного устройства
     */
    suId: number;

    /**
     * Идентификатор семьи опеки
     */
    fid: string;

    /**
     * Движение создано организацией текущего пользователя или дочерней
     */
    myEvent: boolean;
}

/**
 * Типы движений
 */
export enum TutEventType {
    Register = 'REG_TUTEV', // Постановка на первичный учет
    Unregister = 'UNREG_TUTEV', // Снятие с учета
    SocMonitStart = 'MONITOR_START', // Начало социального мониторинга
    SocMonitEnd = 'MONITOR_END', // Окончание социального мониторинга
    Preopeka = 'PREOPEKA_TUTEV', // Предварительная опека (попечительство)
    OpekunOOiP = 'SUPERVISION_TUTEV' // Назначение опекуном ООиП
}

/**
 * Значение ролей пользователей
 */
export enum OpekaRole {
    Specialist = '8da02404-413a-11eb-8b60-3c970e170448', // Специалист
    MainSpecialist = 'f11ef35e-889c-11ea-b0df-c0b6f966a800', // Главный Специалист
    Administrator = '54ddd008-0d00-11eb-ac5b-c8d9d2854ef7', // Администратор
    Razrab = 'f11ef232-889c-11ea-b0df-c0b6f966a800' // Разработчик
}

export enum BddsopRole {
    Administrator = 'ab794f0c-4066-11eb-ada2-00155d02221d', // Администратор
    Razrab = 'abD9Ef0V-E0L6-O1Pb-EdR2-00155d02221d' // Разработчик
}

export enum UrnRole {
    Administrator = '855fec84-b06c-11ea-87c1-00155d02221d', // Администратор
    Razrab = '855fda78-b06c-11ea-87c1-00155d02221d' // Разработчик
}

/**
 * Направление сортировки
 */
export type SortDirection = 'asc' | 'desc' | '';

/**
 * Сортировка
 */
export interface SortEvent {
    column: string;
    direction: SortDirection;
}

/**
 * Отображение результатов поиска в дереве
 */
export interface TreeNode<T> {
    /**
     * Идентификатор
     */
    id?: string;

    /**
     * Идентификатор родителя
     */
    parentId?: string;

    value: string;

    /**
     * Потомки
     */
    children?: T[];

    /**
     * Есть ли потомки
     */
    hasChildren?: boolean;
}

export interface TreeNodeImpl extends TreeNode<TreeNodeImpl> {

}

/**
 * Пользователь системы
 */
export interface User {
    /**
     * Идентификатор из таблицы userlinks
     */
    id: string;
    fio: string;
    aisId: number;
    position: string;
    /**
     * Поле printname из таблицы organisations
     */
    orgName: string;
    orgType?: string;
    orgShort?: string;

    /**
     * Идентификатор организации пользователя
     */
    orgId?: string;

    /**
     * Идентификатор из таблицы roles
     */
    roleId: string;
    email?: string;
    phone?: string;

    /**
     * Логин пользоватотеля (loginname из таблицы users)
     */
    login?: string;
}

export interface BioFamily {
    children: LdTableRow[];
    relatives: LdTableRow[];
    relations: Relation[];
}


export interface FamilyMember {
    systemmanId: string;
    fio: string;
    birthDate: string;
    role: BioRole;
    roleView$: Observable<string>;
}

/**
 * Объект ребенка БС
 */
export interface MainMan extends LdTableRow {
    /**
     * Братья, сестры (Несовершеннолетние) Прямые связи в базе
     */
    sibs: Relation[];

    /**
     * Родители
     */
    parents: Relation[];

    /**
     * Совершеннолетние родственники
     */
    rels: Relation[];
}

export interface Parent extends Relation {
    /**
     * Родственное отношение
     */
    role: BioRole.Father | BioRole.Mother;
}

export interface Document {
    /**
     * Идентификатор документа
     */
    id?: string;
    /**
     * Идентификатор типа документа
     */
    typeId: string;
    name: string;
    /**
     * Серия
     */
    series?: string;
    /**
     * Номер
     */
    number?: string;
    /**
     * Дата выдачи
     */
    issueDate?: string;
    /**
     * Дата начала действия
     */
    startAction?: string;
    /**
     * Кем выдан
     */
    whogive?: string;

    /**
     * Код подразделения
     */
    depCode?: string;

    /**
     * Примечание, Причина замены
     */
    note?: string;

    /**
     * Имя файла, прикрепленного к документу
     */
    fileName?: string;

    active?: number;
}

// Детальная информация о недееспособности
export interface IncapableDetails {
    /**
     * ID записи о недееспособности
     */
    disabilityId: string;

    /**
     * Дата установки
     */
    incDate: string;

    /**
     *  Документ устанавливающий недееспособность
     */
    incDoc: Document;

    /**
     * Описание
     */
    incDesc: string;
}

// Детальная информация о инвалидности
export interface InvalidDetails {
    /**
     * ID записи о инвалидности
     */
    invalidityId: string;

    /**
     * Дата установки
     */
    invDate: string;

    /**
     * Группа инвалидности
     */
    invalidGroupId: string;

    /**
     * Причина инвалидности
     */
    causeId: string;

    /**
     * Документ, устанавливающий инвалидность
     */
    invDoc: Document;
}

export class Training {
    id: number;

    /**
     * Место учебы
     */
    placeId: string;
    place: string;

    /**
     * Вид образования
     */
    kindId: string;

    /**
     * Класс / курс
     */
    course: string;

    /**
     * Специальность
     */
    beruf: string;

    /**
     * Дата начала
     */
    startDate: string;

    /**
     * Дата окончания
     */
    endDate: string;

    constructor() {
        this.startDate = nowStr();
    }
}

/**
 * Электронный запрос СМЭВ
 */
export class SmevRequest {
    /**
     * Идентификатор в таблице smev_plea
     */
    id: number;

    /**
     * Вид сведений, запрос из справочника smev с видом взаимодействия "Электронное"
     */
    smevId: number;
    smevName: string;

    /**
     * Человек на которого отправлен запрос
     */
    manId: string;

    /**
     * Код запроса
     */
    code: string;

    /**
     * Организация
     */
    orgName: string;
    orgId: string;

    /**
     * Дата запроса
     */
    reqDate: string;

    /**
     * Дата ответа
     */
    ansDate: string;

    /**
     * Статус запроса
     */
    statusId: string;

    /**
     * Имя файла, скана документа
     */
    fileName: string;

    /**
     * Текст запроса
     */
    request: string;

    /**
     * Текст ответа
     */
    answer: string;

    constructor(smevId: number, smevName: string, orgId: string, orgName: string) {
        this.smevId = smevId;
        this.smevName = smevName;
        this.orgId = orgId;
        this.orgName = orgName;
    }
}

