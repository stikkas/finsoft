import {ChangeDetectionStrategy, Component, ViewEncapsulation} from '@angular/core';
import {timer} from 'rxjs';
import {map} from 'rxjs/operators';
import {MeService} from '../services/me.service';

@Component({
    selector: 'fi-footer',
    templateUrl: './footer.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent {
    user$ = this.me.getUser();
    days = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
    months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
    now$ = timer(0, 120000).pipe(map(_ => new Date()));

    constructor(private me: MeService) {
    }

    getMinutes(date: Date): string {
        return date ? this.getTime(date.getMinutes()) : '';
    }

    getHours(date: Date): string {
        return date ? this.getTime(date.getHours()) : '';
    }

    private getTime(t: number): string {
        return (t < 10 ? '0' : '') + t;
    }
}

