import {Injectable} from '@angular/core';
import {Address, JsonResponse, TreeNodeImpl} from '../models';
import {urls} from '../utils';
import {Responsable} from './responsable';
import {HttpClient} from '@angular/common/http';
import {NotifierService} from 'angular-notifier';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AdminSearchService extends Responsable {
    private readonly orgUrl: string;
    private readonly clsUrl: string;
    private readonly rootAddressUrl: string;

    constructor(private http: HttpClient, ns: NotifierService) {
        super(ns);
        this.orgUrl = `${urls.adminApi}${urls.api.admin.orgs}`;
        this.clsUrl = `${urls.adminApi}${urls.api.admin.cls}`;
        this.rootAddressUrl = `${urls.adminApi}${urls.api.admin.rootAdr}`;
    }

    findOrganisations(name: string, parentId: string, params: any) {
        return this.findTree(this.orgUrl, name, parentId, null, null, params);
    }

    findClassifiers(name?: string, parentId?: string, ids?: string[], excluded?: string[]) {
        return this.findTree(this.clsUrl, name, parentId, ids, excluded);
    }

    private findTree(url: string, name: string, parentId: string, ids?: string[], excluded?: string[], orgParams?: any) {
        const params = {} as any;
        if (name) {
            params.name = name;
        }
        if (parentId) {
            params.parentId = parentId;
        }
        if (ids) {
            params.parentIds = ids;
        }
        if (excluded) {
            params.excluded = excluded;
        }
        if (orgParams) {
            for (const o in orgParams) {
                if (orgParams[o]) {
                    params[o] = orgParams[o];
                }
            }
        }
        return this.handle(this.http.get<JsonResponse<TreeNodeImpl[]>>(url, {params}), false);
    }

    /**
     * Получает корневой адрес организации, которой принадленжит текущий пользователь
     */
    getRootAddress(): Observable<Address> {
        return this.handle(this.http.get<JsonResponse<Address>>(this.rootAddressUrl), false);
    }

}

