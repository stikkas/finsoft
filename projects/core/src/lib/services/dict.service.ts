import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {catchError, first, map} from 'rxjs/operators';
import {Dict} from '../models';
import {context, urls} from '../utils';

/**
 * Сервис для получения справочников
 */
@Injectable({
    providedIn: 'root'
})
export class DictService {
    private baseUrl: string;

    /**
     * справочники
     */
    private dicts = {} as { [key: string]: [BehaviorSubject<Dict[]>, number] };

    constructor(private http: HttpClient) {
        this.baseUrl = `${context.app}${urls.api.dicts.base}`;
        this.createDict('sex', [{value: 0, label: 'Мужской'}, {value: 1, label: 'Женский'}]);
        this.createDict('sexshort', [{value: 0, label: 'М'}, {value: 1, label: 'Ж'}]);
        this.createDict('showreg', [{value: '', label: 'Всех'}, {value: 'true', label: 'Состоящих на учете'},
            {value: 'false', label: 'Снятых с учета'}]);
        this.createDict('showact', [{value: '', label: 'Всех'}, {value: 'true', label: 'Актуальные'},
            {value: 'false', label: 'Неактуальные'}]);
        this.createDict('job', [{value: 0, label: 'Не работает'}, {value: 1, label: 'Работает'}, {value: 2, label: 'Пенсионер'}]);
        this.createDict('form', [{value: 0, label: 'В семье'}, {value: 1, label: 'Организация'}]);
        this.createDict('cntzp', [{value: 1, label: '1'}, {value: 2, label: '2'}]);
    }

    /**
     * Используется в селектора typeahead
     * @param name - название справочника
     * @param query - параметры, ограничивающие возвращаемый результат
     */
    loadOnce(name: string, query?: string | number | {}): Observable<Dict[]> {
        const params = !query ? null : (typeof query === 'object') ? query : {query};
        return this.http.get<Dict[]>(`${this.baseUrl}${name}`, {params}).pipe(catchError(() => of([])));
    }

    /**
     * Получаем справочник, используем кэширование результатов на 1 час
     * @param dict - название справочника
     * @param query - дополнительный параметр
     */
    getDict(dict: string, query?: string | number): Observable<Dict[]> {
        const dictName = query ? `${dict}__${query}` : dict;
        let d = this.dicts[dictName];
        if (!d || (d[1] > 0 && d[1] - Date.now() <= 0)) {
            this.dicts[dictName] = d = [new BehaviorSubject<Dict[]>(null), Date.now() + 3600000];
            const params = !query ? null : {query: `${query}`};
            this.http.get<Dict[]>(`${this.baseUrl}${(dict.startsWith('/') ? '' : '/') + dict}`, {params})
                .subscribe(res => res && d[0].next(res));
        }
        return d[0].asObservable();
    }

    /**
     * Возвращает значение справочника для заданного value
     * @param value - идентификатор
     * @param dict - справочник
     * @param what - по умолчанию label - интересующее нас поле
     */
    getLabel(value: string | number, dict: string, what = 'label', query?: string | number): Observable<string> {
        return this.getDict(dict, query).pipe(first(res => res !== null), map(res => {
            const v = res.find(it => it.value === value);
            return v && v[what];
        }));
    }

    /**
     * Сбрасывает значение справочника
     * При следующем обращении справочник будет запрошен с сервера
     * @param dict - название справочника
     */
    reset(dict: string) {
        delete this.dicts[dict];
    }

    private createDict(key: string, dict: Dict[]) {
        this.dicts[key] = [new BehaviorSubject<Dict[]>(dict), 0];
    }
}


