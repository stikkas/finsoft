import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ViewEditService {
    private viewMode = new BehaviorSubject<boolean>(false);

    constructor() {
        this.viewMode.next(true);
    }

    get viewMode$() {
        return this.viewMode.asObservable();
    }

    view() {
        this.viewMode.next(true);
    }

    edit() {
        this.viewMode.next(false);
    }
}
