import {Injectable} from '@angular/core';
import {NotifierService} from 'angular-notifier';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {Responsable} from './responsable';
import {LoadingService} from './loading.service';
import {JsonResponse, LdFilter, LdTableRow, Page, SortEvent} from '../models';
import {context} from '../utils';

@Injectable({
    providedIn: 'root'
})
export class ShareSearchService extends Responsable {
    ldUrl: string;
    private apiUrl = '/api/search';
    private myAddressUrl: string;
    private pages$ = {} as { [key: string]: Subject<Page<LdTableRow>> };

    constructor(private http: HttpClient, ns: NotifierService, protected ls: LoadingService) {
        super(ns);
        const startUrl = context.app ? context.app.replace('/admin', context.app) : 'http://localhost:4200';
        this.ldUrl = startUrl + this.apiUrl + '/ld';
        this.myAddressUrl = startUrl + this.apiUrl + '/is-my-address';
    }

    findPeople(params: LdFilter, key: string, page: number, sort?: SortEvent) {
        this.handle(
            this.http.get<JsonResponse<Page<LdTableRow>>>(this.ldUrl, {params: this.getParams(params, page, sort)})
        ).subscribe(res => this.pages$[key].next(res));
    }

    getPeople(key: string): Observable<Page<LdTableRow>> {
        if (!this.pages$[key]) {
            this.pages$[key] = new Subject<Page<LdTableRow>>();
        }
        return this.pages$[key].asObservable();
    }

    /**
     * Возвращает true если выбранный адрес принадлежит организации (или одной из дочерней) текущего пользователя
     */
    isMyAddress(id: string): Observable<boolean> {
        return this.handle(this.http.get<JsonResponse<boolean>>(this.myAddressUrl, {params: {id}}));
    }

    /**
     * Очищает резултаты поиска
     * @param searchKey - идентификатор пользователя сервисом
     */
    clear(searchKey: string) {
        const obs = this.pages$[searchKey];
        if (obs) {
            obs.next(null);
        }
    }

    getParams(p: any, page: number, sort: SortEvent, pageSize = 50): any {
        const params = {pageSize, page: page || 1} as any;
        Object.keys(p).forEach(k => {
            if (!k.endsWith('$')) {
                const v = p[k];
                if (v !== undefined && v !== null && v !== '') { // false нам нужен
                    params[k] = v;
                }
            }
        });
        if (sort && sort.direction) {
            params.sortField = sort.column;
            params.desc = sort.direction === 'desc';
        }
        return params;
    }
}



