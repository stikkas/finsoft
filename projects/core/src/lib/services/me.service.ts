import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, ReplaySubject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {NotifierService} from 'angular-notifier';
import {Responsable} from './responsable';
import {context, urls} from '../utils';
import {JsonResponse, LastPf, User} from '../models';

@Injectable({
    providedIn: 'root'
})
export class MeService extends Responsable {
    private baseUrl: string;
    private user$ = new ReplaySubject<User>(1);
    private lastPfs = new BehaviorSubject<LastPf[]>([]);

    constructor(private http: HttpClient, ns: NotifierService) {
        super(ns);
        // Когда нет aisUrl - значит вызов из самого приложения, тогда пользуемся контекстом
        this.baseUrl = context.aisUrl || context.app;
        this.load();
    }

    load() {
        this.http.get(`${context.app}${urls.ui}`).subscribe((res: User) => this.user$.next(res));
    }

    getUser(): Observable<User> {
        return this.user$.asObservable();
    }
  
    get lastT$() {
        return this.lastPfs.asObservable();
    }

    getLastT() {
        this.handle(this.http.get<JsonResponse<LastPf[]>>(`${this.baseUrl}/api/me/last-ten`), false)
            .subscribe(res => {
                if (res) {
                    this.lastPfs.next(res);
                }
            });
    }
}
