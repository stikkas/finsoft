import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {JsonResponse, MenuAction} from '../models';
import {Responsable} from './responsable';
import {NotifierService} from 'angular-notifier';
import {context} from '../utils';

/**
 * Сервис для сохранения промежуточных данных на сервере или локально
 * для долнейшего использования их в определенных компонентах.
 * Данные сохраняются перед переходом по роуту.
 * После сохраненения возвращается число (идентификатор), по которому можно эти данные получить.
 */
@Injectable({
    providedIn: 'root'
})
export class RouteParamsService extends Responsable {
    private static id = 0;
    private ctx: string;
    history: MenuAction;

    private queryParams = {} as { [key: number]: any };

    constructor(private http: HttpClient, ns: NotifierService) {
        super(ns);
        this.ctx = context.aisUrl || context.app;
    }

    getParams(id: number) {
        const data = this.queryParams[id];
        delete this.queryParams[id];
        return data;
    }

    setParams(value: any) {
        const id = ++RouteParamsService.id;
        this.queryParams[id] = {...value};
        return id;
    }

    /**
     * Открывает заданный путь в новой вкладке. Перед открытием, сохраняет переданные параметры на сервере.
     * Если произошла ошибка при сохранении, то не переходим по пути.
     * @param path - путь, который нужно открыть, ему передается дополнительный параметр - chid номер, под которым сохранили данные
     * @param apiPath - путь, по которому нужно сохранить параметры
     * @param params - параметры, которые нужно сохранить
     */
    openNewTab(path: string, apiPath: string, params: any) {
        this.handle(
            this.http.post<JsonResponse<number>>(this.ctx + apiPath, params), false
        ).subscribe(res => {
            if (res || res === 0) {
                window.open(`${this.ctx}${path}?chid=${res}`, '_blank');
            }
        });
    }
}

