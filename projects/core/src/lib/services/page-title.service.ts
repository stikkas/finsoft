import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PageTitleService {
    private title$ = new BehaviorSubject<string>('');
    private clazz$ = new BehaviorSubject<string>('');

    get title() {
        return this.title$.asObservable();
    }

    get clazz() {
        return this.clazz$.asObservable();
    }

    setTitle(value: string) {
        this.title$.next(value);
    }

    setClazz(value: string) {
        this.clazz$.next(value);
    }
}
