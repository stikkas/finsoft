import {Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {JsonResponse} from '../models';
import {LoadingService} from './loading.service';
import {TemplateRef} from '@angular/core';
import {NotifierService} from 'angular-notifier';

export abstract class Responsable {
    protected ls: LoadingService;
    fieldsTmpl: TemplateRef<any>;

    protected constructor(protected ns: NotifierService) {
    }

    /**
     * Обрабатывает полученный результат с учетом ошибок и обертки в JsonResponse
     * @param obs - ответ http клиента
     * @param loading - включать или нет показ загрузчика, по умолчанию включаем
     */
    protected handle<T>(obs: Observable<JsonResponse<T>>, loading = true): Observable<T> {
        if (loading) {
            this.startLoading();
        }
        return obs.pipe(map((res: JsonResponse<T>) => {
                this.stopLoading();
                return res.status ? res.data : this.errorHandler({message: res.data});
            }),
            catchError(err => {
                this.stopLoading();
                return of(this.errorHandler(err.error));
            }));
    }

    private stopLoading() {
        if (this.ls) {
            this.ls.load(false);
        }
    }

    private startLoading() {
        if (this.ls) {
            this.ls.load(true);
        }
    }

    private errorHandler(err) {
        if (err && err.message) {
            if (Array.isArray(err.message)) {
                if (this.fieldsTmpl) {
                    this.ns.show({
                        type: 'error',
                        message: err.message.map(it => it.defaultMessage || it.code),
                        template: this.fieldsTmpl
                    });
                } else {
                    this.ns.notify('error', 'Необходимо заполнить следующие поля: ' +
                        err.message.map(it => it.defaultMessage || it.code).join(', '));
                }
            } else {
                this.ns.notify('error', err.message);
            }
        } else if (err) {
            console.log(err);
        }
        return null;
    }
}
