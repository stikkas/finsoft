import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LoadingService {
    private load$ = new Subject<boolean>();

    get loading() {
        return this.load$.asObservable();
    }

    load(value: boolean) {
        this.load$.next(value);
    }
}
