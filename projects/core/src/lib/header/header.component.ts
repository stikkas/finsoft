import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation} from '@angular/core';
import {HeadButtonsService} from './head-buttons.service';
import {ButtonType} from '../models';
import {buttonTitle, urls} from '../utils';

@Component({
    selector: 'fi-header',
    templateUrl: './header.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
    buttons$ = this.hbs.buttons$;
    disabled$ = this.hbs.disabled$;
    logoutUrl: string;
    bt = ButtonType;
    btt = buttonTitle;
    urls = urls;
    @Input() title: string;

    constructor(private hbs: HeadButtonsService) {
        this.logoutUrl = `${urls.adminApi}/logout`;
    }
}

