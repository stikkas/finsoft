import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header.component';
import {HeadButtonsService} from './head-buttons.service';
import {LibComponentsModule} from '../components/lib-components.module';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';

/**
 * Заголовок страницы
 */
@NgModule({
    declarations: [HeaderComponent],
    imports: [CommonModule, LibComponentsModule, NgbDropdownModule, RouterModule],
    exports: [HeaderComponent],
    providers: [HeadButtonsService]
})
export class HeaderModule {
}
