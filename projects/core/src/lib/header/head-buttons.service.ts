import {BehaviorSubject} from 'rxjs';
import {ComponentWithOnDestroyObservable, untilComponentDestroyed} from '@w11k/ngx-componentdestroyed';
import {Button, ButtonType} from '../models';
import {bb, buttonTitle, context} from '../utils';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {MeService} from '../services/me.service';

/**
 * Сервис для установки кнопок верхнего меню в зависимости от текущего компонента
 */
@Injectable()
export class HeadButtonsService {
    constructor(private router: Router, private me: MeService) {
    }

    private buttons = new BehaviorSubject<Button[]>(null);
    private disabled = new BehaviorSubject<(number | boolean)[]>([]);

    /**
     * Создает набор кнопок и устанавливает его
     * @param btns - прототипы кнопок
     */
    setButtons(...btns: { [key in ButtonType]?: any }[]) {
        this.buttons.next(
            btns.map(b => {
                const k = Object.keys(b)[0];
                return {
                    title: buttonTitle[k],
                    clazz: k,
                    ...b[k]
                };
            })
        );
    }

    /**
     * Заменяет кнопки
     * @param add - если true, то вставляем кнопку в указанный индекс, иначе заменяем существующую
     * @param btns - прототипы кнопок с индексами тех кнопок, которые нужно заменить
     */
    replaceButtons(add: boolean, ...btns: { [idx: number]: { [key in ButtonType]?: any } }[]) {
        const buttons = this.buttons.getValue();
        btns.forEach(it => {
            const idx = +Object.keys(it)[0];
            const k = Object.keys(it[idx])[0];
            buttons.splice(idx, add && buttons[idx].clazz !== k ? 0 : 1, {
                title: buttonTitle[k],
                clazz: k,
                ...it[idx][k]
            });
        });
        this.buttons.next(buttons);
    }

    /**
     * Для установки с кнопкой "Последние десять"
     * @param cmp - компонент, который подписывается
     * @param btns - кнопки
     */
    setButtonsWithLastTen(cmp: ComponentWithOnDestroyObservable, ...btns: { [key in ButtonType]?: any }[]) {
        this.me.getLastT();
        this.me.lastT$.pipe(untilComponentDestroyed(cmp)).subscribe(res => {
            this.setButtons(bb.lten(res.map(it => ({
                    title: it.title,
                    click: () => {
                        if (context.aisUrl) { // находимся в админке
                            window.open(`${context.aisUrl}/pf/${it.id}/man-info`, '_self');
                        } else {
                            this.router.navigate(['/pf', it.id].concat(window.location.pathname.endsWith('man-info') ? [] : ['man-info']));
                        }
                    }
                }))), ...btns
            );
        });
    }

    /**
     * Устанавливает нужные кнопки в disabled
     * @param disabled - список, в котором индекс соответствует индексу из buttons$, если значение
     * truthable то соответствующая кнопка устанавливается в disabled
     */
    setDisabled(disabled: (number | boolean)[]) {
        this.disabled.next(disabled);
    }

    get buttons$() {
        return this.buttons.asObservable();
    }

    get disabled$() {
        return this.disabled.asObservable();
    }
}

