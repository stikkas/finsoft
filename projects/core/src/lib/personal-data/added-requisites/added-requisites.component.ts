import {ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {PersonalDataAbstractComponent} from '../personal-data-abstract.component';
import {AddedRequisites, Education, JobInfo, SocialStatus} from '../models';
import {ClsSearchComponentService} from '../../modals/cls-search/cls-search.component.service';
import {OrgSearchComponentService} from '../../modals/org-search/org-search.component.service';
import {DictService} from '../../services/dict.service';
import {ConfirmRemoveComponentService} from '../../modals/confirm-remove/confirm-remove.component.service';
import {PersonalService} from '../services/personal.service';
import {HeadButtonsService} from '../../header/head-buttons.service';
import {ConfirmSaveComponentService} from '../../modals/confirm-save/confirm-save.component.service';
import {ViewEditService} from '../../services/view-edit.service';
import {PageTitleService} from '../../services/page-title.service';
import {Dict, Training} from '../../models';
import {bb, cls, onDone, scrollTable, urls} from '../../utils';
import {Title} from '@angular/platform-browser';
import {untilComponentDestroyed} from '@w11k/ngx-componentdestroyed';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

@Component({
    templateUrl: './added-requisites.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddedRequisitesComponent extends PersonalDataAbstractComponent<AddedRequisites> implements OnInit {
    cls = cls;
    statHeaders = ['Социальный статус', 'Дата установки*', 'Дата снятия'];
    eduHeaders = ['Образование', 'Дата сведений*'];
    trainHeaders = ['Место учебы*', 'Вид образования', 'Класс/курс', 'Специальность', 'Дата начала*', 'Дата окончания'];
    jobHeaders = ['Место работы*', 'Должность', 'Телефон 1', 'Телефон 2', 'E-mail', 'Дата<br>приема*', 'Дата<br>увольнения'];

    statuses$ = new BehaviorSubject<SocialStatus[]>(null);
    educts$ = new BehaviorSubject<Education[]>(null);
    trains$ = new BehaviorSubject<Training[]>(null);
    jobs$ = new BehaviorSubject<JobInfo[]>(null);

    edus$ = this.ds.getDict('EDUCATION');
    eduId: string;

    place$: Observable<Dict[]>[] = [];
    kind$ = this.ds.getDict(cls.learning);
    pInputs$: any[];

    constructor(ts: Title, pfs: PersonalService, ves: ViewEditService, private clscs: ClsSearchComponentService, router: Router,
                private ds: DictService, private oscs: OrgSearchComponentService, private crcs: ConfirmRemoveComponentService,
                private route: ActivatedRoute, cscs: ConfirmSaveComponentService, hbs: HeadButtonsService, pt: PageTitleService) {
        super(pfs, ves, ts, cscs, hbs, 'Дополнительные реквизиты', pt,
            [bb.save(() => this.save()), bb.print(() => this.print())], router);
    }

    ngOnInit() {
        this.route.data.pipe(untilComponentDestroyed(this)).subscribe(({info}) => {
            this.data = info as AddedRequisites;
            this.setData();
            info.jobs.forEach((it, i) => {
                const input = (this.pInputs$ = this.pInputs$ || [])[i] = new BehaviorSubject<string>(null);
                (this.place$ = this.place$ || [])[i] = this.typeAhead(input);
            });
            setTimeout(() => {
                this.setPageTitle();
                this.setEditMode(false);
            });
        });

    }

    removeState(idx: number) {
        onDone(this.crcs.open(), () => {
            this.data.states.splice(idx, 1);
            this.statuses$.next([...this.data.states]);
        });
    }

    /**
     * Добавляет социальный статус
     */
    addStatus() {
        onDone(this.clscs.open('Поиск социальных статусов', [cls.socst, cls.fmst], undefined, undefined, true),
            d => {
                (this.data.states = this.data.states || []).push(new SocialStatus(d.id, d.value));
                this.statuses$.next([...this.data.states]);
                scrollTable('soc-statuses-js', this.data.states.length, 2);
            }
        );
    }

    removeEdu(idx: number) {
        onDone(this.crcs.open(), () => {
            this.data.edus.splice(idx, 1);
            this.educts$.next([...this.data.edus]);
        });
    }

    addEdu() {
        if (this.eduId) {
            (this.data.edus = this.data.edus || []).push(new Education(this.eduId));
            this.educts$.next([...this.data.edus]);
            scrollTable('education-js', this.data.edus.length, 2);
        }
    }

    /**
     * Удаляет запись об обучении
     * @param idx - индекс в списке записей
     */
    removeTrain(idx: number) {
        onDone(this.crcs.open(), () => {
            this.data.trains.splice(idx, 1);
            this.trains$.next([...this.data.trains]);
        });
    }

    /**
     * Добавляет запись к записям об обучении
     */
    addTrain(train?: Training) {
        (this.data.trains = this.data.trains || []).push(train ? {...train, id: undefined} : new Training());
        this.trains$.next([...this.data.trains]);
        scrollTable('train-js', this.data.trains.length, 2, 50);
    }

    /**
     * Удаляет запись о работе
     * @param idx - индекс в списке записей
     */
    removeJob(idx: number) {
        onDone(this.crcs.open(), () => {
            this.data.jobs.splice(idx, 1);
            this.jobs$.next([...this.data.jobs]);
        });
    }

    /**
     * Добавляет запись к записям о работе
     */
    addJob() {
        (this.data.jobs = this.data.jobs || []).push(new JobInfo());
        this.jobs$.next([...this.data.jobs]);
        const input = new BehaviorSubject<string>(null);
        (this.pInputs$ = this.pInputs$ || []).push(input);
        (this.place$ = this.place$ || []).push(this.typeAhead(input));
        scrollTable('job-js', this.data.jobs.length, 2, 50);
    }

    /**
     * Устанавливает место обучения / работы
     * @param obj - запись об обучении или работе
     */
    setOrg(obj: Training) {
        onDone(this.oscs.open(undefined, true), ({id, value}) => {
            obj.placeId = id;
            obj.place = value;
            this.trains$.next([...this.data.trains]);
        });
    }

    getLabel(id: string, dict: string) {
        return this.ds.getLabel(id, dict);
    }

    courseChanged(train: Training) {
        if (train.course && train.course.length > 20) {
            train.course = train.course.substr(0, 20);
        }
    }

    protected runSave(): Observable<AddedRequisites> {
        this.bhs.setDisabled([1, 1, 1, 1]);
        return this.pfs.saveAddReq(this.data);
    }

    protected setEditMode(mode = true): void {
        super.setEditMode(mode);
        if (mode) {
            this.ves.edit();
            this.bhs.setDisabled([, , , 0]);
            setTimeout(() => {
                this.data.jobs.forEach((it, i) => {
                    if (it.place) {
                        this.pInputs$[i].next(it.place);
                    }
                });
            });
        } else {
            this.ves.view();
            this.bhs.setDisabled([, !this.data.actual, 1, 0]);
        }
    }

    private typeAhead(input: BehaviorSubject<string>) {
        return input.pipe(
            debounceTime(200),
            distinctUntilChanged(),
            switchMap(query => this.ds.loadOnce(urls.api.dicts.jobp, query)));
    }

    private setData() {
        const data = this.data;
        this.statuses$.next(data.states && [...data.states]);
        this.educts$.next(data.edus && [...data.edus]);
        this.trains$.next(data.trains && [...data.trains]);
        this.jobs$.next(data.jobs && [...data.jobs]);
    }

    private save() {
        this.runSave().subscribe(res => {
            if (res) {
                this.data.states = res.states;
                this.data.edus = res.edus;
                this.data.trains = res.trains;
                this.data.jobs = res.jobs;
                this.setData();
                this.setEditMode(false);
            } else {
                this.setEditMode();
            }
        });
    }

    private print() {

    }

}

