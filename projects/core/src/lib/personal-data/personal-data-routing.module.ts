import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ManInfoComponent} from './man-info/man-info.component';
import {RegistrationsComponent} from './registrations/registrations.component';
import {ChangesDocComponent} from './changes-doc/changes-doc.component';
import {InvalidIncapableComponent} from './invalid-incapable/invalid-incapable.component';
import {ManinfoResolveService} from './services/maninfo-resolve.service';
import {ZagsComponent} from './zags/zags.component';
import {ZagsResolveService} from './services/zags-resolve.service';
import {InvIncResolveService} from './services/inv-inc-resolve.service';
import {ChangeResolveService} from './services/change-resolve.service';
import {AddedRequisitesComponent} from './added-requisites/added-requisites.component';
import {AddedReqResolveService} from './services/added-req-resolve.service';
import {SmevComponent} from './smev/smev.component';
import {SmevsResolveService} from './services/smevs-resolve.service';
import {PersonalDataComponent} from './personal-data.component';
import {CanDeactivateGuard} from '../services/can-deactivate.guard';

const routes: Routes = [{
    path: '',
    component: PersonalDataComponent,
    children: [{
        path: '',
        pathMatch: 'full',
        component: ManInfoComponent,
        resolve: {info: ManinfoResolveService},
        canDeactivate: [CanDeactivateGuard]
    }, {
        path: ':id',
        component: ManInfoComponent,
        resolve: {info: ManinfoResolveService},
        runGuardsAndResolvers: 'always',
        canDeactivate: [CanDeactivateGuard]
    }, {
        path: ':id/man-info',
        component: ManInfoComponent,
        resolve: {info: ManinfoResolveService},
        runGuardsAndResolvers: 'always',
        canDeactivate: [CanDeactivateGuard]
    }, {
        path: ':id/registration',
        component: RegistrationsComponent
    }, {
        path: ':id/zags',
        component: ZagsComponent,
        resolve: {info: ZagsResolveService},
        runGuardsAndResolvers: 'always',
        canDeactivate: [CanDeactivateGuard]
    }, {
        path: ':id/changes-document',
        component: ChangesDocComponent,
        resolve: {info: ChangeResolveService}
    }, {
        path: ':id/invalid-incapable',
        component: InvalidIncapableComponent,
        resolve: {info: InvIncResolveService},
        runGuardsAndResolvers: 'always',
        canDeactivate: [CanDeactivateGuard]
    }, {
        path: ':id/added-req',
        component: AddedRequisitesComponent,
        resolve: {info: AddedReqResolveService},
        runGuardsAndResolvers: 'always',
        canDeactivate: [CanDeactivateGuard]
    }, {
        path: ':id/smevs',
        component: SmevComponent,
        resolve: {info: SmevsResolveService},
        runGuardsAndResolvers: 'always',
        canDeactivate: [CanDeactivateGuard]
    }]
}];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PersonalDataRoutingModule {
}

