import {Component, ViewEncapsulation} from '@angular/core';
import {DwellingPlace} from '../../models';
import {getAddress, context} from '../../utils';
import {Title} from '@angular/platform-browser';

@Component({
    templateUrl: './registrations.component.html',
    encapsulation: ViewEncapsulation.None
})
export class RegistrationsComponent {
    regTableHeaders = [
        'Регистрация',
        'Адрес',
        'Дата начала',
        'Дата окончания',
        'Дата убытия'
    ];
    regs = [] as DwellingPlace[];
    ga = getAddress;
    curReg: DwellingPlace;

    constructor(titleService: Title) {
        if (context.aisId !== 3) {
            titleService.setTitle('Личное дело - Прибытие, убитие, другие регистрации');
        }
    }
}
