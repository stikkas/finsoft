import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {SmevsInfo} from '../models';
import {PersonalService} from './personal.service';

@Injectable()
export class SmevsResolveService implements Resolve<SmevsInfo> {

    constructor(private service: PersonalService, private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SmevsInfo> {
        const id = route.paramMap.get('id');
        return this.service.getSmevs(id).pipe(map(res => res ? res :
            (this.router.navigate(['/pf', id, 'man-info']) && null)));
    }
}

