import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {PersonalService} from './personal.service';
import {InvalidIncapable} from '../models';
import {map} from 'rxjs/operators';

@Injectable()
export class InvIncResolveService implements Resolve<InvalidIncapable> {

    constructor(private service: PersonalService, private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<InvalidIncapable> {
        const id = route.paramMap.get('id');
        return this.service.getInvalidIncapable(id).pipe(map(res => res ?
            res : (this.router.navigate(id ? ['/pf', id, 'man-info'] : ['/']) && null)));
    }
}

