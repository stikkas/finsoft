import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {NotifierService} from 'angular-notifier';
import {map, tap} from 'rxjs/operators';
import {Responsable} from '../../services/responsable';
import {context, notEmpty, urls} from '../../utils';
import {LoadingService} from '../../services/loading.service';
import {AddedRequisites, ChangeInfo, InvalidIncapable, InvIncItem, ManInfo, PfInitials, SmevsInfo, ZagsInfo, ZagsItem} from '../models';
import {Document, DwellingPlace, IncapableDetails, InvalidDetails, JsonResponse, SmevRequest} from '../../models';

@Injectable()
export class PersonalService extends Responsable {
    private baseUrl = `${context.app}${urls.api.pfs.base}`;
    private mainInfoUrl = `${this.baseUrl}${urls.api.pfs.mainInfo}`;
    private personUrl = `${this.baseUrl}${urls.api.pfs.person}`;
    private docUrl = `${this.baseUrl}${urls.api.pfs.doc}`;
    private lenaUrl = `${this.baseUrl}${urls.api.pfs.lena}`;
    private addedUrl = `${this.baseUrl}${urls.api.pfs.added}`;
    private smevsUrl = `${this.baseUrl}${urls.api.pfs.smevs}`;
    private rootAddressUrl = `${this.baseUrl}${urls.api.pfs.root}`;
    private current = new BehaviorSubject<{
        id: string, registered: any, actual: number,
        bd: string, dis: boolean, fid?: string, suId?: number
    }>(null);

    constructor(private http: HttpClient, ns: NotifierService, protected ls: LoadingService) {
        super(ns);
    }

    get current$() {
        return this.current.asObservable();
    }

    /**
     * Возвращает основные реквизиты полученные с сервера, если есть id, либо частично сформированные, если пришел запрос
     * на создание личного дела
     * @param id - идентификатор существующего личного дела
     * @param chid - число, по которому можно попытаться найти сохраненные данные на сервере, используется при переходе
     * из строки поиска по кнопке создать, на основе этих данных будет создана новая запись
     */
    getManInfo(id: string, chid: string): Observable<ManInfo> {
        return this.getInfo(this.mainInfoUrl, id ? {id} : {chid});
    }

    /**
     * Сохраняет данные личного дела в базе
     * @param info - данные личного дела
     * @param doc - документ для добавления
     * @param what - что сохраняем
     * @param files - прикрепляемые файлы
     * @param force - ответ "Да" для создания похожего ЛД
     */
    save(info: ManInfo, doc: Document, what: number, files: any[], force = false) {
        const body = {...info, docs: [...info.docs, ...(doc ? [doc] : [])]} as any;
        if (!notEmpty(body.birthAddress)) {
            delete body.birthAddress;
        }
        if (!notEmpty(body.regs)) {
            delete body.regs;
        }
        if (!notEmpty(body.docs)) {
            delete body.docs;
        }
        const formData = new FormData();
        formData.append('info', JSON.stringify(body));
        if (files && files.length && body.docs && body.docs.length) {
            files.forEach(([obj, file]) => {
                const idx = body.docs.findIndex(o => o === obj);
                if (idx > -1) {
                    formData.append('files', file, `${idx}:${file.name}`);
                }
            });
        }
        if (what === 0 && force) {
            formData.append('force', 'true');
        }
        return this.handle(this.http.post<JsonResponse<ManInfo>>(what === 0 ? this.mainInfoUrl : what === 1 ? this.personUrl
            : this.docUrl, formData)).pipe(tap(res => {
            if (res) {
                if (typeof res === 'string') {
                    this.ns.notify('warning', res);
                } else if (!Array.isArray(res)) {
                    this.ns.notify('info', 'Данные сохранены');
                }
            }
        }));
    }

    getSmevs(id: string): Observable<SmevsInfo> {
        return this.getInfo(`${this.baseUrl}/${id}${urls.api.pfs.smevs}`);
    }

    saveSmevs(info: SmevsInfo): Observable<SmevsInfo> {
        return this.handle(this.http.post<JsonResponse<SmevsInfo>>(this.smevsUrl, info))
            .pipe(tap(res => {
                if (res) {
                    this.ns.notify('info', 'Данные сохранены');
                }
            }));
    }

    sendRequest(id: string, req: SmevRequest): Observable<SmevRequest> {
        return this.handle(this.http.post<JsonResponse<SmevRequest>>(`${this.baseUrl}/${id}${urls.api.pfs.smevs}`, req))
            .pipe(tap(res => {
                if (res) {
                    this.ns.notify('info', 'Запрос отправлен');
                }
            }));
    }

    /**
     * Удаляет данные личного дела из базы
     * @param id - идентификатор из таблицы population
     */
    remove(id: string) {
        return this.handle(this.http.delete<JsonResponse<string>>(`${this.baseUrl}/${id}`)).pipe(tap(res => {
            if (res) {
                this.ns.notify('warning', `Нельзя удалить личное дело, т.к. ${res}`);
            }
        }));
    }

    /**
     * Получает корневой адрес организации, которой принадлежит текущий пользователь
     * @param place - возвращается если пусто
     */
    getRootAddress(place: DwellingPlace): Observable<DwellingPlace> {
        return this.handle(this.http.get<JsonResponse<DwellingPlace>>(this.rootAddressUrl), false).pipe(map(res =>
            (res && {
                ...res,
                addressType: place.addressType,
                regType: place.regType,
                regTypeStr: place.regTypeStr,
                endDate: place.endDate,
                startDate: place.startDate
            }) || place));
    }

    /**
     * Возвращает информацию для вкладки "Сведения ЗАГС" личного дела
     * @param id - идентификатор человека
     */
    getZagsInfo(id: string): Observable<ZagsInfo> {
        return this.getInfo(`${this.baseUrl}/${id}${urls.api.pfs.zags}`)
            .pipe(tap((res: ZagsInfo) => {
                    if (res) {
                        res.certs.forEach(it => {
                            it.regDate = it.regDate || it.issueDate;
                        });
                    }
                })
            );
    }

    /**
     * Возвращает информацию для вкладки "Изменение ФИО, документа" личного дела
     * @param id - идентификатор человека
     */
    getChangeInfo(id: string): Observable<ChangeInfo> {
        return this.getInfo(`${this.baseUrl}/${id}${urls.api.pfs.chng}`);
    }


    saveZags(cert: ZagsItem, manId: string): Observable<ZagsItem> {
        const data = {
            ...cert, regDate: cert.regDate || cert.issueDate
        };
        return this.handle(
            this.http.post<JsonResponse<ZagsItem>>(`${this.baseUrl}/${manId}${urls.api.pfs.zags}`, data))
            .pipe(tap(res => {
                if (res) {
                    this.ns.notify('info', 'Данные сохранены');
                }
            }));
    }

    removeZags(id: string): Observable<boolean> {
        return this.handle(this.http.delete<JsonResponse<boolean>>(`${this.baseUrl}/${id}${urls.api.pfs.zags}`))
            .pipe(tap(res => {
                if (!res) {
                    this.ns.notify('warning', 'Документ используется в заявлении. Сначала удалите документ из заявления');
                }
            }));
    }

    /**
     * Возвращает информацию для вкладки "Дополнительные реквизиты" личного дела
     * @param id - идентификатор человека
     */
    getInvalidIncapable(id: string): Observable<InvalidIncapable> {
        return this.getInfo(`${this.baseUrl}/${id}${urls.api.pfs.other}`);
    }

    saveInvInc(inv: InvalidDetails, inc: IncapableDetails, manId: string): Observable<InvIncItem> {
        const data = {} as any;
        if (inv) {
            data.invalid = {
                ...inv, invDoc: notEmpty(inv.invDoc) ? inv.invDoc : null
            };
        }
        if (inc) {
            data.incapable = {
                ...inc, incDoc: notEmpty(inc.incDoc) ? inc.incDoc : null
            };
        }
        return this.handle(
            this.http.post<JsonResponse<InvIncItem>>(`${this.baseUrl}/${manId}${urls.api.pfs.other}`, data))
            .pipe(tap(res => {
                if (res) {
                    this.current.next({
                        ...this.current.getValue(),
                        dis: !!(res.incapable && res.incapable.disabilityId)
                    });
                    this.ns.notify('info', 'Данные сохранены');
                }
            }));
    }

    removeInvalidity(invalidityId: string): Observable<boolean> {
        return this.handle(this.http.delete<JsonResponse<boolean>>(`${this.baseUrl}/${invalidityId}${urls.api.pfs.other}`));
    }

    removeIncapability(disabilityId: string): Observable<boolean> {
        return this.handle(this.http.delete<JsonResponse<boolean>>(`${this.baseUrl}/${disabilityId}${urls.api.pfs.other}`, {params: {dis: 'true'}}));
    }

    getAddedRequisites(id: string): Observable<AddedRequisites> {
        return this.getInfo(`${this.baseUrl}/${id}${urls.api.pfs.added}`);
    }

    saveAddReq(info: AddedRequisites): Observable<AddedRequisites> {
        return this.handle(this.http.post<JsonResponse<AddedRequisites>>(this.addedUrl, info)).pipe(tap(res => {
            if (res) {
                this.ns.notify('info', 'Данные сохранены');
            }
        }));
    }

    /**
     * Возвращает true если последнее движение ребенка - не усыновление/удочерение
     * @param id - идентификатор ребенка
     */
    lastEventNotAdopt(id): Observable<boolean> {
        return this.handle(this.http.get<JsonResponse<boolean>>(`${this.lenaUrl}/${id}`), false);
    }

    private setCurData(res: PfInitials) {
        this.current.next({
            id: res.id, registered: res.registered,
            actual: res.actual,
            bd: res.birthDateText, dis: res.disability,
            fid: res.familyId, suId: res.suId
        });
    }

    /**
     * Получает общие данные для закладки
     * @param url - путь
     * @param params - дополнительные параметры
     * @private
     */
    private getInfo<T extends PfInitials>(url: string, params?: any): Observable<T> {
        return this.handle(this.http.get<JsonResponse<T>>(url, params ? {params} : {}), false)
            .pipe(tap(res => {
                    if (res) {
                        this.setCurData(res);
                    }
                })
            );
    }
}

