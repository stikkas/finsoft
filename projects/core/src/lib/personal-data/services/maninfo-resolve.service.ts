import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {PersonalService} from './personal.service';
import {ManInfo} from '../models';
import {map} from 'rxjs/operators';

@Injectable()
export class ManinfoResolveService implements Resolve<ManInfo> {

    constructor(private service: PersonalService, private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ManInfo> {
        return this.service.getManInfo(route.paramMap.get('id'), route.queryParamMap.get('chid')).pipe(map(res => res ?
            res : (this.router.navigate(['/']) && null)));
    }
}

