import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {PersonalService} from './personal.service';
import {ChangeInfo} from '../models';
import {map} from 'rxjs/operators';

@Injectable()
export class ChangeResolveService implements Resolve<ChangeInfo> {

    constructor(private service: PersonalService, private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ChangeInfo> {
        const id = route.paramMap.get('id');
        return this.service.getChangeInfo(id).pipe(map(res => res ?
            res : (this.router.navigate(id ? ['/pf', id, 'man-info'] : ['/']) && null)));
    }
}

