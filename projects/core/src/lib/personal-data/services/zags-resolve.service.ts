import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ZagsInfo} from '../models';
import {PersonalService} from './personal.service';

@Injectable()
export class ZagsResolveService implements Resolve<ZagsInfo> {

    constructor(private service: PersonalService, private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ZagsInfo> {
        const id = route.paramMap.get('id');
        return this.service.getZagsInfo(id).pipe(map(res => res ?
            res : (this.router.navigate(id ? ['/pf', id, 'man-info'] : ['/']) && null)));
    }
}

