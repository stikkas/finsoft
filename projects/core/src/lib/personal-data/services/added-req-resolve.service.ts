import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {PersonalService} from './personal.service';
import {AddedRequisites} from '../models';
import {map} from 'rxjs/operators';

@Injectable()
export class AddedReqResolveService implements Resolve<AddedRequisites> {

    constructor(private service: PersonalService, private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<AddedRequisites> {
        const id = route.paramMap.get('id');
        return this.service.getAddedRequisites(id).pipe(map(res => res ?
            res : (this.router.navigate(id ? ['/pf', id, 'man-info'] : ['/']) && null)));
    }
}

