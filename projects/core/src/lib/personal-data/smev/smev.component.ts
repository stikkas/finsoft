import {ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {NgbDropdown} from '@ng-bootstrap/ng-bootstrap';
import {PersonalDataAbstractComponent} from '../personal-data-abstract.component';
import {DictService} from '../../services/dict.service';
import {ConfirmRemoveComponentService} from '../../modals/confirm-remove/confirm-remove.component.service';
import {ChooseAddressComponentService} from '../../modals/choose-address/choose-address.component.service';
import {HeadButtonsService} from '../../header/head-buttons.service';
import {SmevsInfo} from '../models';
import {ConfirmSaveComponentService} from '../../modals/confirm-save/confirm-save.component.service';
import {Dict, DictOrg, SmevRequest, SortEvent} from '../../models';
import {PageTitleService} from '../../services/page-title.service';
import {ViewEditService} from '../../services/view-edit.service';
import {PersonalService} from '../services/personal.service';
import {bb, cls, context, onDone, showFile, urls} from '../../utils';
import {Title} from '@angular/platform-browser';
import {untilComponentDestroyed} from '@w11k/ngx-componentdestroyed';

@Component({
    templateUrl: './smev.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SmevComponent extends PersonalDataAbstractComponent<SmevsInfo> implements OnInit {
    /**
     * Храним загруженный справочник СМЭВ запросов для получения названия соответсующего id
     */
    private requests: DictOrg[];
    private defSort: SortEvent = {column: 'reqDate', direction: 'desc'};
    headers = ['№', 'Вид сведений', 'Дата<br>запроса', 'Статус', 'Дата<br>ответа', 'Код запроса'];
    sorts = ['', 'smevName', 'reqDate', 'statusId', 'ansDate', 'code'];
    selectedReqs: number[];
    reqs$ = new BehaviorSubject<SmevRequest[]>(null);
    stypes$: Observable<Dict[]>;
    curReq$ = new BehaviorSubject<SmevRequest>(null);
    /**
     * Запросы, которые сейчас отправляются
     */
    processing = [] as SmevRequest[];

    sf = fn => showFile(fn, context.aisUrl || context.app);

    constructor(pfs: PersonalService, cscs: ConfirmSaveComponentService, pageTitle: PageTitleService,
                ts: Title, ves: ViewEditService, bhs: HeadButtonsService, private dictService: DictService,
                private route: ActivatedRoute, router: Router,
                private rpcs: ChooseAddressComponentService, private crcs: ConfirmRemoveComponentService) {
        super(pfs, ves, ts, cscs, bhs, 'Межведомственные запросы', pageTitle, [bb.save(() => this.save()),
            bb.print(() => this.print())], router);
    }

    ngOnInit(): void {
        this.stypes$ = this.dictService.loadOnce(urls.api.dicts.stets).pipe(map(r => {
            this.requests = r as DictOrg[];
            return r.map(it => ({...it, novalue: 1}));
        }));
        combineLatest([this.route.data, this.route.queryParams]).pipe(untilComponentDestroyed(this))
            .subscribe(([data, qp]) => {
                const info = this.data = data.info as SmevsInfo;
                const sid = +qp.sid;
                this.reqs$.next(info.smevs ? [...info.smevs] : []);
                this.sort(this.defSort);
                this.curReq$.next(null);
                setTimeout(() => {
                    this.setPageTitle();
                    this.setEditMode(false);
                    if (!isNaN(sid) && info.smevs) {
                        this.setCurrent(info.smevs.find(it => it.id === sid));
                    }
                });
            });
    }

    getLabel(id: string, dict: string) {
        return this.dictService.getLabel(id, dict);
    }

    smevsSelected() {
        const smevs = this.selectedReqs;
        if (smevs && smevs.length) {
            const reqs = this.data.smevs = this.data.smevs || [];
            smevs.forEach(it => {
                const d = this.requests.find(t => t.value === it);
                reqs.push(new SmevRequest(it, d.label, d.orgId, d.orgName));
            });
            this.selectedReqs = [];
            this.reqs$.next([...reqs]);
            this.sort(this.defSort);
        }
    }

    setCurrent(req: SmevRequest) {
        this.curReq$.next(req);
    }

    isCurrent(req: SmevRequest) {
        return this.curReq$.getValue() === req;
    }

    /**
     * Отправить запрос
     * @param ctxm - объект меню
     * @param req - запрос
     */
    send(ctxm: NgbDropdown, req: SmevRequest) {
        ctxm.close();
        if (!this.processing.some(it => it === req)) {
            this.processing.push(req);
            this.pfs.sendRequest(this.data.id, req).subscribe(res => {
                if (res) {
                    const i = this.data.smevs.findIndex(it => it === req);
                    if (i > -1) {
                        this.data.smevs[i] = res;
                        this.reqs$.next([...this.data.smevs]);
                        this.sort(this.defSort);
                        this.setCurrent(res);
                    }
                }
                const idx = this.processing.findIndex(it => it === req);
                if (idx > -1) {
                    this.processing.splice(idx, 1);
                }
            });
        }
    }

    /**
     * Копирование запроса
     * @param ctxm - объект меню
     * @param req - запрос
     */
    copy(ctxm: NgbDropdown, req: SmevRequest) {
        ctxm.close();
        this.data.smevs.push(new SmevRequest(req.smevId, req.smevName, req.orgId, req.orgName));
        this.reqs$.next([...this.data.smevs]);
        this.sort(this.defSort);
        this.setCurrent(this.data.smevs[this.data.smevs.length - 1]);
    }

    /**
     * Удаляет запрос
     * @param ctxm - объект меню
     * @param i - индекс запроса
     */
    remove(ctxm: NgbDropdown, i: number) {
        ctxm.close();
        onDone(this.crcs.open(), () => {
            this.data.smevs.splice(i, 1);
            this.reqs$.next([...this.data.smevs]);
            this.sort(this.defSort);
            this.curReq$.next(null);
        });
    }

    sort(event: SortEvent) {
        const data = this.reqs$.getValue();
        if (data) {
            let field = this.defSort.column;
            let dir = this.defSort.direction;
            if (event.column && event.direction) {
                field = event.column;
                dir = event.direction;
            }
            data.sort((a, b) => (dir === 'asc' ? (!a[field] ? false : !b[field] ? true : a[field] > b[field])
                : (!a[field] ? true : !b[field] ? false : a[field] < b[field])) ? 1 : -1);
            this.reqs$.next(data);
        }
    }

    /**
     * Устанавливаем режим редактирования
     * @mode - если true - все поля доступны для редактирования, иначе только просмотр
     */
    protected setEditMode(mode = true) {
        super.setEditMode(mode);
        if (mode) {
            this.ves.edit();
            this.bhs.setDisabled([]);
        } else {
            this.ves.view();
            this.bhs.setDisabled([, !this.data.actual, 1]);
        }
    }

    protected runSave() {
        this.bhs.setDisabled([1, 1]);
        return this.pfs.saveSmevs(this.data);
    }

    private save() {
        this.runSave().subscribe(res => {
            if (res) {
                const cur = this.curReq$.getValue();
                const curId = cur && this.data.smevs.findIndex(it => it === cur);
                this.data.smevs = res.smevs;
                this.reqs$.next([...res.smevs]);
                this.sort(this.defSort);
                this.setCurrent((curId && curId > -1) ? this.data.smevs[curId] : null);
                this.setEditMode(false);
            } else {
                this.setEditMode();
            }
        });
    }

    private print() {
        const cur = this.curReq$.getValue();
        const id = cur && cur.code;
        if (id && cur.statusId === cls.reqrec) {
            window.open(`${context.app}${urls.api.rptrs}?r=smev&id=${id}`, '_blank');
        }
    }
}

