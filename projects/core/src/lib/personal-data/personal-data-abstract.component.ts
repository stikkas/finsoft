import {Observable, of} from 'rxjs';
import {switchMap, take} from 'rxjs/operators';
import {AfterViewInit, TemplateRef, ViewChild} from '@angular/core';
import {OnDestroyMixin} from '@w11k/ngx-componentdestroyed';
import {PfInitials} from './models';
import {CanComponentDeactivate} from '../models';
import {PersonalService} from './services/personal.service';
import {ViewEditService} from '../services/view-edit.service';
import {ConfirmSaveComponentService} from '../modals/confirm-save/confirm-save.component.service';
import {HeadButtonsService} from '../header/head-buttons.service';
import {PageTitleService} from '../services/page-title.service';
import {Title} from '@angular/platform-browser';
import {bb, context} from '../utils';
import {Router} from '@angular/router';

export abstract class PersonalDataAbstractComponent<T extends PfInitials>
    extends OnDestroyMixin implements CanComponentDeactivate, AfterViewInit {
    private inCancel = false;
    private readonly editButton: any;
    protected readonly cancelButton: any;

    @ViewChild('fieldsErrors') fieldsErrorsTmpl: TemplateRef<any>;
    viewMode$ = this.ves.viewMode$;
    data: T;

    protected constructor(protected pfs: PersonalService, protected ves: ViewEditService,
                          ts: Title, protected cscs: ConfirmSaveComponentService, protected bhs: HeadButtonsService, title: string,
                          private pt: PageTitleService, btns: any[], protected router: Router, addEdit = true, editButton?: any) {
        super();
        if (context.aisId !== 3) {
            ts.setTitle(`Личное дело - ${title}`);
        }
        if (addEdit) {
            this.editButton = editButton || bb.edit(() => {
                this.bhs.replaceButtons(false, {1: this.cancelButton});
                this.setEditMode();
            });

            this.cancelButton = bb.cancel(() => {
                this.inCancel = true;
                this.router.navigateByUrl(this.router.url);
            });
            bhs.setButtonsWithLastTen(this, this.editButton, ...btns);
        } else {
            bhs.setButtonsWithLastTen(this, ...btns);
        }
    }

    canDeactivate(): Observable<boolean> {
        return (this.inCancel && of(true)) || this.viewMode$.pipe(take(1),
            switchMap(view => (view === false && this.hasChanges()) ? this.cscs.open()
                .pipe(take(1),
                    switchMap(ok => ok ? this.autoSave() : of(ok === false))) : of(true)
            ));
    }

    ngAfterViewInit() {
        this.pfs.fieldsTmpl = this.fieldsErrorsTmpl;
    }

    protected setPageTitle() {
        const data = this.data;
        const birthDate = data.birthDateText;
        const age = data.age;
        let label = 'лет';
        if (age) {
            const end = age % 10;
            const hend = age <= 100 ? age : age % 100;
            if (hend < 10 || 20 < hend) {
                if (end === 1) {
                    label = 'год';
                } else if (1 < end && end < 5) {
                    label = 'года';
                }
            }
        }
        this.pt.setTitle([(data.lastName ? `${data.lastName} ` : '') +
            (data.firstName ? `${data.firstName} ` : '') + (data.middleName ? `${data.middleName}` : ''),
                ...(!birthDate ? [] : [birthDate.replace(/00\./g, '') +
                (data.deathDate ? ` - ${data.deathDate}` : '')])].join(', ') +
            ((age && !data.deathDate) ? ` (${age} ${label})` : '') + (data.actual === 0 ? '<span class="shild-not_actual"></span>' : ''));
    }

    protected hasChanges(): boolean {
        return true;
    }

    protected abstract runSave(): Observable<T>;

    protected setEditMode(mode = true) {
        if (!mode) {
            this.bhs.replaceButtons(false, {1: this.editButton});
            this.inCancel = false;
        }
    }

    protected stillCreate(data: any[]): Observable<any> {
        return of(true);
    }

    private autoSave(): Observable<boolean> {
        return this.runSave().pipe(switchMap(res => this.handleAutoSaveResult(res)));
    }

    private handleAutoSaveResult(res: any): Observable<any> {
        if (res) {
            if (Array.isArray(res)) {
                return this.stillCreate(res).pipe(switchMap(ok => {
                    if (ok === false) {
                        this.setEditMode();
                        return of(false);
                    } else {
                        return this.handleAutoSaveResult(ok);
                    }
                }));
            }
            return of(true);
        } else {
            this.setEditMode();
            return of(false);
        }
    }
}

