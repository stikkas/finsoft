import {Address, Document, DwellingPlace, IncapableDetails, InvalidDetails, SmevRequest, Training} from '../models';
import {nowStr} from '../functions';

export interface PfInitials {
    id: string;
    lastName: string;
    firstName: string;
    middleName: string;
    /**
     * Полных лет
     */
    age: number;

    /**
     * Дата рождения
     */
    birthDateText: string;

    /**
     * Актуальна ли запись
     * 1 - актуальна
     */
    actual: number;

    /**
     * Тип учета, если имеется
     */
    registered: any;

    /**
     * Дата смерти
     */
    deathDate: string;

    /**
     * Имеется или нет запись в таблице disability
     */
    disability: boolean;

    /**
     * ID актуальной семьи опеки
     */
    familyId: string;

    /**
     * ID семейного устройства
     */
    suId: number;
}

export interface ManInfo extends PfInitials {

    citizenshipId: string;
    nationalityId: string;
    sex: number;
    inn: string;
    snils: string;

    /**
     * Адрес рождения
     */
    birthAddress: Address;

    /**
     * Адреса регистрации и фактического места проживания
     */
    regs: DwellingPlace[];

    /**
     * Документы
     */
    docs: Document[];
}

/**
 * Данные в личном кабинете на вкладке "Межведомственные запросы"
 */
export interface SmevsInfo extends PfInitials {
    smevs: SmevRequest[];
}

export interface ZagsItem {
    id?: string;

    /**
     * Тип документа (свидетельство о браке или свидетельство о смерти)
     */
    type: string;
    typeId: string;

    /**
     * Дата
     */
    date?: string;

    /**
     * Дата регистрации
     */
    regDate?: string;

    /**
     * Дата выдачи
     */
    issueDate?: string;

    /**
     * Серия
     */
    series?: string;

    /**
     * Номер
     */
    nomer?: string;

    /**
     * Отдел ЗАГС
     * поле registryofficetext
     */
    whogive?: string;

    /**
     * Запись акта гражданского состояния №
     */
    actNomer?: string;

    /**
     * Супруг, исплользуется в "Свидетельстве о браке"
     */
    spouse?: string;
    spouseId?: string;

    /**
     * Код субъекта зарегистрировавшего АГС
     */
    orgId?: string;
}

export interface ZagsInfo extends PfInitials {
    certs: ZagsItem[];
}

export interface ChangeFio {
    id: string;
    lastName: string;
    firstName: string;
    middleName: string;
    /**
     * Пол
     */
    sex: string;
    birthDate: string;

    /**
     * начало периода события
     */
    startDate: string;

    /**
     * Перемена ФИО
     * окончания периода события
     */
    endDate: string;

    /**
     * актуальная запись
     */
    actual: number;

    /**
     * место рождения
     */
    birthPlace: string;

    /**
     * Документы
     */
    docs: Document[];
}

/**
 * Адрес проживания
 */
export interface ChangeAddress {
    /**
     * Регистрация
     */
    type: string;
    /**
     * Адрес
     */
    address: string;
    /**
     * Дата начала
     */
    startDate: string;
    /**
     * Дата окончания
     */
    endDate: string;
}

export interface ChangeInfo extends PfInitials {
    fios: ChangeFio[];
    docs: Document[];
    regs: ChangeAddress[];
}

export interface InvalidIncapable extends PfInitials {
    invalids: InvalidDetails[];
    incaps: IncapableDetails[];
}

export interface InvIncItem {
    invalid: InvalidDetails;
    incapable: IncapableDetails;
}

export class SocialStatus {
    id: number;

    /**
     * Название (из справочника)
     */
    statusId: string;
    statusName: string;

    /**
     * Дата установки
     */
    startDate: string;

    /**
     * Дата снятия
     */
    endDate: string;

    constructor(stId: string, stName: string) {
        this.statusId = stId;
        this.statusName = stName;
        this.startDate = nowStr();
    }
}

export class Education {
    id: number;

    /**
     * Название (из справочника)
     */
    typeId: string;

    /**
     * Дата сведений
     */
    infoDate: string;

    constructor(id: string) {
        this.typeId = id;
        this.infoDate = nowStr();
    }
}

export class JobInfo {
    id: number;

    /**
     * Место работы
     */
    place: string;

    /**
     * Должность
     */
    position: string;

    /**
     * Телефон 1
     */
    phone1: string;

    /**
     * Телефон 2
     */
    phone2: string;

    /**
     * E-mail
     */
    email: string;

    /**
     * Дата приема
     */
    startDate: string;

    /**
     * Дата увольнения
     */
    endDate: string;

    constructor() {
        this.startDate = nowStr();
    }
}

/**
 * Информация на вкладке "Дополнительные реквизиты"
 */
export interface AddedRequisites extends PfInitials {
    /**
     * Социальные статусы
     */
    states: SocialStatus[];

    /**
     * Образование
     */
    edus: Education[];

    /**
     * Обучение
     */
    trains: Training[];

    /**
     * Сведения о работе
     */
    jobs: JobInfo[];
}

export enum AddressTypeLabel {
    Birth = 'Место рождения',
    Reg = 'Место регистрации',
    Live = 'Место фактического проживания'
}

export enum AddressType {
    Reg = 'REG_ADDR', // Адрес регистрации
    Fact = 'FACT_ADDR' // Фактический адрес
}
