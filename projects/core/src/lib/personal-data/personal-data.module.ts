import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ManInfoComponent} from './man-info/man-info.component';
import {RegistrationsComponent} from './registrations/registrations.component';
import {ChangesDocComponent} from './changes-doc/changes-doc.component';
import {InvalidIncapableComponent} from './invalid-incapable/invalid-incapable.component';
import {ManinfoResolveService} from './services/maninfo-resolve.service';
import {ZagsComponent} from './zags/zags.component';
import {ZagsResolveService} from './services/zags-resolve.service';
import {InvIncResolveService} from './services/inv-inc-resolve.service';
import {ChangeResolveService} from './services/change-resolve.service';
import {AddedRequisitesComponent} from './added-requisites/added-requisites.component';
import {AddedReqResolveService} from './services/added-req-resolve.service';
import {SmevsResolveService} from './services/smevs-resolve.service';
import {SmevComponent} from './smev/smev.component';
import {NgbDatepickerModule, NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {PersonalDataRoutingModule} from './personal-data-routing.module';
import {PersonalDataComponent} from './personal-data.component';
import {BodyModule} from '../body/body.module';
import {PersonalService} from './services/personal.service';
import {LibComponentsModule} from '../components/lib-components.module';
import {FormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';

/**
 * Модуль создания личного дела
 */
@NgModule({
    declarations: [PersonalDataComponent,
        ManInfoComponent, AddedRequisitesComponent, SmevComponent,
        RegistrationsComponent, ZagsComponent, ChangesDocComponent, InvalidIncapableComponent],
    imports: [
        CommonModule,
        FormsModule,
        NgSelectModule,
        PersonalDataRoutingModule,
        NgbDatepickerModule,
        NgbDropdownModule,
        LibComponentsModule,
        BodyModule
    ],
    providers: [PersonalService, ManinfoResolveService, ZagsResolveService, InvIncResolveService,
        AddedReqResolveService, ChangeResolveService, SmevsResolveService]
})
export class PersonalDataModule {
}
