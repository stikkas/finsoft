import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap, take, tap} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbDateStruct, NgbDropdown} from '@ng-bootstrap/ng-bootstrap';
import {NgSelectComponent} from '@ng-select/ng-select';
import {PersonalDataAbstractComponent} from '../personal-data-abstract.component';
import {AddressType, AddressTypeLabel, ManInfo} from '../models';
import {Address, Dict, DictS, Document, DwellingPlace} from '../../models';
import {DictService} from '../../services/dict.service';
import {ConfirmRemoveComponentService} from '../../modals/confirm-remove/confirm-remove.component.service';
import {ChooseAddressComponentService} from '../../modals/choose-address/choose-address.component.service';
import {PersonalService} from '../services/personal.service';
import {HeadButtonsService} from '../../header/head-buttons.service';
import {ConfirmSaveComponentService} from '../../modals/confirm-save/confirm-save.component.service';
import {PageTitleService} from '../../services/page-title.service';
import {ViewEditService} from '../../services/view-edit.service';
import {bb, cls, context, daysBetween, geNow, getAddress, onDone, russiaId, toDate, urls} from '../../utils';
import {Title} from '@angular/platform-browser';
import {untilComponentDestroyed} from '@w11k/ngx-componentdestroyed';
import {nowStr} from '../../functions';


@Component({
    templateUrl: './man-info.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ManInfoComponent extends PersonalDataAbstractComponent<ManInfo> implements OnInit {
    private lastTerms = {} as any;
    private resRegType = 'RES_PLACE_REG';
    private regAtype: string;

    adrss = {
        [AddressTypeLabel.Birth]: new Subject<Address>(),
        [AddressTypeLabel.Reg]: new Subject<Address>(),
        [AddressTypeLabel.Live]: new Subject<Address>()
    };

    regs$ = new BehaviorSubject<DwellingPlace[]>([]);
    docs$ = new BehaviorSubject<Document[]>([]);

    regTableHeaders = ['Регистрация', 'Адрес', 'Дата начала', 'Дата окончания'];
    docTableHeaders = ['Документ*', 'Серия', 'Номер', 'Кем выдан', 'Код подразделения', 'Дата выдачи*'];

    dn = urls.api.dicts;
    atl = AddressTypeLabel;
    at = AddressType;
    ga = getAddress;

    viewMode$ = this.ves.viewMode$;
    docTypes$ = this.dictService.getDict(this.dn.dt);
    regTypes$ = this.dictService.getDict(this.dn.rt);
    adrType$ = this.dictService.getDict(this.dn.adrts);

    sex$ = this.dictService.getDict('sex');

    sexLabel$ = new BehaviorSubject<string>('label');
    csInput$ = new Subject<string>();
    citizenship$ = this.typeAhead(this.dn.ctzn, this.csInput$);

    wgInput$ = new BehaviorSubject<string>(null);
    whogive$ = this.typeAhead(this.dn.whogiv, this.wgInput$);
    whogives$: Observable<Dict[]>[] = [];

    nInput$ = new Subject<string>();
    nationality$ = this.typeAhead(this.dn.ntns, this.nInput$, true) as Observable<DictS[]>;

    doc: Document;
    curDoc: Document;
    wgInputs$: any[];
    curRegIdx: number;
    seriesMasks: { [key: number]: string };
    numberMasks: { [key: number]: string };
    adrTypeId: string;
    // что редактируем
    what: number;
    // Персональные данные для случая what === 1
    pd: ManInfo;
    files = [] as any[];

    constructor(ts: Title, pfs: PersonalService, cscs: ConfirmSaveComponentService, pageTitle: PageTitleService,
                ves: ViewEditService, bhs: HeadButtonsService, private dictService: DictService,
                private route: ActivatedRoute, router: Router, private rpcs: ChooseAddressComponentService,
                private crcs: ConfirmRemoveComponentService) {
        super(pfs, ves, ts, cscs, bhs, 'Основные реквизиты', pageTitle, [
            bb.save(() => this.save()),
            bb.del(() => this.remove()),
            bb.print(() => this.print())
        ], router, true, bb.medit([{
            title: 'Редактировать данные',
            click: () => this.edit(0)
        }, {
            title: 'Изменить персональные данные',
            click: () => this.edit(1)
        }, {
            title: 'Сменить документ',
            click: () => this.edit(2)
        }]));
        this.regTypes$.pipe(untilComponentDestroyed(this)).subscribe(res => {
            const it = res && res.find(i => i.value === this.resRegType);
            this.regAtype = it && it.label;
        });
    }

    ngOnInit() {
        this.route.data.pipe(untilComponentDestroyed(this)).subscribe(({info}) => {
            this.doc = {} as Document;
            this.wgInputs$ = [];
            this.curRegIdx = 0;
            this.seriesMasks = {};
            this.numberMasks = {};
            this.adrTypeId = undefined;
            this.pd = undefined;
            this.what = 0;
            this.data = info as ManInfo;
            this.capitalize(info, 'lastName', 'firstName', 'middleName');

            info.docs.forEach((it, i) => {
                const input = this.wgInputs$[i] = new BehaviorSubject<string>(null);
                this.whogives$[i] = this.typeAhead(this.dn.whogiv, input, false, i);
            });
            this.fillAdrTable();
            this.docs$.next([...info.docs]);
            this.setCurDoc();
            this.middleNameChanged(info, info.middleName, true);

            setTimeout(() => {
                this.setPageTitle();
                this.setEditMode(!this.data.id);
                if (info.citizenshipId) {
                    this.csInput$.next(info.citizenshipId);
                }
                if (info.nationalityId) {
                    this.nInput$.next(info.nationalityId);
                }
                this.adrss[AddressTypeLabel.Birth].next(info.birthAddress);
                if (!info.id) {
                    this.adrss[AddressTypeLabel.Reg].next(info.regs.find(it => it.addressType === AddressType.Reg));
                    this.adrss[AddressTypeLabel.Live].next({levels: [], addressType: AddressType.Fact} as Address);
                }
            });
        });
    }

    setAddress(address: DwellingPlace, type: AddressTypeLabel, idx = -1) {
        const data = this.data;
        if (type === AddressTypeLabel.Birth) {
            data.birthAddress = address;
        } else {
            if (idx > -1) { // редактируем уже существующий адрес
                data.regs[idx] = address;
            } else if (!data.id) {
                const i = data.regs.findIndex(it => it.addressType === address.addressType);
                if (i > -1) {
                    data.regs[i] = address;
                } else {
                    data.regs.push(address);
                }
            } else {
                const i = data.regs.findIndex(it => it.addressType === address.addressType
                    && (it.regType === address.regType || (!it.regType && !address.regType)) && !it.endDate);
                if (i > -1) {
                    const date = toDate(address.startDate);
                    if (date) {
                        data.regs[i].endDate = nowStr(new Date(date.getTime() - 24 * 60 * 60000));
                    }
                }
                data.regs.push(address);
            }
            this.fillAdrTable();
        }
        this.adrss[type].next(address);
    }

    /**
     * Когда изменяется пол, нужно поменять написание национальности
     * @param value - Dict
     * @param init - вызывается при инициализации
     */
    sexChanged(value: any, init = false) {
        this.sexLabel$.next(value.value ? 'short' : 'label');
        if (!init && this.data.nationalityId) {
            const v = this.lastTerms[this.dn.ntns];
            this.nInput$.next(null);
            setTimeout(() => this.nInput$.next(v), 200);
        }
    }

    /**
     * Проставляем пол в соответствии с отчеством. Если заканчивается на 'ч' или 'оглы' - мужской, иначе - женский
     * @param data - значения в поле ввода отчества, срабатывает на каждый символ
     * @param force - запускаем смену пола при инициализации, обязательно
     */
    middleNameChanged(obj: ManInfo, data: string, force = false) {
        const d = data && data.substr(-4);
        if (d) {
            const sfx = d.toLowerCase();
            const sex = (sfx.endsWith('ч') || sfx.endsWith('оглы')) ? 0 : 1;
            if (obj.sex !== sex || force) {
                obj.sex = sex;
                this.sexChanged({value: sex}, force);
            }
        }
    }

    openPlace(type: AddressTypeLabel, place: DwellingPlace, typeId?: string, idx = -1) {
        if (place && place.addressType === AddressType.Reg && !place.regTypeStr) {
            place.regTypeStr = this.regAtype;
            place.regType = this.resRegType;
            place.startDate = nowStr();
        }
        onDone(this.rpcs.open(type,
                ((type === AddressTypeLabel.Reg || type === AddressTypeLabel.Live) &&
                    !(place.id || place.levels.length || place.countryId || place.addressText)) ?
                    this.pfs.getRootAddress(place) : {...place, countryId: place.countryId || russiaId},
                type === AddressTypeLabel.Birth, true, undefined, undefined, undefined, typeId),
            res => this.setAddress(res, type, idx));
    }

    chooseAdr(sel: NgSelectComponent) {
        if (this.adrTypeId) {
            let label = AddressTypeLabel.Reg;
            let addressType = AddressType.Reg;
            if (this.adrTypeId !== cls.radr) {
                label = AddressTypeLabel.Live;
                addressType = AddressType.Fact;
            }
            this.openPlace(label, (!this.data.id && this.data.regs.find(it => it.addressType === addressType)) || {
                levels: [],
                addressType
            }, this.adrTypeId);
            sel.writeValue(null);
            this.adrTypeId = null;
        }
    }

    /**
     * Копирует адрес места регистрации в место фактического проживания
     */
    copyAddress() {
        const data = this.data;
        const address = data.id ? data.regs[this.curRegIdx] :
            data.regs.find(it => it.addressType === AddressType.Reg);
        if (address && (address.levels.length || address.addressText || address.countryId || address.countryText)) {
            const copy = {
                ...address, levels: address.levels.map(it => ({...it})),
                id: null, regType: null, startDate: null, endDate: null, eventDate: null,
                regTypeStr: null, addressType: AddressType.Fact, startDateStr: null, endDateStr: null
            };
            this.setAddress(copy, AddressTypeLabel.Live);
        }
    }

    editAdr(ctxm: NgbDropdown, dp: DwellingPlace, idx: number) {
        ctxm.close();
        this.openPlace(dp.addressType === AddressType.Reg ? AddressTypeLabel.Reg : AddressTypeLabel.Live, dp,
            dp.addressType === AddressType.Reg ? cls.radr : cls.ladr, idx);
    }

    removeAdr(ctxm: NgbDropdown, idx: number) {
        ctxm.close();
        onDone(this.crcs.open(), () => {
            const adr = this.data.regs.splice(idx, 1)[0];
            const old = this.data.regs.find(it => it.addressType === adr.addressType && it.endDate && adr.startDate
                && daysBetween(it.endDate, adr.startDate) === 1);
            if (old) {
                old.endDate = undefined;
            }
            this.fillAdrTable();
        });
    }

    /**
     * Делает первую букву заглавной для заданного поля объекта data
     * @param field - название поля
     */
    capitalize(data: ManInfo, ...fields: string[]) {
        fields.forEach(field => {
            const val = data[field] as string;
            if (val && val.length) {
                data[field] = val[0].toUpperCase() + val.substr(1).toLowerCase();
            }
        });
    }

    /**
     * Переключает фокус на нужный элемент, использование tabindex для этой цели - антипатерн
     * @param ev - событие при нажатии на tab
     * @param el - элемент на который нужно переключится
     * @param child - нужно фокусироваться на ребенке. когда элемент может поменяться, тогда вешаемся на родителя, а потом уже
     *  по факту фокусируемся на том ком нужно.
     */
    next(ev: KeyboardEvent, el: HTMLElement | NgSelectComponent, child = false) {
        ev.preventDefault();
        if (child) {
            (el as any).children[0].focus();
        } else {
            el.focus();
        }
    }

    /**
     * При смене типа документа нужно поменять маску
     * @param data - значение типа докумена
     * @param doc - документ
     * @param i - индекс документа в data.docs
     */
    docTypeChanged(data: any, doc: Document, i: number) {
        if (data) {
            this.numberMasks[i] = data.number;
            this.seriesMasks[i] = data.series;
            const input = (doc === this.doc ? this.wgInput$ : this.wgInputs$[i]);
            input.next(input.getValue() === null ? '' : null);
        } else {
            this.numberMasks[i] = this.seriesMasks[i] = null;
        }
        // удаляем данные, т.к. они явно не подходят под маску
        doc.series = null;
        doc.number = null;
    }

    /**
     * Для полей у которых есть маска, последний символ, который выходит за пределы маски, записывается в модель,
     * (решения этого косяка в инете не нашлось)
     * чтобы этого не происходило не используется двойной биндинг, а используется одинарный, обратная запись в модель
     * происходит на change событие этим методом
     * Для даты и где динамическая маска вроде этого не происходит, поэтому там стандартный двойной биндинг
     * @param ev - событие ввода
     * @param obj - объект, в котором храним данные
     * @param field - поле этого объекта для записи данных
     */
    inputChanged(ev: any, obj: any, field: string) {
        obj[field] = ev.target.value;
    }

    birthDateChanged(data: ManInfo, event: any) {
        if (event && event.target) {
            data.birthDateText = event.target.value;
        } else if (event && event.year) {
            const d = event as NgbDateStruct;
            data.birthDateText = [(d.day < 10 ? '0' : '') + d.day, (d.month < 10 ? '0' : '') + d.month, d.year].join('.');
        } else {
            data.birthDateText = '';
        }
    }

    noFactAddress() {
        return !this.data.regs.some(it => it.addressType === AddressType.Fact);
    }

    /**
     * Возвращает значение поля единицы справочника Observable<string>
     * @param id - идентификатор единицы справочника
     * @param dict - справочник
     * @param field - интересующее поле
     */
    getLabel(id: string, dict = this.dn.rt, field = 'label') {
        return this.dictService.getLabel(id, dict, field);
    }

    private fillAdrTable() {
        this.regs$.next(this.data.regs.filter(it => !it.endDate || geNow(it.endDate)));
    }

    /**
     * Проверяет есть ли заполненные данные в документе
     */
    private hasDocData() {
        const doc = this.doc;
        return doc.typeId || doc.series || doc.number || doc.issueDate || doc.whogive || doc.depCode ||
            (this.files && this.files.some(it => it[0] === doc));
    }

    /**
     * Создает генератор значений для селектора, который использует автоподсказку
     * @param dict - название словаря
     * @param subject - Subjectable, которые представляет собой ввод с клавиатуры
     * @param save - сохранять или нет последний ненулевой ввод в this.lastTerms, это нужно когда необходимо повторить запрос программно
     * @param i - индекс в списке документов
     */
    private typeAhead(dict: string, subject: Subject<string>, save = false, i?: number) {
        return subject.pipe(
            tap(it => save && it && (this.lastTerms[dict] = it)),
            debounceTime(200),
            distinctUntilChanged(),
            switchMap(term => {
                const typeId = dict === this.dn.whogiv && (i !== undefined ? this.data.docs[i].typeId : this.doc.typeId);
                return this.dictService.loadOnce(dict, typeId ? {query: term || '', typeId} : term);
            }));
    }


    /**
     * Удаляет карточку
     */
    private remove() {
        onDone(this.crcs.open(),
            () => this.pfs.remove(this.data.id).subscribe(res => {
                if (!res) {
                    this.router.navigate(['/']);
                }
            }));
    }

    /**
     * Устанавливаем режим редактирования
     * @mode - если true - все поля доступны для редактирования, иначе только просмотр
     */
    protected setEditMode(mode = true) {
        super.setEditMode(mode);
        if (mode) {
            this.ves.edit();
            this.bhs.setDisabled([1, !this.data.id, , this.data.id ? 0 : 1]);
            const data = this.data;
            if (data && !this.what) {
                setTimeout(() => {
                    data.docs.forEach((it, i) => {
                        if (it.whogive) {
                            this.wgInputs$[i].next(it.whogive);
                        }
                    });
                });
            }
        } else {
            this.ves.view();
            this.bhs.setDisabled([, !this.data.actual, 1, !this.data.actual]);
        }
    }

    protected stillCreate(data: any[]): Observable<any> {
        return this.cscs.open(`Человек ${data[0].lastName} ${data[0].firstName} ${data[0].middleName} ${data[0].birthDate}
                                            найден в базе данных. Продолжить создание нового личного дела?`, 'Да')
            .pipe(take(1), switchMap(ok => {
                if (ok) {
                    return this.pfs.save(this.data, !this.data.docs.length && this.hasDocData() && this.doc, this.what,
                        this.files.length && this.files, true);
                } else if (ok === false) {
                    // Открыть в соседних вкладках все найденные личные дела и перейти на соседнюю вкладку
                    data.forEach((it, i) => {
                        setTimeout(() => {
                            const link = document.createElement('a');
                            link.href = `${context.app}/pf/${it.id}/man-info`;
                            link.target = '_blank';
                            link.click();
                        }, i * 500);
                    });
                }
                return of(false);
            }));
    }

    protected runSave() {
        this.bhs.setDisabled([1, 1, 1, 1, 1]);
        return this.pfs.save(this.what === 0 ? this.data : this.pd,
            this.what === 0 ? (!this.data.docs.length && this.hasDocData() && this.doc) : this.doc,
            this.what, this.files.length && this.files);
    }

    private edit(what: number) {
        this.what = what;
        if (what) {
            this.doc = {} as Document;
            this.pd = {id: this.data.id, docs: []} as ManInfo;
        }
        this.bhs.replaceButtons(false, {1: this.cancelButton});
        this.setEditMode();
    }

    /**
     * Сохраняет данные
     */
    private save() {
        this.runSave().subscribe(res => this.handleSaveResult(res));
    }

    private setCurDoc() {
        this.curDoc = (this.data.docs && this.data.docs.length) ? this.data.docs[0] : {} as Document;
    }

    private handleSaveResult(res: ManInfo) {
        if (!res || typeof res === 'string') {
            this.setEditMode();
        } else if (res && !Array.isArray(res)) {
            this.setEditMode(false);
            if (this.data.id !== res.id || this.what !== 0) {
                this.router.navigate(['/pf', res.id, 'man-info']);
            } else {
                const data = this.data;
                data.docs = res.docs;
                this.docs$.next(data.docs);
                this.setCurDoc();
                res.regs.forEach((it, i) => {
                    const reg = data.regs[i];
                    reg.id = it.id;
                    reg.flat = it.flat;
                    reg.regId = it.regId;
                    reg.startDate = it.startDate;
                });
                data.lastName = res.lastName;
                data.middleName = res.middleName;
                data.firstName = res.firstName;
            }
        } else if (res && Array.isArray(res)) {
            this.stillCreate(res).subscribe(ok => {
                if (ok === false) {
                    this.setEditMode();
                } else {
                    this.handleSaveResult(ok);
                }
            });
        }
    }


    /**
     * Печатает чего-то
     */
    private print() {

    }
}



