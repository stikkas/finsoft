import {ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {PersonalDataAbstractComponent} from '../personal-data-abstract.component';
import {ConfirmSaveComponentService} from '../../modals/confirm-save/confirm-save.component.service';
import {InvalidIncapable, InvIncItem} from '../models';
import {ConfirmRemoveComponentService} from '../../modals/confirm-remove/confirm-remove.component.service';
import {ViewEditService} from '../../services/view-edit.service';
import {PageTitleService} from '../../services/page-title.service';
import {PersonalService} from '../services/personal.service';
import {HeadButtonsService} from '../../header/head-buttons.service';
import {IncapableDetails, InvalidDetails} from '../../models';
import {bb, onDone} from '../../utils';
import {Title} from '@angular/platform-browser';
import {untilComponentDestroyed} from '@w11k/ngx-componentdestroyed';

@Component({
    templateUrl: './invalid-incapable.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvalidIncapableComponent extends PersonalDataAbstractComponent<InvalidIncapable> implements OnInit {

    private shareHeaders = ['Дата установки', 'Серия', 'Номер', 'Кем выдан', 'Дата выдачи'];
    invHeaders = ['Документ, устанавливающий инвалидность', ...this.shareHeaders];
    incHeaders = ['Документ, устанавливающий недееспособность', ...this.shareHeaders];

    // Флаги удаления
    invProc$ = new Subject();
    incProc$ = new Subject();

    curInv: InvalidDetails;
    invs$ = new BehaviorSubject<InvalidDetails[]>(null);
    curInc: IncapableDetails;
    incs$ = new BehaviorSubject<IncapableDetails[]>(null);

    constructor(pfs: PersonalService, ves: ViewEditService, ts: Title, router: Router,
                private crcs: ConfirmRemoveComponentService, private route: ActivatedRoute,
                cscs: ConfirmSaveComponentService, bhs: HeadButtonsService, pt: PageTitleService) {
        super(pfs, ves, ts, cscs, bhs, 'Инвалидность / Недееспособность', pt,
            [bb.save(() => this.save()), bb.print(() => this.print())], router);
    }

    ngOnInit() {
        this.route.data.pipe(untilComponentDestroyed(this)).subscribe(({info}) => {
            this.data = info as InvalidIncapable;
            this.invs$.next([...info.invalids]);
            this.incs$.next([...info.incaps]);
            this.setCurInv();
            this.setCurInc();
            setTimeout(() => {
                this.setPageTitle();
                this.setEditMode(false);
            });
        });
    }

    addInvalid() {
        const cert = {invDoc: {}} as InvalidDetails;
        this.data.invalids.push(cert);
        this.invs$.next([...this.data.invalids]);
        this.setCurInv(cert);
        this.bhs.replaceButtons(false, {1: this.cancelButton});
        this.setEditMode();
    }

    addIncapable() {
        const cert = {incDoc: {}} as IncapableDetails;
        this.data.incaps.push(cert);
        this.incs$.next([...this.data.incaps]);
        this.setCurInc(cert);
        this.bhs.replaceButtons(false, {1: this.cancelButton});
        this.setEditMode();
    }

    setCurInv(invalid?: InvalidDetails) {
        if (!this.curInv || this.curInv !== invalid) {
            this.curInv = invalid || (this.data.invalids.length && this.data.invalids[0]);
        }
    }

    setCurInc(incapable?: IncapableDetails) {
        if (!this.curInc || this.curInc !== incapable) {
            this.curInc = incapable || (this.data.incaps.length && this.data.incaps[0]);
        }
    }

    removeInv(item: InvalidDetails, idx: number) {
        onDone(this.crcs.open(), () => {
                if (item.invalidityId) {
                    this.invProc$.next(true);
                    this.pfs.removeInvalidity(item.invalidityId).subscribe(res => {
                        this.invProc$.next(false);
                        if (res) {
                            this.removeLocalInv(idx);
                        }
                    });
                } else {
                    this.removeLocalInv(idx);
                }
            }
        );
    }

    removeInc(item: IncapableDetails, idx: number) {
        onDone(this.crcs.open(), () => {
                if (item.disabilityId) {
                    this.incProc$.next(true);
                    this.pfs.removeIncapability(item.disabilityId).subscribe(res => {
                        this.incProc$.next(false);
                        if (res) {
                            this.removeLocalInc(idx);
                        }
                    });
                } else {
                    this.removeLocalInc(idx);
                }
            }
        );
    }

    protected runSave(): Observable<any> {
        this.bhs.setDisabled([1, 1, 1, 1]);
        return this.pfs.saveInvInc(this.curInv, this.curInc, this.data.id);
    }

    protected setEditMode(mode = true): void {
        super.setEditMode(mode);
        if (mode) {
            this.ves.edit();
            this.bhs.setDisabled([, , , 0]);
        } else {
            this.ves.view();
            this.bhs.setDisabled([, !(this.curInv || this.curInc) || !this.data.actual, 1, 0]);
        }
    }

    private save() {
        this.runSave().subscribe((res: InvIncItem) => {
            if (res) {
                this.setEditMode(false);
                if (res.invalid) {
                    this.curInv.invalidityId = res.invalid.invalidityId;
                }
                if (res.incapable) {
                    this.curInc.disabilityId = res.incapable.disabilityId;
                }
            } else {
                this.setEditMode();
            }
        });
    }

    private print() {

    }

    private removeLocalInv(idx: number) {
        this.data.invalids.splice(idx, 1);
        this.invs$.next([...this.data.invalids]);
        this.setCurInv();
        if (!this.curInc) {
            this.setEditMode(false);
        }
    }

    private removeLocalInc(idx: number) {
        this.data.incaps.splice(idx, 1);
        this.incs$.next([...this.data.incaps]);
        this.setCurInc();
        if (!this.curInv) {
            this.setEditMode(false);
        }
    }
}
