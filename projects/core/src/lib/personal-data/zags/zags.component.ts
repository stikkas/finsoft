import {ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, switchMap} from 'rxjs/operators';
import {ZagsInfo, ZagsItem} from '../models';
import {PersonalDataAbstractComponent} from '../personal-data-abstract.component';
import {PersonalService} from '../services/personal.service';
import {DictService} from '../../services/dict.service';
import {ConfirmSaveComponentService} from '../../modals/confirm-save/confirm-save.component.service';
import {ViewEditService} from '../../services/view-edit.service';
import {Dict, LdFilter, LdTableRow} from '../../models';
import {ConfirmRemoveComponentService} from '../../modals/confirm-remove/confirm-remove.component.service';
import {PageTitleService} from '../../services/page-title.service';
import {HeadButtonsService} from '../../header/head-buttons.service';
import {bb, cls, onDone, urls} from '../../utils';
import {Title} from '@angular/platform-browser';
import {PeopleSearchComponentService} from '../../modals/people-search/people-search.component.service';
import {untilComponentDestroyed} from '@w11k/ngx-componentdestroyed';

@Component({
    templateUrl: './zags.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ZagsComponent extends PersonalDataAbstractComponent<ZagsInfo> implements OnInit {
    private dn = urls.api.dicts;

    certs$ = this.ds.getDict(this.dn.zcert);
    curCert: ZagsItem;
    cur$ = new BehaviorSubject<ZagsItem>(null);
    items$ = new BehaviorSubject<ZagsItem[]>(null);
    rproc$ = new Subject();
    wgInput$ = new Subject<string>();
    whogive$ = this.wgInput$.pipe(debounceTime(200), distinctUntilChanged(),
        switchMap(term => this.ds.getDict(this.dn.zags).pipe(map(res => {
                const regExp = new RegExp(term, 'i');
                return res && (term ? res.filter(it => regExp.test(it.label)) : res);
            }
        ))));
    cls = cls;
    headers = ['Наименование документа', 'Дата регистрации', 'Серия', 'Номер', 'Отдел ЗАГС', 'Дата выдачи'];

    constructor(ts: Title, pfs: PersonalService, ves: ViewEditService, private pscs: PeopleSearchComponentService,
                private crcs: ConfirmRemoveComponentService, private ds: DictService, router: Router,
                private route: ActivatedRoute, cscs: ConfirmSaveComponentService,
                hbs: HeadButtonsService, pt: PageTitleService) {
        super(pfs, ves, ts, cscs, hbs, 'Сведения ЗАГС', pt,
            [bb.save(() => this.save()), bb.print(() => this.print())], router);
    }

    ngOnInit() {
        this.route.data.pipe(untilComponentDestroyed(this)).subscribe(({info}) => {
            this.data = info as ZagsInfo;
            this.items$.next([...info.certs]);
            this.setCurrent();
            setTimeout(() => {
                this.setPageTitle();
                this.setEditMode(false);
            });
        });
    }

    createCert(type: Dict) {
        const cert = {type: type.label, typeId: type.value as string};
        this.data.certs.push(cert);
        this.items$.next([...this.data.certs]);
        this.setCurrent(cert);
        this.bhs.replaceButtons(false, {1: this.cancelButton});
        this.setEditMode();
    }

    setCurrent(cert?: ZagsItem) {
        if (!this.curCert || this.curCert !== cert) {
            this.curCert = cert = cert || (this.data.certs.length && this.data.certs[0]);
            this.cur$.next(cert);
            if (cert && cert.whogive) {
                setTimeout(() => {
                    this.wgInput$.next(cert.whogive);
                });
            }
        }
    }

    dateChanged(date: any) {
        if (date && !this.curCert.issueDate) {
            this.curCert.issueDate = date;
        }
    }

    remove(item: ZagsItem, idx: number) {
        onDone(this.crcs.open(), () => {
                if (item.id) {
                    this.rproc$.next(true);
                    this.pfs.removeZags(item.id).subscribe(res => {
                        this.rproc$.next(false);
                        if (res) {
                            this.removeLocal(idx);
                        }
                    });
                } else {
                    this.removeLocal(idx);
                }
            }
        );
    }

    nomerChanged(event: any, item: ZagsItem) {
        item.nomer = event.target.value;
    }

    spouse(cert: ZagsItem) {
        onDone(this.pscs.open(new LdFilter(undefined, undefined, false)), (it: LdTableRow) => {
            cert.spouse = it.fio;
            cert.spouseId = it.id;
            this.cur$.next(cert);
        });
    }

    zagsChanged(dict: any, item: ZagsItem) {
        if (dict) {
            if (typeof dict === 'string') {
                item.whogive = dict;
                item.orgId = undefined;
            } else {
                item.whogive = dict.label;
                item.orgId = dict.value;
            }
        } else {
            item.whogive = item.orgId = undefined;
        }
    }

    protected setEditMode(mode = true) {
        super.setEditMode(mode);
        if (mode) {
            this.ves.edit();
            this.bhs.setDisabled([, , , 0]);
        } else {
            this.ves.view();
            this.bhs.setDisabled([, !this.curCert || !this.data.actual, 1, 0]);
        }
    }

    protected runSave(): Observable<any> {
        this.bhs.setDisabled([1, 1, 1, 1]);
        return this.pfs.saveZags(this.curCert, this.data.id);
    }

    private save() {
        this.runSave().subscribe((res: ZagsItem) => {
            if (res) {
                this.setEditMode(false);
                if (this.curCert.typeId === cls.dthc) {
                    this.data.deathDate = this.curCert.date;
                    this.setPageTitle();
                }
                this.curCert.id = res.id;
                if (!this.curCert.regDate) {
                    this.curCert.regDate = res.regDate;
                }
            } else {
                this.setEditMode();
            }
        });
    }

    private removeLocal(idx: number) {
        this.data.certs.splice(idx, 1);
        this.items$.next([...this.data.certs]);
        if (this.data.deathDate && !this.data.certs.some(it => it.typeId === cls.dthc)) {
            this.data.deathDate = null;
            this.setPageTitle();
        }
        this.setCurrent();
        this.setEditMode(false);
    }

    private print() {

    }

}
