import {Component, ViewEncapsulation} from '@angular/core';
import {Menu, OpekaRole, RegisteredType} from '../models';
import {PersonalService} from './services/personal.service';
import {context, isChild} from '../utils';
import {Observable} from 'rxjs';
import {skipWhile, take, tap} from 'rxjs/operators';
import {ConfirmInfoComponentService} from '../modals/confirm-info/confirm-info.component.service';
import {MeService} from '../services/me.service';

@Component({
    template: `
        <fi-body [menus]="menus" [id]="(cur$|async)?.id"></fi-body>`,
    encapsulation: ViewEncapsulation.None
})
export class PersonalDataComponent {
    menus: Menu[];
    mainSpec: boolean;
    cur$: Observable<any>;

    constructor(pfs: PersonalService, cics: ConfirmInfoComponentService, me: MeService) {
        this.menus = [{
            title: 'Данные о&nbsp;человеке', actions: [{
                url: 'man-info',
                title: 'Основные реквизиты',
                default: true
            }, {
                url: 'zags',
                title: 'Сведения ЗАГС'
            }, {
                url: 'changes-document',
                title: 'Изменение ФИО / ДУЛ / адреса'
            }, {
                url: 'invalid-incapable',
                title: 'Инвалидность / Недееспособность'
            }, {
                url: 'added-req',
                title: 'Дополнительные реквизиты'
            }, {
                url: 'smevs',
                title: 'Межведомственные запросы'
            }]
        }];
        if (context.aisId === 3) { // БДДСОП
            this.menus.push({
                title: 'Дополнительные данные', actions: [{
                    url: '/reg',
                    title: 'Учет'
                }, {
                    url: '/bf/biological-family',
                    title: 'Семья несовершеннолетнего'
                }]
            });
            this.cur$ = pfs.current$;
        } else if (context.aisId === 4) { // Опека
            me.getUser().pipe(skipWhile(it => !it), take(1)).subscribe(user => {
                this.mainSpec = user.roleId === OpekaRole.MainSpecialist || user.roleId === OpekaRole.Razrab;
            });
            this.menus.push({
                title: 'Дополнительные данные', actions: [{
                    url: '/bf/biological-family',
                    title: 'Биологическая семья'
                }, {
                    url: '',
                    title: 'Семья опеки',
                    withoutId: true
                }, {
                    url: '/reg',
                    title: 'Учет'
                }]
            });
            this.cur$ = pfs.current$.pipe(tap(res => {
                const action = this.menus[1].actions[2];
                action.beforeNav = null;
                if (res.actual) {
                    if (!isChild(res.bd)) {
                        if (res.registered === RegisteredType.Underage) {
                            // Теперь на учете для несовершеннолетних могут стоять и совершеннолетние
                            action.queryParams = {type: RegisteredType.Underage};
                        } else if (res.dis || res.registered === RegisteredType.Incapable) {
                            action.queryParams = {type: RegisteredType.Incapable};
                        } else if (res.registered === RegisteredType.Patron) {
                            action.queryParams = {type: RegisteredType.Patron};
                        } else {
                            action.beforeNav = () => {
                                cics.open('На учет можно поставить только недееспособного гражданина.');
                                return false;
                            };
                        }
                    } else {
                        action.queryParams = {type: RegisteredType.Underage};
                        if (!this.mainSpec) {
                            action.beforeNav = () => pfs.lastEventNotAdopt(res.id);
                        }
                    }
                } else {
                    action.beforeNav = () => false;
                }
                const opeka = this.menus[1].actions[1];
                opeka.beforeNav = null;
                if (res.suId) {
                    if (res.fid) {
                        opeka.url = ['/family-op', res.fid, 'main'];
                    }
                    opeka.queryParams = {suId: res.suId};
                } else {
                    opeka.beforeNav = () => false;
                }
            }));
        }
    }
}

