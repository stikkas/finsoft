import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {PersonalDataAbstractComponent} from '../personal-data-abstract.component';
import {ChangeInfo} from '../models';
import {PersonalService} from '../services/personal.service';
import {ConfirmSaveComponentService} from '../../modals/confirm-save/confirm-save.component.service';
import {MeService} from '../../services/me.service';
import {ViewEditService} from '../../services/view-edit.service';
import {PageTitleService} from '../../services/page-title.service';
import {HeadButtonsService} from '../../header/head-buttons.service';
import {bb, geNow} from '../../utils';
import {Title} from '@angular/platform-browser';

@Component({
    templateUrl: './changes-doc.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ChangesDocComponent extends PersonalDataAbstractComponent<ChangeInfo> implements OnInit {
    fioHeaders = ['Фамилия', 'Имя', 'Отчество', 'Пол', 'Дата<br>рождения', 'Место рождения', 'Дата<br>начала'];
    docHeaders = ['Документ', 'Серия', 'Номер', 'Кем выдан', 'Код<br>подразд.', 'Дата<br>выдачи'];
    regHeaders = ['Регистрация', 'Адрес', 'Дата<br>начала', 'Дата<br>окончания'];

    constructor(ts: Title, pfs: PersonalService, ves: ViewEditService, me: MeService, private route: ActivatedRoute,
                cscs: ConfirmSaveComponentService, bhs: HeadButtonsService, pt: PageTitleService, router: Router) {
        super(pfs, ves, ts, cscs, bhs, 'Изменение ФИО / ДУЛ / адреса', pt, [bb.print(() => this.print())], router, false);
    }

    ngOnInit() {
        this.data = this.route.snapshot.data.info as ChangeInfo;
        setTimeout(() => {
            this.setPageTitle();
            this.setEditMode(false);
        });
    }

    noactive(date: string): boolean {
        return !(!date || geNow(date));
    }

    toLd(id: string) {
        this.router.navigate(['pf', id, 'man-info']);
    }

    protected runSave(): Observable<any> {
        return of({});
    }

    protected setEditMode(mode = true) {
        if (mode) {
            this.ves.edit();
            this.bhs.setDisabled([, 0]);
        } else {
            this.ves.view();
            this.bhs.setDisabled([, 0]);
        }
    }

    private print() {
    }

}
