// Чтобы исключить циклическую зависимость создаю этот файл

/**
 * Возвращает текущую дату или дату переданную как параметр,  строкой
 */
export function nowStr(date?: Date): string {
    const now = date || new Date();
    const month = now.getMonth() + 1;
    const day = now.getDate();
    return [day < 10 ? `0${day}` : day, month < 10 ? `0${month}` : month, now.getFullYear()].join('.');
}

