import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BodyComponent} from './body.component';
import {RouterModule} from '@angular/router';
import {NotifierModule} from 'angular-notifier';

/**
 * Костяк основной части всех страниц, кроме основной ({MainComponent})
 */
@NgModule({
    declarations: [BodyComponent],
    imports: [CommonModule, RouterModule,
        NotifierModule.withConfig({
            behaviour: {
                autoHide: 3000
            },
            position: {
                horizontal: {
                    position: 'right'
                },
                vertical: {
                    position: 'top'
                }
            }
        })
    ],
    exports: [BodyComponent]
})
export class BodyModule {
}
