import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {Menu, MenuAction} from '../models';
import {PageTitleService} from '../services/page-title.service';
import {ViewEditService} from '../services/view-edit.service';

@Component({
    selector: 'fi-body',
    templateUrl: './body.component.html',
    encapsulation: ViewEncapsulation.None
})
export class BodyComponent {
    title$ = this.pageTitle.title;
    view$ = this.ves.viewMode$;

    @Input() menus: Menu[];
    @Input() id: string;
    @Input() notitle: boolean;
    // Кнопка наверх
    @Input() upbtn: boolean;
    @Output() toUp = new EventEmitter();
    @Output() activate = new EventEmitter();

    constructor(private pageTitle: PageTitleService, private ves: ViewEditService, private router: Router) {
        pageTitle.setTitle('');
    }

    /**
     * Переход по ссылкам левого меню.
     * @param target - куда нужно перейти
     * @param id - идентификатор
     */
    navigate(target: MenuAction, id?: string) {
        if (target.beforeNav) {
            const res = target.beforeNav();
            if (typeof res === 'boolean') {
                if (res) {
                    this.goTo(target, id);
                }
            } else if (res && res.subscribe) {
                res.subscribe(ok => ok && this.goTo(target, id));
            }
        } else {
            this.goTo(target, id);
        }
    }


    /**
     * Возвращает класс стиля для текущего пути
     * @param urls - хвост пути
     * @param main - является ли роутинг по умолчанию
     */
    currentCls(urls: string | string[], main?: boolean, fragment?: string) {
        const path = window.location.pathname;
        const cls = 'sidebar-menu__item_state_active';
        const url = Array.isArray(urls) ? urls.join('/') : urls;
        if ((path.endsWith(`/${url}`) || path === url) && (fragment ? window.location.hash === `#${fragment}` : true)) {
            return cls;
        } else {
            const id = path.substr(path.lastIndexOf('/') + 1);
            return (id === this.id || !id) && main ? cls : '';
        }
    }

    /**
     * Возвращает массив фрагментов роутера
     * @param id - основной идентификатор
     * @param url - путь из меню, может быть строкой или массивом фрагментов
     */
    getLink(id: string, url: string | string[]) {
        return [...(id ? [id] : []), ...(Array.isArray(url) ? url : [url])];
    }

    scrollUp() {
        this.toUp.emit();
    }


    /**
     * Только переход, без всяких проверок
     * @param target - куда нужно перейти
     * @param id - идентификатор
     * @private
     */
    private goTo(target: MenuAction, id: string) {
        const commands = (typeof target.url === 'string') ? [target.url] : [...target.url];
        if (id) {
            commands.push(id);
        }
        if (target.tail) {
            commands.push(target.tail);
        }
        let extras;
        if (target.queryParams || target.fragment) {
            extras = {};
            if (target.queryParams) {
                extras.queryParams = target.queryParams;
            }
            if (target.fragment) {
                extras.fragment = target.fragment;
            }
        }
        this.router.navigate(commands, extras);
    }
}


