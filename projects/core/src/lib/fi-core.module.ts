import {LOCALE_ID, ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {NgbDatepickerConfig, NgbDatepickerModule, NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule} from '@angular/common';
import {ChooseAddressComponent} from './modals/choose-address/choose-address.component';
import {ClsSearchComponent} from './modals/cls-search/cls-search.component';
import {ConfirmInfoComponent} from './modals/confirm-info/confirm-info.component';
import {ConfirmRemoveComponent} from './modals/confirm-remove/confirm-remove.component';
import {ConfirmSaveComponent} from './modals/confirm-save/confirm-save.component';
import {ModalComponent} from './modals/modal/modal.component';
import {OrgSearchComponent} from './modals/org-search/org-search.component';
import {PeopleSearchComponent} from './modals/people-search/people-search.component';
import {LibComponentsModule} from './components/lib-components.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {NgSelectConfig, NgSelectModule} from '@ng-select/ng-select';
import {NgxSmartModalModule} from 'ngx-smart-modal';
import {TreeModule} from '@circlon/angular-tree-component';
import {RedirectInterceptor} from './services/redirect-interceptor';
import {BodyModule} from './body/body.module';
import {HeaderModule} from './header/header.module';
import {FooterModule} from './footer/footer.module';

@NgModule({
    declarations: [
        ChooseAddressComponent,
        ClsSearchComponent,
        ConfirmInfoComponent,
        ConfirmRemoveComponent,
        ConfirmSaveComponent,
        ModalComponent,
        OrgSearchComponent,
        PeopleSearchComponent
    ],
    imports: [
        CommonModule,
        NgbDatepickerModule,
        LibComponentsModule,
        HttpClientModule,
        FormsModule,
        RouterModule,
        NgSelectModule,
        NgbDropdownModule,
        NgxSmartModalModule.forRoot(),
        TreeModule,
        BodyModule,
        HeaderModule,
        FooterModule
    ],
    exports: [
        ChooseAddressComponent,
        ClsSearchComponent,
        ConfirmInfoComponent,
        ConfirmRemoveComponent,
        ConfirmSaveComponent,
        ModalComponent,
        OrgSearchComponent,
        PeopleSearchComponent,
        LibComponentsModule,
        HttpClientModule,
        NgbDatepickerModule,
        TreeModule,
        NgxSmartModalModule,
        FormsModule,
        BodyModule,
        HeaderModule,
        NgSelectModule,
        FooterModule
    ]
})
export class FiCoreModule {
    constructor(config: NgbDatepickerConfig, selectConfig: NgSelectConfig, @Optional() @SkipSelf() parentModule?: FiCoreModule) {
        if (parentModule) {
            throw new Error('CoreModule is already loaded. Import it in the AppModule only');
        }
        config.minDate = {year: 1900, month: 1, day: 1};
        config.maxDate = {year: 2099, month: 12, day: 31};
        selectConfig.notFoundText = '';
        selectConfig.loadingText = '';
        selectConfig.bindValue = 'value';
        selectConfig.typeToSearchText = '';
        selectConfig.clearAllText = 'Очистить';
        selectConfig.addTagText = '';
    }

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: FiCoreModule,
            providers: [
                {provide: LOCALE_ID, useValue: 'ru'},
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: RedirectInterceptor,
                    multi: true
                }
            ]
        };
    }
}
