import {Observable} from 'rxjs';
import {AddressDict, Button, ButtonType} from './models';
import {nowStr} from './functions';

export const NOT_EXISTS = 'NOT EXISTS';
/**
 * Код уровня дома (для адреса) 008 в выражениях используется как цифра
 */
export const hlNum = 8;
/**
 * Код уровня "Планировачная структура"
 */
export const psNum = 65;
export const russiaId = '127';

/**
 * Объект для наполнения серверными URLs, заполняется перед стартом приложения, запрашивая данные с сервера
 */
export const urls = {} as any;

/**
 * Объект для наполнения значениями id справочников, заполняется перед стартом приложения, запрашивая данные с сервера
 */
export const cls = {} as any;

/**
 * Кол-во миллисекунд в 18 годах, учитывая високосные года
 */
export const year18 = 1000 * 60 * 60 * 24 * (365 * 18 + 4);

/**
 * Ключ для хранения текущей темы в браузере
 */
export const themaKey = 'opeka.theme';

/**
 * Возможные варианты названия кнопок верхнего меню
 */
export const buttonTitle = {
    add: 'Добавить',
    home: 'На главную',
    exit: 'Выход',
    last: 'Последние 10',
    undo: 'Отменить',
    delete: 'Удалить',
    edit: 'Изменить',
    print: 'Печать',
    save: 'Сохранить',
    search: 'Поиск'
};

/**
 * Утилиты создания кнопок
 */
export const bb = {
    button: (type: ButtonType, click: () => void) => ({[type]: {click}}),
    menu: (type: ButtonType, menu: Button[]) => ({[type]: {menu}}),
    add: (callback: () => void) => bb.button(ButtonType.Add, callback),
    cancel: (callback: () => void) => bb.button(ButtonType.Cancel, callback),
    del: (callback: () => void) => bb.button(ButtonType.Delete, callback),
    edit: (callback: () => void) => bb.button(ButtonType.Edit, callback),
    print: (callback: () => void) => bb.button(ButtonType.Print, callback),
    save: (callback: () => void) => bb.button(ButtonType.Save, callback),

    medit: (menu: Button[]) => bb.menu(ButtonType.Edit, menu),
    mprint: (menu: Button[]) => bb.menu(ButtonType.Print, menu),
    msearch: (menu: Button[]) => bb.menu(ButtonType.Search, menu),
    lten: (menu: Button[]) => bb.menu(ButtonType.LastTen, menu)
};

/**
 * Объект для хранения текущего контекста
 * Приложение может быть запущено с разными параметрами, например, для работы с разными БД
 * Все приложения работают через один nginx-прокси, поэтому отличаем их по контексту на прокси.
 * этот конекст не попадает на само приложение, он нужен чтобы строить правильные пути к api и смежным приложения.
 * Контекст текущего приложения хранится в поле *app* и получается с сервера при запуске клиентской части приложения.
 * Например, при старте инициализировался так:
 * {app: '/opeka'}
 * Дальше можем строить абсолютный путь так:
 * window.location.origin + context.app + '/main'
 */
export let context = {} as any;

export function isChild(date: string): boolean {
    const d = toDate(date);
    return d && Date.now() - d.getTime() < year18;
}

export function toDate(date: string): Date {
    if (!date) {
        return null;
    }
    const parts = date.replace(/00\./g, '01.').split('.').map(it => parseInt(it, 10));
    if (parts.length !== 3 || parts.some(isNaN)) {
        return null;
    }
    return new Date(Date.UTC(parts[2], parts[1] - 1, parts[0]));
}

/**
 * Сравнивает дату с текущей, если такая же или больше - возвращает true, иначе false
 */
export function geNow(date: string): boolean {
    const now = nowStr().split('.').map(it => parseInt(it, 10));
    const dt = date.split('.').map(it => parseInt(it, 10));
    return dt[2] > now[2] || (dt[2] === now[2] && (dt[1] > now[1] || (dt[1] === now[1] && dt[0] >= now[0])));
}

/**
 * Вычисляет разницу в днях между двумя датами
 */
export function daysBetween(date1: string, date2: string): number {
    const d1 = date1.split('.');
    const d2 = date2.split('.');
    return Math.abs(new Date([d1[2], d1[1], d1[0]].join('/')).getTime() - new Date([d2[2], d2[1], d2[0]].join('/')).getTime())
        / 86400000;
}

/**
 * Возврщает true если у объекта есть хоть одно трушное поле
 * Если в объекте все поля, а также подобъекты этих полей не содержат объекта со значениями, тогда объект считается пустым
 * не учитывается что поле может содержать false
 * если поле является списком, то проверяется только наличие в списке чего-либо
 * но не проверяется на пустоту объекты этого списка
 * @param obj - исследуемый объект
 */
export function notEmpty(obj: any): boolean {
    return !!obj && (Array.isArray(obj) ? !!obj.length : Object.values(obj).some(v => v && (typeof v === 'object' ? notEmpty(v) : true)));
}


/**
 * Подписывает и автоматически отписывает подписчика после первого вызова, используется для получения ответа от модального окна
 * @param ob - объект, на который подписываемся
 * @param fn - функция, которая будет вызвана на событие
 * @param always - если true то вызываем обработчик (функцию) в любом случае, иначе, только в случае трушного результата
 */
export function onDone<T>(ob: Observable<T>,
                          fn = (t: T) => {
                          },
                          always = false) {
    const sub = ob.subscribe(res => {
        sub.unsubscribe();
        if (res || always) {
            fn(res);
        }
    });
}

/**
 * Склеивает адреса в одну строку
 * @param levels - квалифицированный адрес
 * @param text - дополнительные данные (неквалифицированный адрес, квартира и т.п.)
 */
export function getAddress(levels: AddressDict[], ...text: string[]) {
    return (levels ? levels.map(it => {
            let res = levelToString(it);
            if (it.child) {
                res += ' ' + levelToString(it.child);
                if (it.child.child) {
                    res += ' ' + levelToString(it.child.child);
                }
            }
            return res;
        }
    ) : [])
        .concat(text).filter(it => it).join(', ');
}

export function levelToString(it: AddressDict) {
    return it.order === 0 ? (it.short + ' ' + it.label) : (it.label + ' ' + it.short);
}

/**
 * Возвращает индекс массива, который не выходит за гранцы
 * @param idx - желаемый индекс
 * @param length - размер массива
 */
export function correctIdx(idx: number, length: number) {
    return Math.min(idx, Math.max(0, length - 1));
}

/**
 * Возвращает значение куки по имени
 */
export function getCookie(name: string): string {
    const myCookie = document.cookie.split('; ').find(it => it.startsWith(`${name}=`));
    return (myCookie && myCookie.split('=')[1]) || '';
}

/**
 * Открывает путь в новой вкладке
 * @param path - путь до файла, который получаем с сервера
 * @param ctx - контекст приложения
 */
export function showFile(path: string, ctx = '') {
    window.open(`${window.location.origin}${ctx}${urls.api.files}/${path}`, '_blank');
}

/**
 * Прокручивает до низа таблицы
 * @param clazz - класс css для выбора таблицы
 * @param count - кол-во строк, на которое нужно прокрутить
 * @param max - если кол-во элеметов больше этого значения, то нужно прокручивать
 * @param height - высота одной строки
 */
export function scrollTable(clazz: string, count: number, max = 1, height = 37) {
    if (count > max) {
        setTimeout(() => document.querySelector(`.${clazz}`)
            .scroll(0, count * height), 300);
    }
}

/**
 * Устанавливает общую тему приложения
 * @param theme - название темы для установки, если не передаем, то берем из localStorage
 */
export function setTheme(theme?: string) {
    try {
        const root = document.querySelector(':root');
        const next = theme === undefined ? localStorage.getItem(themaKey) : theme;
        if (root) {
            if (next) {
                root.setAttribute('theme', next);
            } else {
                root.removeAttribute('theme');
            }
        }
        if (theme) {
            localStorage.setItem(themaKey, theme);
        } else if (theme === null) {
            localStorage.removeItem(themaKey);
        }
    } catch (ex) {
        console.error('Cannot set theme');
        console.error(ex);
    }
}
