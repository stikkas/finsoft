import {ChangeDetectionStrategy, Component, ViewEncapsulation} from '@angular/core';
import {BarComponent} from '../bar.component';

@Component({
    selector: 'fi-cf-bar',
    templateUrl: './cf-bar.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CfBarComponent extends BarComponent {
}

