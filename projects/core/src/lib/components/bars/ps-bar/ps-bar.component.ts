import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation} from '@angular/core';
import {Observable} from 'rxjs';
import {BarComponent} from '../bar.component';
import {Dict} from '../../../models';
import {urls} from '../../../utils';
import {DictService} from '../../../services/dict.service';

@Component({
    selector: 'fi-ps-bar',
    templateUrl: './ps-bar.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PsBarComponent extends BarComponent {
    @Input() hasRelation: boolean;
    @Input() relDisabled: boolean;

    /**
     * Выбирать из всех типов родственников или только из близких
     */
    @Input() set full(value: boolean) {
        this.members$ = this.ds.getDict(value ? urls.api.dicts.ffm : urls.api.dicts.fm);
        this.useIds = value;
    }

    useIds = false;

    members$: Observable<Dict[]>;

    constructor(private ds: DictService) {
        super();
    }

    relationChanged(data: any) {
        if (this.useIds) {
            this.model.relationId = data.value;
        } else {
            this.model.relativeId = data.value;
        }
    }

}


