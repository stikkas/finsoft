import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {RouteParamsService} from '../../../services/route-params.service';
import {LdFilter, LdTableRow} from '../../../models';

@Component({
    selector: 'fi-action-bar',
    templateUrl: './action-bar.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionBarComponent {

    @Input() current: LdTableRow;
    @Input() request: LdFilter;
    @Output() save = new EventEmitter();
    @Input() nosave: boolean;
    @Input() nocreate: boolean;


    constructor(private rps: RouteParamsService) {
    }

    create() {
        this.rps.openNewTab('/pf', '/api/private-files/save-params', {...this.request});
    }
}

