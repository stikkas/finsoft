import {EventEmitter, Input, Output} from '@angular/core';
import {LdFilter} from '../../models';

export abstract class BarComponent {
    @Input() model: LdFilter;
    @Output() search = new EventEmitter();
    @Output() clear = new EventEmitter();

    snilsChanged(ev: any) {
        this.model.snils = ev.target.value;
    }
}

