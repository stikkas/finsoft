import {Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren, ViewEncapsulation} from '@angular/core';
import {SortableDirective} from '../directives/sortable.directive';
import {SortDirection, SortEvent} from '../../models';

@Component({
    selector: 'fi-table',
    templateUrl: './table.component.html',
    encapsulation: ViewEncapsulation.None
})
export class TableComponent implements OnInit {
    @Input() title: string;
    @Input() titleCls: string;
    @Input() headers: string[];
    @Input() widths: string[] = [];
    @Input() wrapClass: string;
    @Input() width: string;
    @Input() sorts: any[];
    @Output() sort = new EventEmitter<SortEvent>();

    @ViewChildren(SortableDirective) sortHeaders: QueryList<SortableDirective>;

    constructor() {
    }

    ngOnInit() {
    }

    setSort(event: SortEvent) {
        this.sort.emit(event);
        this.setUi(event);
    }

    /**
     * Выключает все значки сортировки, кроме выбранной
     * @param event - событие сортировки
     * @param direction - если есть, то вызвали программно, при инициализации страницы
     */
    setUi(event: SortEvent, direction?: SortDirection) {
        if (this.sortHeaders) {
            this.sortHeaders.forEach(header => {
                if (header.sortable !== event.column) {
                    header.clear();
                } else if (direction) {
                    header.setClass(direction);
                }
            });
        }
    }

    clearSorts() {
        this.sortHeaders.forEach(header => header.clear());
    }
}
