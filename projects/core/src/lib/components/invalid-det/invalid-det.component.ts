import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, SimpleChanges, ViewEncapsulation} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {ClsSearchComponentService} from '../../modals/cls-search/cls-search.component.service';
import {Document, InvalidDetails} from '../../models';
import {DictService} from '../../services/dict.service';
import {cls, onDone, urls} from '../../utils';

/**
 * Детальные сведения по инвалидности
 */
@Component({
    selector: 'fi-invalid-det',
    templateUrl: './invalid-det.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvalidDetComponent implements OnChanges {
    @Input() data: InvalidDetails;
    @Input() view: boolean;

    dn = urls.api.dicts;
    invalidGrps$ = this.ds.getDict('INVALID_GROUP');
    cause$ = this.ds.getDict('INVALID_CAUSE');
    wgInvInput$ = new BehaviorSubject<string>(null);
    whogiveInv$ = this.wgInvInput$.pipe(debounceTime(200), distinctUntilChanged(),
        switchMap(term => {
            return this.ds.loadOnce(this.dn.wgo, term);
        }));

    constructor(private ds: DictService, private clscs: ClsSearchComponentService, private cr: ChangeDetectorRef) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.data && changes.data.currentValue) {
            const curValue = changes.data.currentValue;
            if (!curValue.invDoc) {
                curValue.invDoc = {} as Document;
            }
            if (curValue.invDoc && curValue.invDoc.whogive) {
                this.wgInvInput$.next(curValue.invDoc.whogive);
            }
        }
    }

    setDoc(doc: Document) {
        onDone(this.clscs.open('Поиск документа', cls.meddt, {
            id: doc.typeId,
            value: doc.name
        }), d => {
            doc.typeId = d.id;
            doc.name = d.value;
            this.cr.markForCheck();
        });
    }

    clearDoc() {
        this.data.invDoc = {id: this.data.invDoc.id} as Document;
        this.cr.markForCheck();
    }
}
