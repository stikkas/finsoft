import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation} from '@angular/core';
import {ConfirmInfoComponentService} from '../../modals/confirm-info/confirm-info.component.service';
import {Action, LdTableRow, Path, PleaType, RegChildTableRow, RegFields, RegisteredType} from '../../models';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';
import {context, isChild, urls} from '../../utils';

export const registryCareOrg = '/registry-careorg';
export const actions = [{
    title: 'Личное дело',
    link: '/pf',
    clazz: 'personal-affair'
}, {
    title: 'Учет',
    link: '/reg',
    clazz: 'children-registered'
}, {
    title: 'Учет недееспособных',
    link: '/reg',
    clazz: Path.IncCitizens,
    queryParams: {type: RegisteredType.Incapable}
}, {
    title: 'Реестр детей, помещенных в организацию',
    link: registryCareOrg,
    clazz: 'children-registry',
    queryParams: {type: RegisteredType.Underage}
}, {
    title: 'Реестр кандидатов',
    link: `/${Path.CandidateRegistry}`,
    clazz: Path.CandidateRegistry,
    queryParams: {type: PleaType.Underage}
}, {
    title: 'Реестр семей опеки',
    link: `/${Path.FamilyRegistry}`,
    clazz: Path.FamilyRegistry
}, {
    title: 'Биологическая семья',
    link: `/bf/${Path.BioFamily}`,
    clazz: Path.BioFamily
}, {
    title: 'Реестр жилых помещений',
    link: `/${Path.PremisesRegistry}`,
    clazz: Path.PremisesRegistry
}, {
    title: 'Отчетные формы',
    link: `/${Path.Reports}`,
    clazz: Path.Reports,
    woid: true
}, {
    title: 'Журнал ПГУ/МФЦ',
    flink: () => {
        let type;
        if (window.location.pathname.endsWith(Path.CandidateRegistry)) {
            type = new URLSearchParams(window.location.search).get('type') || PleaType.Underage;
        } else if (window.location.pathname.endsWith(Path.AppealsRegistry)) {
            type = PleaType.Travel;
        }
        return `${urls.adminUi}/journal` + (type ? `?type=${type}` : '');
    },
    clazz: 'pgu-journal'
}, {
    title: 'Обращение',
    link: '/plea',
    clazz: Path.CandidateRegistry,
    queryParams: {type: PleaType.Underage},
    tail: Path.Main
}, {
    title: 'Обращение',
    link: '/plea',
    clazz: 'candidates-incompetent',
    queryParams: {type: PleaType.Adult},
    tail: Path.Main
}, {
    title: 'Семья опеки',
    link: '/family-op',
    tail: Path.Main,
    clazz: 'guardianship-family',
    woid: true
}, {
    title: 'Семейное устройство',
    link: `/${Path.FamilyArrangements}`,
    clazz: Path.FamilyArrangement,
    queryParams: {type: RegisteredType.Underage},
    woid: true
}, {
    title: 'Кандидаты в опекуны',
    link: `/${Path.CandidateRegistry}`,
    clazz: 'candidates-incompetent',
    queryParams: {type: PleaType.Adult}
}, {
    title: 'Устройство недееспособных',
    link: `/${Path.FamilyArrangements}`,
    clazz: 'incompetent-arrangement',
    queryParams: {type: RegisteredType.Incapable},
    woid: true
}, {
    title: 'Семейное устройство',
    link: `/${Path.FamilyArrangement}`,
    clazz: Path.FamilyArrangement,
    queryParams: {type: RegisteredType.Underage}
}, {
    title: 'Журнал СМЭВ',
    flink: () => `${urls.adminUi}/smevs`,
    clazz: 'smevs-journal'
}, {
    title: 'Журнал ЕГИССО (Жилье)',
    flink: () => `${urls.adminUi}/egisso`,
    clazz: 'egisso'
}, {
    title: 'Журнал ЕГИССО (Реестры)',
    flink: (router: Router) => {
        let par;
        if (router.url.startsWith('/children-registered') || router.url.startsWith('/family-arrangements')
            || router.url.startsWith('/candidate-registry')) {
            const type = router.parseUrl(router.url).queryParamMap.get('type');
            par = (!type || type === RegisteredType.Underage || type === PleaType.Underage) ? RegisteredType.Underage
                : RegisteredType.Incapable;
        }
        return `${urls.adminUi}/egisso-registry${par ? '?type=' + par : ''}`;
    },
    clazz: 'egisso-registry'
}, {
    title: 'Учет нуждающихся в патронаже',
    link: '/reg',
    clazz: 'needing-helpers',
    queryParams: {type: RegisteredType.Patron}
}, {
    title: 'Обращение',
    link: '/plea',
    clazz: 'assistant-candidates',
    queryParams: {type: PleaType.AssignAssistant},
    tail: Path.Main
}, {
    title: 'Ребенок до усыновления',
    link: '/pf',
    clazz: 'personal-affair'
}, {
    clazz: 'empty' // 23
},
    // БДДСОП
    {
        title: 'Реестр учета семей',
        link: `/${Path.FamilyRegistry}`,
        clazz: Path.FamilyRegistry
    }, {
        title: 'Семья несовершеннолетнего',
        link: `/bf/${Path.BioFamily}`,
        clazz: Path.BioFamily
    }
];


@Component({
    selector: 'fi-custody-nav',
    templateUrl: './custody-nav.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustodyNavComponent {
    private regUrl: string;
    private searchUrl: string;
    private readonly ctx: string;

    // Для всех кнопок передаем данные в этом объекте
    @Input() cur: RegFields;
    // Если нужно передать данные для какой-то кнопки отличные от других, то
    // нужно в этом массиве передать эти данные с индексом соотвествующим индексу в массиве кнопок
    // сперва ищем id здесь, а потом уже в cur
    @Input() expanded: (RegChildTableRow | LdTableRow)[];
    @Input() modal = true;
    // Показывать только эти кнопки
    @Input() btns: number[] = context.aisId === 3 ? [0, 1, 25] : context.aisId === 6 ? [0] : [0, 6, 23, 1, 4, 13, 23, 2, 14, 15, 23, 7];

    constructor(private router: Router, private http: HttpClient, private cics: ConfirmInfoComponentService) {
        this.ctx = context.aisUrl || context.app;
        this.regUrl = `${this.ctx}/api/private-files/is-registered`;
        this.searchUrl = `${this.ctx}/api/search/exist-appeal`;
    }

    navigate(action: Action, idx: number) {
        const cur = (this.expanded && this.expanded[idx]) || this.cur;
        const queryParams = this.expanded && this.expanded[idx] && (this.expanded[idx] as any).qp;
        if (action.link === `/${Path.PremisesRegistry}` || action.link === registryCareOrg) {
            if (cur && isChild(cur.birthDate)) {
                let parts = cur as any;
                if (parts.fio) {
                    parts = (cur as any).fio.trim().split(/\s+/);
                } else {
                    parts = [parts.lastName, parts.firstName, parts.middleName];
                }
                this.go(action.link, null, {
                    bl: parts[0] || '', bf: parts[1] || '', bm: parts[2] || '',
                    ...action.queryParams
                });
            } else {
                this.go(action.link, null, {bl: '', bf: '', bm: '', ...action.queryParams});
            }
        } else if (action.flink) {
            window.open(action.flink(this.router), '_blank');
        } else if (action.woid) {
            this.go(action.link, action.link === '/family-op' ? cur && cur.id : null, (action.queryParams || queryParams) ?
                {...action.queryParams, ...queryParams} : null, action.tail);
        } else if (cur && cur.id) {
            if (action.link === '/reg') {
                if (action.queryParams && (action.queryParams.type === RegisteredType.Incapable
                    || action.queryParams.type === RegisteredType.Patron)) {
                    this.http.get(`${this.regUrl}/${cur.id}`, {params: {type: action.queryParams.type}})
                        .pipe(catchError(res => of(false)))
                        .subscribe(res => res ? this.go(action.link, cur.id, action.queryParams) : this.notFoundShow());
                } else {
                    this.http.get(`${this.regUrl}/${cur.id}`).pipe(catchError(res => of(false)))
                        .subscribe(res => res ? this.go(action.link, cur.id) : this.notFoundShow());
                }
            } else if (action.link === `/${Path.CandidateRegistry}`) {
                if (!isChild(cur.birthDate)) {
                    this.http.get(`${this.searchUrl}/${cur.id}/${action.queryParams.type}`).pipe(catchError(res => of(false)))
                        .subscribe(res => {
                            if (res) {
                                const parts = (cur as any).fio.trim().split(/\s+/);
                                const qp = {} as any;
                                if (parts[0]) {
                                    qp.l = parts[0];
                                }
                                if (parts[1]) {
                                    qp.f = parts[1];
                                }
                                if (parts[2]) {
                                    qp.m = parts[2];
                                }
                                this.go(action.link, null, {...action.queryParams, ...qp});
                            } else {
                                this.notFoundShow('Обращение не найдено');
                            }
                        });
                }
            } else {
                this.go(action.link, cur.id, (action.queryParams || queryParams) ?
                    {...action.queryParams, ...queryParams} : null, action.tail);
            }
        }
    }

    private notFoundShow(label = 'Не найдена запись об учете') {
        this.cics.open(label);
    }

    /**
     * Переход по ссылке
     * @param link - ссылка
     * @param id - идентификатор чтобы использовать в ссылке
     * @param queryParams - дополнительные параметры запроса
     * @param tail - путь после id
     * @private
     */
    private go(link: string, id?: string, queryParams?: any, tail?: string) {
        if (link === '/family-op' && !id && !queryParams) {
            return;
        }
        // Когда есть context.aisUrl - значит вызов происходит из админки
        if (this.modal || context.aisUrl) {
            window.open(`${this.ctx}${link}${id ? '/' + id : ''}${tail ? '/' + tail : ''}${queryParams ? '?' + new URLSearchParams(queryParams).toString() : ''}`, this.modal ? '_blank' : '_self');
        } else {
            this.router.navigate([link].concat(id ? [id] : []).concat(tail ? [tail] : []), queryParams ? {queryParams} : {});
        }
    }

    get actions() {
        return this.btns ? this.btns.map(it => actions[it]) : [];
    }
}
