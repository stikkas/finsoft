import {ChangeDetectionStrategy, Component, ElementRef, Input, ViewChild, ViewEncapsulation} from '@angular/core';
import {context, showFile} from '../../utils';

/**
 * Кнопка добаления, обноления, удаления файла для документа
 */
@Component({
    selector: 'fi-doc-file',
    templateUrl: './doc-file.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocFileComponent {
    /**
     * Для хранения файлов используется массив массивов
     * где первый элемент - объект, для которого предназначен файл
     * второй элемент - сам файл (File)
     */
    @Input() files: any [];

    /**
     * Текущий объект, с которым работаем в данный момент,
     * должен иметь поле fileName
     */
    @Input() cur: { fileName?: string };

    /**
     * Флаг редактирования, означает что можно добавлять, удалять файл
     */
    @Input() edit: boolean;

    /**
     * С label, при использовании в разных формах, вообще это надо сделать через css, но пока так
     */
    @Input() label: boolean;

    /**
     * Объект для выбора файла
     */
    @ViewChild('file') fileEl: ElementRef;

    sf = (path: string) => showFile(path, context.aisUrl || context.app);

    chooseFile(obj: any) {
        const input = this.fileEl.nativeElement;
        input.onchange = () => {
            if (input.files.length) {
                const cur = this.files.find(it => it[0] === obj);
                if (cur) {
                    cur[1] = input.files[0];
                } else {
                    this.files.push([obj, input.files[0]]);
                }
            }
        };
        input.click();
    }

    remove(obj: any) {
        const idx = this.files.findIndex(it => it[0] === obj);
        if (idx > -1) {
            this.files.splice(idx, 1);
        }
        delete obj.fileName;
    }
}
