import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import {Page} from '../../models';

@Component({
    selector: 'fi-pgntr',
    template: `
        <div *ngIf="page?.pages > 1 && !disabled">
            <div>
                <ngb-pagination [page]="page.page"
                                [maxSize]="3"
                                [pageSize]="page.size"
                                [collectionSize]="page.count"
                                (pageChange)="changed.emit($event)">
                </ngb-pagination>
            </div>
        </div>
    `,
    encapsulation: ViewEncapsulation.None
})
export class PaginatorComponent {
    @Input() page: Page<any>;
    @Input() disabled: boolean;
    @Output() changed = new EventEmitter<number>();
}
