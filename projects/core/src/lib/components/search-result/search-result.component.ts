import {Component, EventEmitter, Input, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {TableComponent} from '../table/table.component';
import {LdTableRow, Page, SortEvent} from '../../models';

@Component({
    selector: 'fi-search-result',
    templateUrl: './search-result.component.html',
    styleUrls: ['./search-result.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SearchResultComponent {
    @Input() width: string;
    @Input() data: Page<LdTableRow>;
    @Input() current: LdTableRow;
    @Input() actual: boolean;

    @Output() load = new EventEmitter<SortEvent | number>();
    @Output() rowClick = new EventEmitter<LdTableRow>();
    @Output() rowDblclick = new EventEmitter<LdTableRow>();
    @Output() actualChange = new EventEmitter<boolean>();

    @ViewChild('at') at: TableComponent;

    sorts = ['surname', 'birthdate', 'sex', 'snils', 'doc', 'fadr', 'radr'];
    headers = ['ФИО', 'Дата рождения', 'Пол', 'СНИЛС', 'Документ', 'Фактический адрес', 'Место регистрации'];
    maxRows = 10;

    clearSorts() {
        this.at.clearSorts();
    }

    trackById(index, item) {
        return item.systemmanId;
    }

    getDoc(p: LdTableRow): string {
        return [p.documentType, p.seria, p.number].join(' ');
    }

    clear() { // Если хочется OnPush тогда нужно что-то делать с actual = true, не всего отображается
        this.clearSorts();
    }

    getClass(p: LdTableRow) {
        const cls = {} as any;
        if (this.current === p) {
            cls['b-table__row_state_active'] = true;
        } else if (p.kind) {
            cls['b-table__row_bg_lazur'] = true;
        }
        if (p.actual === 0) {
            cls['b-table__row_color_blue'] = true;
        } else if (p.deathDate) {
            cls.dead = true;
        }
        return cls;
    }
}

