import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'fi-calendar',
    templateUrl: './calendar.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {provide: NG_VALUE_ACCESSOR, useExisting: CalendarComponent, multi: true}
    ]
})
export class CalendarComponent implements ControlValueAccessor {
    private innerValue: string;
    private onChanged: (_: string) => void;

    @Output() changed = new EventEmitter<string>();
    @Input() disabled: boolean;
    @Input() width = 82;
    /**
     * без кнопки, используется в таблицах
     */
    @Input() nobtn;

    @ViewChild('din')
    din: ElementRef;

    get value(): string {
        return this.innerValue || null;
    }

    set value(value: string) {
        if (this.innerValue !== value) {
            this.innerValue = value;
            this.onChanged(value);
        }
    }

    writeValue(value: string) {
        this.innerValue = value;
        if (this.onChanged) {
            this.onChanged(value);
        }
    }

    registerOnChange(fn: (_: string) => void) {
        this.onChanged = fn;
    }

    registerOnTouched(fn: () => void) {
    }

    onChange(event: NgbDateStruct) {
        this.value = event && [(event.day < 10 ? `0${event.day}` : event.day),
            (event.month < 10 ? `0${event.month}` : event.month),
            event.year].join('.');
        this.changed.emit(this.value);
    }

    inputChanged(e: any) {
        const value = e.target.value;
        if (value.length > 9) {
            const arr = value.replace(/\./g, '/').split('');
            const start = arr[0];
            arr[0] = arr[3];
            arr[3] = arr[1];
            arr[1] = arr[4];
            arr[4] = arr[3];
            arr[3] = start;
            const d = new Date(arr.join(''));
            if (isNaN(d.getTime())) {
                this.value = null;
                this.din.nativeElement.value = '';
                return;
            } else {
                this.value = value;
            }
        } else {
            this.value = null;
        }
        this.changed.emit(this.value);
    }
}
