import {NgModule} from '@angular/core';
import {ErrorsComponent} from './errors/errors.component';
import {LoadComponent} from './load/load.component';
import {CommonModule} from '@angular/common';
import {PaginatorComponent} from './paginator/paginator.component';
import {NgbDatepickerModule, NgbDropdownModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {EmptyDirective} from './directives/empty.directive';
import {TableComponent} from './table/table.component';
import {SortableDirective} from './directives/sortable.directive';
import {MaskDateDirective} from './directives/mask-date.directive';
import {ExternalUrlDirective} from './directives/external-url.directive';
import {MaskFloatDirective} from './directives/mask-float.directive';
import {MaskNumDirective} from './directives/mask-num.directive';
import {PsBarComponent} from './bars/ps-bar/ps-bar.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {FormsModule} from '@angular/forms';
import {MaskSnilsDirective} from './directives/mask-snils.directive';
import {CfBarComponent} from './bars/cf-bar/cf-bar.component';
import {ActionBarComponent} from './bars/action-bar/action-bar.component';
import {CustodyNavComponent} from './custody-nav/custody-nav.component';
import {SearchResultComponent} from './search-result/search-result.component';
import {InputClearComponent} from './input-clear/input-clear.component';
import {CalendarComponent} from './calendar/calendar.component';
import {TableNavDirective} from './directives/table-nav.directive';
import {MaskDocDirective} from './directives/mask-doc.directive';
import {IncapableDetComponent} from './incapable-det/incapable-det.component';
import {InvalidDetComponent} from './invalid-det/invalid-det.component';
import {MaskFlatDirective} from './directives/mask-flat.directive';
import {MaskDepCodeDirective} from './directives/mask-dep-code.directive';
import {MaskFioDirective} from './directives/mask-fio.directive';
import {PfDirective} from './directives/pf.directive';
import {DocFileComponent} from './doc-file/doc-file.component';

@NgModule({
    declarations: [ActionBarComponent, CfBarComponent, PsBarComponent, CalendarComponent, CustodyNavComponent, EmptyDirective,
        ExternalUrlDirective, MaskDateDirective, MaskDepCodeDirective, MaskFloatDirective, MaskDocDirective, MaskFioDirective,
        MaskFlatDirective, MaskNumDirective, MaskSnilsDirective, PfDirective, SortableDirective, TableNavDirective, ErrorsComponent,
        IncapableDetComponent, InputClearComponent, InvalidDetComponent, LoadComponent, PaginatorComponent, SearchResultComponent,
        TableComponent, DocFileComponent],
    imports: [CommonModule, NgbPaginationModule, NgbDatepickerModule, NgSelectModule, FormsModule, NgbDropdownModule],
    exports: [ActionBarComponent, CfBarComponent, PsBarComponent, CalendarComponent, CustodyNavComponent, EmptyDirective,
        ExternalUrlDirective, MaskDateDirective, MaskDepCodeDirective, MaskDocDirective, MaskFioDirective, MaskFlatDirective,
        MaskFloatDirective, MaskNumDirective, MaskSnilsDirective, PfDirective, SortableDirective, TableNavDirective, ErrorsComponent,
        IncapableDetComponent, InputClearComponent, InvalidDetComponent, LoadComponent, PaginatorComponent, SearchResultComponent,
        TableComponent, DocFileComponent]
})
export class LibComponentsModule {
}

