import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'fi-errors',
    template: `
        <p>Необходимо заполнить следующие поля:</p>
        <ul>
            <li *ngFor="let m of errors">{{m}}</li>
        </ul>
    `,
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorsComponent {
    @Input() errors: string[];

    constructor() {
    }
}
