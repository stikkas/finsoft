import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';

/**
 * Поле ввода с кнопкой очистить
 */
@Component({
    selector: 'fi-input-clear',
    template: `
        <input class="b-form__input" [style.width]="width" readonly [value]="value || ''" [disabled]="disabled">
        <span class="clearb" title="Очистить" *ngIf="showClear && !disabled" (click)="clear.emit()">
            <span>×</span>
        </span>
    `,
    styleUrls: ['./input-clear.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputClearComponent {
    @Input() showClear: boolean;
    @Input() value: string;
    @Input() width: string;
    @Input() disabled: boolean;
    @Output() clear = new EventEmitter();

    constructor() {
    }
}
