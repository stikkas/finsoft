import {Directive, ElementRef, OnDestroy} from '@angular/core';
import * as textMask from 'vanilla-text-mask/dist/vanillaTextMask.js';
import {triArray} from './mask-snils.directive';

export const maskDepCode = [...triArray, '-', ...triArray];

@Directive({
    selector: '[fiMaskDepCode]'
})
export class MaskDepCodeDirective implements OnDestroy {
    maskedInputController;

    constructor(private element: ElementRef) {
        this.maskedInputController = textMask.maskInput({
            inputElement: this.element.nativeElement,
            mask: maskDepCode,
            guide: false
        });
    }

    ngOnDestroy() {
        this.maskedInputController.destroy();
    }
}
