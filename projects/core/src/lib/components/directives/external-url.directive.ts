import {Directive, ElementRef, HostListener, Input} from '@angular/core';
import {Router} from '@angular/router';

@Directive({
    selector: '[fiExternalUrl]',
})
export class ExternalUrlDirective {
    @Input() fiExternalUrl: string;

    constructor(private el: ElementRef, private router: Router) {
    }

    @HostListener('click')
    clicked() {
        this.router.navigate(['/externalRedirect', {externalUrl: this.fiExternalUrl}], {
            skipLocationChange: true
        });
    }
}

