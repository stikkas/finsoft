import {Directive, ElementRef, Input, OnDestroy} from '@angular/core';
import * as textMask from 'vanilla-text-mask/dist/vanillaTextMask.js';

@Directive({
    selector: '[fiMaskFloat]'
})
export class MaskFloatDirective implements OnDestroy {

    /**
     * Кол-во знаков до запятой (целая часть)
     */
    @Input('fiMaskFloat') intCount;
    maskedInputController;

    constructor(private element: ElementRef) {
        this.maskedInputController = textMask.maskInput({
            inputElement: this.element.nativeElement,
            guide: false,
            mask: (input: string) => {
                const m = [/[1-9]/];
                const l = input.length;
                if (l > 1) {
                    let hasDot = false;
                    for (let i = 1; i < l; ++i) {
                        if (i < this.intCount) {
                            m.push(hasDot ? /\d/ : /[\d.]/);
                        } else {
                            m.push(hasDot ? /\d/ : /\./);
                        }
                        if (!hasDot && input[i] === '.') {
                            hasDot = true;
                        }
                    }
                }
                return m;
            }
        });
    }

    ngOnDestroy() {
        this.maskedInputController.destroy();
    }
}

