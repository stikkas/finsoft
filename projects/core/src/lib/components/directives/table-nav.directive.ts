import {Directive, ElementRef, EventEmitter, HostListener, Input, Output} from '@angular/core';

type Value = [number /* индекс строки таблицы */, boolean /* является ли строка первой */, boolean /* является ли строка последней */];

/**
 * Навигация вверх-вниз по таблице результатов с помощью стрелок
 */
@Directive({
    selector: '[fiTabNav]'
})
export class TableNavDirective {
    private data: Value;

    @Output() navTabNext = new EventEmitter<number>();

    @Input()
    set fiTabNav(value: Value) {
        this.data = value;
        if (value) {
            this.el.nativeElement.tabIndex = 1000 + value[0];
        }
    }

    constructor(private el: ElementRef) {
    }

    @HostListener('keyup.arrowDown', ['$event'])
    down(event: any) {
        if (this.data && !this.data[2]) {
            this.move(event, this.data[0] + 1);
        }
    }

    @HostListener('keyup.arrowUp', ['$event'])
    up(event: any) {
        if (this.data && !this.data[1]) {
            this.move(event, this.data[0] - 1);
        }
    }

    setFocus() {
        this.el.nativeElement.focus();
    }

    private move(event: any, i: number) {
        event.preventDefault();
        this.navTabNext.emit(i);
    }
}
