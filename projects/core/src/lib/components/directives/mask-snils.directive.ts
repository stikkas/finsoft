import {Directive, ElementRef, OnDestroy} from '@angular/core';
import * as textMask from 'vanilla-text-mask/dist/vanillaTextMask.js';

export const triArray = new Array(3).fill(/\d/);
export const maskSnils = [...triArray, '-', ...triArray, '-', ...triArray, '-', ...triArray.slice(0, 2)];

@Directive({
    selector: '[fiMaskSnils]'
})
export class MaskSnilsDirective implements OnDestroy {
    maskedInputController;

    constructor(private element: ElementRef) {
        this.maskedInputController = textMask.maskInput({
            inputElement: this.element.nativeElement,
            mask: maskSnils,
            guide: false
        });
    }

    ngOnDestroy() {
        this.maskedInputController.destroy();
    }
}
