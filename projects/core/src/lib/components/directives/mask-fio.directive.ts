import {Directive, ElementRef, OnDestroy} from '@angular/core';
import * as textMask from 'vanilla-text-mask/dist/vanillaTextMask.js';

@Directive({
    selector: '[fiMaskFio]'
})
export class MaskFioDirective implements OnDestroy {
    maskedInputController;

    constructor(private element: ElementRef) {
        const letters = /[А-ЯЁ]/i;
        const all = /[-А-Я.Ё,'\s]/i;
        this.maskedInputController = textMask.maskInput({
            inputElement: this.element.nativeElement,
            mask(input: string) {
                return input.split('')
                    .filter((it, i) => i === 0 ? letters.test(it) : all.test(it)).map(() => /./);
            }
        });
    }

    ngOnDestroy() {
        this.maskedInputController.destroy();
    }
}
