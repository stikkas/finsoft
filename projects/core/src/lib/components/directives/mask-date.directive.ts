import {Directive, ElementRef, OnDestroy} from '@angular/core';
import * as textMask from 'vanilla-text-mask/dist/vanillaTextMask.js';

export const dateMask = [/[0-3]/, /\d/, '.', /[01]/, /\d/, '.', /[1-3]/, /\d/, /\d/, /\d/];

@Directive({
    selector: '[fiMaskDate]'
})
export class MaskDateDirective implements OnDestroy {

    maskedInputController;

    constructor(private element: ElementRef) {
        this.maskedInputController = textMask.maskInput({
            inputElement: this.element.nativeElement,
            guide: false,
            mask(input: string) {
                const value = input.split('').map(it => parseInt(it, 10)).filter(it => !isNaN(it));
                const m = [...dateMask];
                const l = value.length;
                if (l) {
                    if (value[0] < 4) {
                        const lessThree = value[0] < 3;
                        if (l > 1) {
                            if (!lessThree) {
                                m[1] = /[01]/;
                            }
                            if (l > 2) {
                                if (l > 3) {
                                    m[4] = value[2] ? /[0-2]/ : lessThree ? /[0-9]/ : /[13-9]/;
                                    if (l > 4) {
                                        const y = value[4];
                                        if (0 < y && y < 4) {
                                            if (l > 5) {
                                                if (y === 1) {
                                                    m[7] = /9/;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return m;
            }
        });
    }

    ngOnDestroy() {
        this.maskedInputController.destroy();
    }
}

