import {Directive, HostListener, Input} from '@angular/core';
import {context} from '../../utils';

@Directive({
    selector: '[fiPf]'
})
export class PfDirective {
    // Идентфикатор пользователя, для перехода в личное дело
    @Input() fiPf: string;

    constructor() {
    }

    @HostListener('dblclick')
    toPf() {
        if (this.fiPf) {
            window.open(`${context.app}/pf/${this.fiPf}/man-info`, '_blank');
        }
    }

    @HostListener('mousedown', ['$event'])
    noSelect(event: any) {
        event.preventDefault();
    }
}
