import {Directive, ElementRef, Input, OnDestroy} from '@angular/core';
import * as textMask from 'vanilla-text-mask/dist/vanillaTextMask.js';

@Directive({
    selector: '[fiMaskDoc]'
})
export class MaskDocDirective implements OnDestroy {
    @Input()
    set mask(value: string) {
        this.ngOnDestroy();
        if (value) {
            this.maskedInputController = textMask.maskInput({
                inputElement: this.element.nativeElement,
                mask: value.split('').map(it => it === '9' ? /\d/ : it === 'X' ? /./ : it),
                guide: false
            });
        }
    }

    maskedInputController;

    constructor(private element: ElementRef) {
    }


    ngOnDestroy() {
        if (this.maskedInputController) {
            this.maskedInputController.destroy();
        }
    }
}

