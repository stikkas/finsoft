import {Directive, ElementRef, Input, OnDestroy} from '@angular/core';
import * as textMask from 'vanilla-text-mask/dist/vanillaTextMask.js';

@Directive({
    selector: '[fiMaskNum]'
})
export class MaskNumDirective implements OnDestroy {
    maskedInputController;

    @Input('fiMaskNum')
    set numCount(value: number) {
        this.ngOnDestroy();
        if (value) {
            this.maskedInputController = textMask.maskInput({
                inputElement: this.element.nativeElement,
                mask: new Array(value).fill(/\d/),
                guide: false
            });
        }
    }

    constructor(private element: ElementRef) {
    }

    ngOnDestroy() {
        if (this.maskedInputController) {
            this.maskedInputController.destroy();
        }
    }
}
