import {Directive, ElementRef, OnDestroy} from '@angular/core';
import * as textMask from 'vanilla-text-mask/dist/vanillaTextMask.js';

@Directive({
    selector: '[fiMaskFlat]'
})
export class MaskFlatDirective implements OnDestroy {
    maskedInputController;

    constructor(private element: ElementRef) {
        const all = /[-А-Яа-яЁё0-9,]/i;
        this.maskedInputController = textMask.maskInput({
            inputElement: this.element.nativeElement,
            mask(input: string) {
                return input.split('')
                    .filter(it => all.test(it)).map(() => all);
            }
        });
    }

    ngOnDestroy() {
        this.maskedInputController.destroy();
    }
}
