import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
    selector: '[fiEmpty]'
})
export class EmptyDirective {
    @Input() set fiEmpty(value: number) {
        const exists = this.vcRef.length;
        if (value > 0) {
            if (value > exists) {
                for (let i = exists; i < value; ++i) {
                    this.vcRef.createEmbeddedView(this.templateRef);
                }
            } else if (value < exists) {
                for (let i = exists; i > value; --i) {
                    this.vcRef.remove();
                }
            }
        } else {
            this.vcRef.clear();
        }
    }

    constructor(private templateRef: TemplateRef<any>, private vcRef: ViewContainerRef) {
    }
}
