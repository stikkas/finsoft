import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, SimpleChanges, ViewEncapsulation} from '@angular/core';
import {Document, IncapableDetails} from '../../models';
import {BehaviorSubject} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {cls, onDone, urls} from '../../utils';
import {DictService} from '../../services/dict.service';
import {ClsSearchComponentService} from '../../modals/cls-search/cls-search.component.service';

/**
 * Детальные сведения по недееспособности
 */
@Component({
    selector: 'fi-incapable-det',
    templateUrl: './incapable-det.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class IncapableDetComponent implements OnChanges {
    @Input() data: IncapableDetails;
    @Input() view: boolean;

    dn = urls.api.dicts;
    wgIncInput$ = new BehaviorSubject<string>(null);
    whogiveInc$ = this.wgIncInput$.pipe(debounceTime(200), distinctUntilChanged(),
        switchMap(term => {
            return this.ds.loadOnce(this.dn.wgo, term);
        }));

    constructor(private ds: DictService, private clscs: ClsSearchComponentService, private cr: ChangeDetectorRef) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.data && changes.data.currentValue) {
            const curValue = changes.data.currentValue;
            if (curValue.incDoc && curValue.incDoc.whogive) {
                this.wgIncInput$.next(curValue.incDoc.whogive);
            }
        }
    }

    setDoc(doc: Document) {
        onDone(this.clscs.open('Поиск документа', cls.sdt, {
            id: doc.typeId,
            value: doc.name
        }), d => {
            doc.typeId = d.id;
            doc.name = d.value;
            this.cr.markForCheck();
        });
    }

    clearDoc() {
        const doc = this.data.incDoc;
        this.data.incDoc = {id: doc.id} as Document;
        if (this.data.disabilityId) {
            this.data.incDoc.typeId = doc.typeId;
            this.data.incDoc.name = doc.name;
        }
        this.cr.markForCheck();
    }
}
