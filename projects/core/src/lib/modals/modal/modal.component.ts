import {Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgxSmartModalComponent} from 'ngx-smart-modal';

@Component({
    selector: 'fi-modal',
    templateUrl: './modal.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ModalComponent implements OnInit {
    @Input() id: string;
    @Input() title: string;
    @Input() clazz: string;
    @Input() closable: boolean;
    @Output() closed = new EventEmitter();

    @ViewChild('myModal', {static: true})
    modal: NgxSmartModalComponent;

    constructor() {
    }

    ngOnInit() {
    }

    close() {
        this.modal.close();
    }

    getData() {
        return this.modal.getData();
    }
}
