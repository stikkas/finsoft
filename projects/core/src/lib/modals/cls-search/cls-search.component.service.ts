import {Injectable} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Observable, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ClsSearchComponentService {
    private subject = new Subject<{ id: string, value: string }>();
    private cfg = new Subject<{
        id?: string, value?: string, title: string, rootId: string | string[],
        excluded: string[], lo?: boolean
    }>();
    readonly id = 'ModalId.ClsSearch';

    constructor(private sms: NgxSmartModalService) {
    }

    /**
     * Открывает диалог выбора из справочника
     * @param title - заголовок диалога
     * @param rootId - если строка, то ищем только под этим id, если массив строк, то ищем для этих id c ними включительно
     * @param input - исходные данные
     */
    open(title: string, rootId: string | string[],
         input?: { id?: string, value?: string }, excluded?: string[], lo?: boolean): Observable<{ id: string, value: string }> {
        this.cfg.next({title, rootId, ...input, excluded, lo});
        this.sms.getModal(this.id).open();
        return this.subject.asObservable();
    }

    done(cls: any) {
        this.subject.next(cls);
    }

    get cfg$() {
        return this.cfg.asObservable();
    }
}

