import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ModalComponent} from '../modal/modal.component';
import {AdminSearchService} from '../../services/admin-search.service';
import {ClsSearchComponentService} from './cls-search.component.service';
import {ITreeOptions, TREE_ACTIONS, TreeNode} from '@circlon/angular-tree-component';

@Component({
    selector: 'fi-cls-search',
    templateUrl: './cls-search.component.html',
    styleUrls: ['./cls-search.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ClsSearchComponent implements OnInit {
    private rootId: string;
    private parentIds: string[];
    private excluded: string[];
    /**
     * Можно выбирать только листья дерева, папки нельзя
     * @private
     */
    private leavesOnly: boolean;
    id = this.cscs.id;
    title: string;
    @ViewChild('mc') mc: ModalComponent;
    @ViewChild('tree') tree;
    @ViewChild('input') input: ElementRef;

    current = false;
    data: any[];

    treeOptions: ITreeOptions = {
        displayField: 'value', idField: 'id',
        scrollOnActivate: false,
        getChildren: (node: TreeNode) => this.search(undefined, node.id).toPromise(),
        actionMapping: {
            mouse: {
                dblClick: (tree, node, $event) => this.ok(node),
                click: TREE_ACTIONS.TOGGLE_ACTIVE,
            }
        }
    };

    constructor(private cscs: ClsSearchComponentService, private ss: AdminSearchService) {
    }

    ngOnInit() {
        this.cscs.cfg$.subscribe(({id, value, title, rootId, excluded, lo}) => {
                this.title = title;
                if (Array.isArray(rootId)) {
                    this.rootId = null;
                    this.parentIds = rootId;
                } else {
                    this.rootId = rootId;
                    this.parentIds = null;
                }
                this.leavesOnly = lo;
                this.excluded = excluded;
                this.update(undefined, id);
            }
        );
    }

    search(name: string, parentId?: string) {
        if (name !== undefined) {
            this.update(name);
        } else {
            return this.ss.findClassifiers(name, parentId);
        }
    }

    ok(node?: TreeNode) {
        const data = (node || this.tree.treeModel.getActiveNode()).data;
        if (!(this.leavesOnly && (data.children || data.hasChildren))) {
            this.cscs.done({id: data.id, value: data.value});
            this.mc.close();
        }
    }

    onClose() {
        this.unsetActiveNode();
        this.data = null;
        this.input.nativeElement.value = '';
        this.cscs.done(null);
    }

    clear() {
        this.input.nativeElement.value = '';
    }

    doneDis() {
        let cur = null;
        return !this.current ||
            (this.leavesOnly && ((cur = this.tree.treeModel.getActiveNode()) && (cur.data.children || cur.data.hasChildren)));
    }

    private update(name?: string, curId?: string) {
        this.ss.findClassifiers(name, this.rootId, this.parentIds, this.excluded).subscribe(res => {
            this.data = res;
            if (curId) {
                setTimeout(() => this.tree.treeModel.expandAll());
                let times = 3;
                const find = () => setTimeout(() => {
                    const node = this.tree.treeModel.getNodeById(curId);
                    if (node) {
                        node.setActiveAndVisible();
                    } else if (times > 0) {
                        --times;
                        find();
                    }
                }, 150);
                find();
            } else {
                this.unsetActiveNode(!!name);
            }
        });
    }

    private unsetActiveNode(expand = false) {
        const node = this.tree.treeModel.getActiveNode();
        if (node) {
            node.setIsActive(false);
            node.blur();
        }
        if (this.tree.treeModel.roots) {
            if (expand) {
                this.tree.treeModel.expandAll();
            } else {
                this.tree.treeModel.collapseAll();
            }
        }
    }

}

