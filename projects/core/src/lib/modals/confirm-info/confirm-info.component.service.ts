import {Injectable} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';

@Injectable({
    providedIn: 'root'
})
export class ConfirmInfoComponentService {
    readonly id = 'ModalId.ConfirmInfo';

    constructor(private sms: NgxSmartModalService) {
    }

    open(text: string) {
        this.sms.getModal(this.id).setData(text, true).open();
    }
}

