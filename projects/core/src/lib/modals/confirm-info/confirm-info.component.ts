import {ChangeDetectionStrategy, Component, ViewEncapsulation} from '@angular/core';
import {ConfirmInfoComponentService} from './confirm-info.component.service';

@Component({
    selector: 'fi-confirm-info',
    templateUrl: './confirm-info.component.html',
    styleUrls: ['./confirm-info.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmInfoComponent {
    id = this.cics.id;

    constructor(private cics: ConfirmInfoComponentService) {
    }

}

