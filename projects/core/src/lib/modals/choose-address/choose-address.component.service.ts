import {Injectable} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Observable, Subject} from 'rxjs';
import {DwellingPlace} from '../../models';

@Injectable({
    providedIn: 'root'
})
export class ChooseAddressComponentService {
    private subject = new Subject<DwellingPlace>();
    private address = new Subject<DwellingPlace>();
    private config = new Subject<{
        title: string, country: boolean, wCountry: boolean, dCountry: boolean,
        flat: boolean, noadr: boolean, typeId: string, rooms: boolean, cause: boolean, ground: boolean
    }>();
    readonly id = 'ModalId.ChooseAddress';

    constructor(private sms: NgxSmartModalService) {
    }

    open(title: string, address: DwellingPlace | Observable<DwellingPlace>, country: boolean,
         wCountry = true, flat?: boolean, noadr?: boolean, dCountry?: boolean, typeId?: string,
         rooms?: boolean, cause?: boolean, ground?: boolean): Observable<DwellingPlace> {
        this.config.next({title, country, wCountry, flat, noadr, dCountry, typeId, rooms, cause, ground});
        if (address instanceof Observable) {
            address.subscribe(res => this.address.next(res));
        } else {
            this.address.next(address);
        }
        this.sms.getModal(this.id).open();
        return this.subject.asObservable();
    }

    done(res: DwellingPlace) {
        this.subject.next(res);
    }

    get config$() {
        return this.config.asObservable();
    }

    get address$() {
        return this.address.asObservable();
    }
}

