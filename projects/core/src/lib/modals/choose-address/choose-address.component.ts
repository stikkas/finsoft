import {Component, ElementRef, HostListener, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, switchMap, tap} from 'rxjs/operators';
import {ModalComponent} from '../modal/modal.component';
import {ChooseAddressComponentService} from './choose-address.component.service';
import {AddressDict, DwellingPlace} from '../../models';
import {DictService} from '../../services/dict.service';
import {cls, getAddress, hlNum, NOT_EXISTS, psNum, russiaId, urls} from '../../utils';
import {nowStr} from '../../functions';
import {ShareSearchService} from '../../services/share-search.service';

@Component({
    selector: 'fi-choose-address',
    templateUrl: './choose-address.component.html',
    styleUrls: ['./choose-address.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ChooseAddressComponent implements OnInit {
    @ViewChild('mc') mc: ModalComponent;
    @ViewChild('searchIn') searchIn: ElementRef;
    stayType = 'STAY_PLACE_REG';
    cls = cls;
    cnf$ = this.rpcs.config$;
    address = {levels: []} as DwellingPlace;
    models = [] as { v: string }[];
    disabled = [] as number[];
    inputs = [] as Subject<string>[];
    items = [] as Observable<AddressDict[]>[];
    lastCountries: any[];

    cInput$ = new Subject<string>();
    countries$ = this.typeAhead(this.cInput$, urls.api.dicts.cntrs).pipe(tap(res => {
        this.lastCountries = res;
        this.setCountry();
    }));
    fullAdrs$ = new BehaviorSubject<{ value: AddressDict[], label: string }[]>(null);
    regTypes$ = this.dictService.getDict(urls.api.dicts.rt);
    rentType$ = this.dictService.getDict(cls.rentType);
    arrivalCause$ = this.dictService.getDict(cls.arrivalCause);
    id = this.rpcs.id;

    constructor(private rpcs: ChooseAddressComponentService, private dictService: DictService,
                private sss: ShareSearchService) {
    }

    ngOnInit(): void {
        this.rpcs.address$.subscribe(address => {
            const a = {
                ...address, levels: address.levels.map(it => ({...it})).concat(
                    ((address.levels.length < 5) ?
                        [...Array(5 - address.levels.length)].map(() => ({} as AddressDict)) : [])
                )
            };
            this.disabled.length = this.models.length = this.inputs.length = this.items.length = 0;
            a.levels.forEach((it, i, arr) => {
                this.disabled.push((!it.nochg && (it.level || (i && arr[i - 1].level && +arr[i - 1].levelId < hlNum))) ? 0 : 1);
                this.models.push({v: it.value as string});
                this.inputs.push(new Subject<string>());
                this.items.push(this.typeAhead(this.inputs[i], urls.api.dicts.lvls, i));
            });
            if (a.countryId === russiaId && !a.levels[0].nochg) {
                this.disabled[0] = 0;
            }
            this.address = a;
            setTimeout(() => {
                if (a.countryId) {
                    this.cInput$.next(a.countryId);
                }
                a.levels.forEach((it, i) => {
                    if (it.value) {
                        this.inputs[i].next(it.label as string);
                    }
                });
            });
        });
    }

    @HostListener('click')
    outClick() {
        if (this.fullAdrs$.getValue()) {
            this.fullAdrs$.next(null);
        }
    }

    /**
     * @param checkAdr - нужно ли проверять адрес на принадлежность к орагнизации
     */
    done(checkAdr?: boolean) {
        if (checkAdr) {
            const levels = this.address.levels;
            let index = levels.findIndex(it => !it.value);
            if (index === -1) {
                index = levels.length;
            }
            this.sss.isMyAddress(levels[index - 1].value as string).subscribe(res => {
                if (res) {
                    this.sendAndClose();
                }
            });
        } else {
            this.sendAndClose();
        }
    }

    onClose() {
        this.rpcs.done(null);
    }

    clear() {
        this.searchIn.nativeElement.value = '';
    }

    search() {
        const levels = this.address.levels;
        let level = levels[0];
        for (let i = 1; i < levels.length; ++i) {
            if (levels[i].value) {
                level = levels[i];
            } else {
                break;
            }
        }
        this.dictService.loadOnce(urls.api.dicts.lvls, {
            paid: (level && level.value) || '',
            level: (level && level.levelId) || null,
            all: true,
            query: this.searchIn.nativeElement.value
        }).subscribe((res: any) => {
            this.fullAdrs$.next(res.map(it => ({
                value: it,
                label: getAddress(it)
            })));
        });
    }

    changed(data: any, i: number, level: AddressDict) {
        if (data) {
            for (const k in data) {
                if (data.hasOwnProperty(k)) {
                    level[k] = data[k];
                }
            }
            const nextIndex = i + 1;
            if (this.address.levels[nextIndex] && (+level.levelId <= hlNum || +level.levelId === psNum)) {
                this.clearLevels(nextIndex);
            }
        } else {
            this.clearLevels(i);
        }
    }

    countryChanged(dict: any) {
        this.disabled = this.disabled.map(it => 1);
        if (dict) {
            this.address.country = dict.label;
        } else {
            this.address.country = '';
        }
        this.clearLevels(0, true);
        if (dict && dict.value === russiaId) {
            this.disabled[0] = 0;
        }
    }

    /**
     * При смене типа адреса, может понадобится обнулить дату окончания
     */
    regTypeChanged(data: any) {
        if (data) {
            this.address.regType = data.value;
            this.address.regTypeStr = data.label;
        }
        if (this.address.regType !== this.stayType) {
            this.address.endDate = null;
        }
    }

    /**
     * Устанавнливает дату начала регистрации текущей датой
     */
    setRegStartDate() {
        this.address.startDate = nowStr();
    }

    /**
     * Устанавливает дату окончания регистрации годом позже даты начала регистрации
     */
    setRegEndDate() {
        if (this.address.startDate) {
            const parts = this.address.startDate.split('.');
            parts[parts.length - 1] = '' + (parseInt(parts[parts.length - 1], 10) + 1);
            this.address.endDate = parts.join('.');
        }
    }

    /**
     * Проставляет уровни ардеса при выборе из списка найденых адресов по всем дочерим
     * @param levels - уровни адреса
     */
    setLevels(event: Event, levels: AddressDict[]) {
        event.stopPropagation();
        const idx = this.address.levels.findIndex(it => !it.value);
        if (idx > -1) {
            this.address.countryId = russiaId;
            this.setCountry();
            this.address.levels.splice(idx, levels.length, ...levels);
            this.disabled.length = 0;
            this.address.levels.forEach((it, i, arr) => {
                this.disabled.push((!it.nochg && (it.level || (i && arr[i - 1].level && (+arr[i - 1].levelId < hlNum
                    || +arr[i - 1].levelId === psNum)))) ? 0 : 1);
                this.models[i].v = it.value as string;
            });
            setTimeout(() => {
                    this.cInput$.next(russiaId);
                    this.address.levels.slice(idx).forEach((it, i) => {
                        if (it.value) {
                            this.inputs[idx + i].next(it.label);
                        }
                    });
                }
            );
        }
        this.fullAdrs$.next(null);
    }

    /**
     * Можно нажимать кнопку только если все обязательные поля заполнены
     * @param typeId - тип регистрации
     * @param ground - если true, то это ЛД из АИС Регистрация населения
     */
    dis(typeId: string, ground?: boolean) {
        let disabled = !!typeId;
        if (disabled) {
            const address = this.address;
            disabled = !(address.countryId && address.startDate);
            if (typeId === cls.radr) {
                disabled = disabled || !address.regType;
                if (ground) {
                    disabled = disabled || !(address.arrivalCauseId && address.lifeGroundId) ||
                        (address.regType === this.stayType && !address.endDate)
                        || (address.lifeGroundId === cls.owner && !(address.ownerDate && address.numerator && address.denominator)
                            || !this.address.levels[0].value);
                }
            }
        }
        return disabled;
    }

    /**
     * Подписывает изменения ввода адреса на подтягивание значений справочников
     * @param input - объект ввода
     * @param dict - название справочника
     * @param idx - индекс для получения идентификатора родителя
     */
    private typeAhead(input: Subject<string>, dict, idx = 0): Observable<AddressDict[]> {
        return input.pipe(
            filter(it => it != null),
            debounceTime(200),
            distinctUntilChanged(),
            switchMap(query => query === NOT_EXISTS ? of([] as AddressDict[]) :
                this.dictService.loadOnce(dict, dict === urls.api.dicts.lvls ? {
                    paid: idx ? (this.address.levels[idx - 1].value || '') : '',
                    level: idx ? (this.address.levels[idx - 1].levelId || null) : null,
                    query
                } : query) as Observable<AddressDict[]>
            )
        );
    }

    private clearLevels(start: number, all = false) {
        const levels = this.address.levels;
        for (let j = start; j < levels.length; ++j) {
            const l = levels[j];
            for (const k in l) {
                if (l.hasOwnProperty(k)) {
                    delete l[k];
                }
            }
            this.models[j].v = null;
            this.inputs[j].next(NOT_EXISTS);
            if (all || j > start) {
                this.disabled[j] = 1;
            } else {
                this.disabled[j] = 0;
            }
        }
    }

    liveGroundChanged() {
        this.address.ownerDate = undefined;
        this.address.numerator = undefined;
        this.address.denominator = undefined;
    }

    private sendAndClose() {
        this.rpcs.done({
            ...this.address, addressText: this.address.addressText && this.address.addressText.trim(),
            levels: this.address.levels.filter(it => it.value)
        });
        this.mc.close();
    }

    private setCountry() {
        if (this.address.countryId && this.lastCountries) {
            const item = this.lastCountries.find(it => it.value === this.address.countryId);
            if (item) {
                this.address.country = item.label;
            }
        }
    }

}

