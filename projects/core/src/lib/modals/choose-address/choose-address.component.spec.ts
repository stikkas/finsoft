import {DwellingPlace} from '../../models';

const cls = {radr: 'radr'} as any;
let address = {} as DwellingPlace;

function oldDis(typeId: string) {
    return !!typeId && (typeId === cls.radr ?
        !(address.countryId && address.startDate && address.regType)
        : !(address.countryId && address.startDate));
}

function dis(typeId: string, arrivalCause?: boolean) {
    let disabled = !!typeId;
    if (disabled) {
        disabled = !(address.countryId && address.startDate);
        if (typeId === cls.radr) {
            disabled = disabled || !address.regType;
            if (arrivalCause) {
                disabled = disabled || !(address.arrivalCauseId);
            }
        }
    }
    return disabled;
}

describe('[ChooseAddressComponent:dis]', () => {
    // let component: XXXComponent;
    // let fixture: ComponentFixture<XXXComponent>;
    beforeEach(() => {
        address = {} as DwellingPlace;
    });
    // beforeEach(async(() => {
    //   TestBed.configureTestingModule({
    //     declarations: [ XXXComponent ]
    //   })
    //   .compileComponents();
    // }));

    // beforeEach(() => {
    //   fixture = TestBed.createComponent(XXXComponent);
    //   component = fixture.componentInstance;
    //   fixture.detectChanges();
    // });

    it('without typeId', () => {
        const res = oldDis(undefined);
        expect(res).toEqual(false);
        expect(res).toEqual(dis(undefined));
    });

    it('without typeId and countryId', () => {
        address.countryId = 'couotry';
        const res = oldDis(undefined);
        expect(res).toEqual(false);
        expect(res).toEqual(dis(undefined));
    });

    it('without typeId and countryId and startDate', () => {
        address.countryId = 'couotry';
        address.startDate = 'couotry';
        const res = oldDis(undefined);
        expect(res).toEqual(false);
        expect(res).toEqual(dis(undefined));
    });

    it('without typeId and countryId and startDate and regType', () => {
        address.countryId = 'couotry';
        address.startDate = 'couotry';
        address.regType = 'couotry';
        const res = oldDis(undefined);
        expect(res).toEqual(false);
        expect(res).toEqual(dis(undefined));
    });

    it('with "radr" as typeId', () => {
        const res = oldDis('radr');
        expect(res).toEqual(true);
        expect(oldDis('radr')).toEqual(dis('radr'));
    });
    it('with "radr" as typeId and countryId', () => {
        address.countryId = 'couotry';
        const res = oldDis('radr');
        expect(res).toEqual(true);
        expect(res).toEqual(dis('radr'));
    });
    it('with "radr" as typeId and countryId and startDate', () => {
        address.countryId = 'couotry';
        address.startDate = 'couotry';
        const res = oldDis('radr');
        expect(res).toEqual(true);
        expect(res).toEqual(dis('radr'));
    });
    it('with "radr" as typeId and countryId and startDate and regType', () => {
        address.countryId = 'couotry';
        address.startDate = 'couotry';
        address.regType = 'couotry';
        const res = oldDis('radr');
        expect(res).toEqual(false);
        expect(res).toEqual(dis('radr'));
    });

    it('with not "radr" as typeId', () => {
        const res = oldDis('noradr');
        expect(res).toEqual(true);
        expect(res).toEqual(dis('noradr'));
    });
    it('with not "radr" as typeId and countryId', () => {
        address.countryId = 'couotry';
        const res = oldDis('noradr');
        expect(res).toEqual(true);
        expect(res).toEqual(dis('noradr'));
    });
    it('with not "radr" as typeId and countryId and startDate', () => {
        address.countryId = 'couotry';
        address.startDate = 'couotry';
        const res = oldDis('noradr');
        expect(res).toEqual(false);
        expect(res).toEqual(dis('noradr'));
    });
    it('with not "radr" as typeId and countryId and startDate and regType', () => {
        address.countryId = 'couotry';
        address.startDate = 'couotry';
        address.regType = 'couotry';
        const res = oldDis('noradr');
        expect(res).toEqual(false);
        expect(res).toEqual(dis('noradr'));
    });
});
