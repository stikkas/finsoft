import {ChangeDetectionStrategy, Component, ViewChild, ViewEncapsulation} from '@angular/core';
import {ModalComponent} from '../modal/modal.component';
import {ConfirmRemoveComponentService} from './confirm-remove.component.service';

@Component({
    selector: 'fi-confirm-remove',
    templateUrl: './confirm-remove.component.html',
    styleUrls: ['./confirm-remove.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmRemoveComponent {
    @ViewChild('mc') mc: ModalComponent;
    id = this.crcs.id;

    constructor(private crcs: ConfirmRemoveComponentService) {
    }

    da() {
        this.crcs.done(true);
        this.mc.close();
    }

    onClose() {
        this.crcs.done(false);
    }
}

