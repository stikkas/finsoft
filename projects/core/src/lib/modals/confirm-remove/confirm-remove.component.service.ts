import {Injectable} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Observable, of, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ConfirmRemoveComponentService {
    private subject = new Subject<boolean>();
    readonly id = 'ModalId.ConfirmRemove';

    constructor(private sms: NgxSmartModalService) {
    }

    open(text?: string | Observable<string>): Observable<boolean> {
        this.sms.getModal(this.id).setData((typeof text === 'string') ? of(text) : text, true).open();
        return this.subject.asObservable();
    }

    done(res: boolean) {
        this.subject.next(res);
    }
}

