import {Injectable} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {Observable, Subject} from 'rxjs';

interface OrgData {
    id: string;
    value: string;
    type: string;
    typeId: string;
    print: string;
}

@Injectable({
    providedIn: 'root'
})
export class OrgSearchComponentService {
    private subject = new Subject<OrgData>();
    private data = new Subject<{
        catId?: number | number[], lo: boolean, noAdd: boolean, parentId: string,
        disType: boolean, disAdr: boolean, ais: boolean
    }>();
    readonly id = 'ModalId.OrgsSearch';

    constructor(private sms: NgxSmartModalService) {
    }

    open(catId?: number | number[], lo?: boolean, noAdd?: boolean, parentId?: string, disType = false, disAdr = false,
         ais = false):
        Observable<OrgData> {
        this.data.next({catId, lo, noAdd, parentId, disType, disAdr, ais});
        this.sms.getModal(this.id).open();
        return this.subject.asObservable();
    }

    done(org: OrgData) {
        this.subject.next(org);
    }

    get data$() {
        return this.data.asObservable();
    }
}

