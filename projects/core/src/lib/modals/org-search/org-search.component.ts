import {Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {OrgSearchComponentService} from './org-search.component.service';
import {AdminSearchService} from '../../services/admin-search.service';
import {MeService} from '../../services/me.service';
import {ModalComponent} from '../modal/modal.component';
import {ClsSearchComponentService} from '../cls-search/cls-search.component.service';
import {ChooseAddressComponentService} from '../choose-address/choose-address.component.service';
import {AddressDict, OpekaRole} from '../../models';
import {getAddress, onDone, russiaId} from '../../utils';
import {ITreeOptions, TreeNode} from '@circlon/angular-tree-component';
import {skipWhile, take} from 'rxjs/operators';

@Component({
    selector: 'fi-org-search',
    templateUrl: './org-search.component.html',
    styleUrls: ['./org-search.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class OrgSearchComponent implements OnInit {
    /**
     * Можно выбирать только листья дерева, папки нельзя
     * @private
     */
    private leavesOnly: boolean;
    private parentId: string;
    /**
     * Ищем только организации с меткой "АИС"
     * @private
     */
    private ais: boolean;
    id = this.oscs.id;
    ga = getAddress;
    disType: boolean;
    disAdr: boolean;

    @Input() adminUi: string;
    @ViewChild('mc') mc: ModalComponent;
    @ViewChild('tree') tree;
    @ViewChild('input') input: ElementRef;

    current = false;
    catId: number | number[]; // 8 - ООП
    orgType: string;
    orgTypeId: string;
    data: any[];
    address: AddressDict[];

    treeOptions: ITreeOptions = {
        displayField: 'value', idField: 'id',
        scrollOnActivate: false,
        actionMapping: {
            mouse: {
                dblClick: (tree, node) => this.ok(node)
            }
        },
        getChildren: (node: TreeNode) => this.search(undefined, node.id).toPromise()
    };
    isSpec = false;
    noAdd = false;

    constructor(private oscs: OrgSearchComponentService, private ss: AdminSearchService, me: MeService,
                private cacs: ChooseAddressComponentService, private clscs: ClsSearchComponentService) {
        me.getUser().pipe(skipWhile(it => !it), take(1)).subscribe(user => {
            this.isSpec = user.roleId === OpekaRole.Specialist || user.roleId === OpekaRole.MainSpecialist;
        });
    }

    ngOnInit() {
        this.oscs.data$.subscribe(({catId, lo, noAdd, parentId, disType, disAdr, ais}) => {
            this.catId = catId;
            this.leavesOnly = lo;
            this.noAdd = noAdd;
            this.parentId = parentId;
            this.disType = disType;
            this.disAdr = disAdr;
            this.ais = ais;
            this.ss.getRootAddress().subscribe(res => {
                this.address = res && res.levels;
                this.update(this.input.nativeElement.value);
            });
        });
    }

    search(name: string, parentId?: string) {
        if (parentId !== undefined) {
            const params: any = this.catId && Array.isArray(this.catId) ? {catIds: this.catId} : {catId: this.catId};
            if (this.ais) {
                params.ais = this.ais;
            }
            return this.ss.findOrganisations(undefined, parentId, params);
        } else {
            this.update(name);
        }
    }

    ok(node?: TreeNode) {
        const data = (node || this.tree.treeModel.getActiveNode()).data;
        if ((data.ais || !this.ais) && !(this.leavesOnly && (data.children || data.hasChildren))) {
            this.oscs.done({id: data.id, value: data.value, type: data.typeName, typeId: data.typeId, print: data.print});
            this.mc.close();
        }
    }

    onClose() {
        const node = this.tree.treeModel.getActiveNode();
        if (node) {
            node.setIsActive(false);
            node.blur();
        }
        if (this.tree.treeModel.roots) {
            this.tree.treeModel.collapseAll();
        }
        this.data = null;
        this.clear(true);
        this.oscs.done(null);
    }

    clear(all = false) {
        this.input.nativeElement.value = '';
        if (all || !this.disType) {
            this.orgType = this.orgTypeId = undefined;
        }
        if (all || !this.disAdr) {
            this.address = undefined;
        }
    }

    doneDis() {
        if (!this.current) {
            return true;
        }
        const cur = this.tree.treeModel.getActiveNode();
        if (this.ais && !(cur && cur.data.ais)) {
            return true;
        }
        return this.leavesOnly && (cur && (cur.data.children || cur.data.hasChildren));
    }

    setAddress() {
        onDone(this.cacs.open('Выбор адреса', {levels: this.address || [], countryId: russiaId},
            false, true, false, true, true), res => this.address = res.levels);
    }

    setType() {
        onDone(this.clscs.open('Поиск типа учреждения', '11420f37-8288-492a-8963-3924ac116570'), ({id, value}) => {
            this.orgTypeId = id;
            this.orgType = value;
        });
    }

    clearOrgType() {
        this.orgTypeId = this.orgType = undefined;
    }

    private update(name: string) {
        const level = this.address && this.address[this.address.length - 1];
        const params = {addressId: level && (level.value as string), typeId: this.orgTypeId} as any;
        if (this.catId) {
            if (Array.isArray(this.catId)) {
                params.catIds = this.catId;
            } else {
                params.catId = this.catId;
            }
        }
        if (this.parentId) {
            params.parentIds = [this.parentId];
        }
        if (this.ais) {
            params.ais = this.ais;
        }
        this.ss.findOrganisations(name, undefined, params).subscribe(res => {
            this.data = res;
            if (res && res.length) {
                setTimeout(() => this.tree.treeModel.expandAll(), 300);
            }
        });
    }
}

