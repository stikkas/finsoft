import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ModalComponent} from '../modal/modal.component';
import {ConfirmSaveComponentService} from './confirm-save.component.service';
import {Subject} from 'rxjs';

@Component({
    selector: 'fi-confirm-save',
    templateUrl: './confirm-save.component.html',
    styleUrls: ['./confirm-save.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ConfirmSaveComponent implements OnInit {
    private subject$: Subject<boolean>;
    id = this.cscs.id;
    @ViewChild('mc') mc: ModalComponent;
    msg: string;
    ok: string;
    no: string;

    constructor(private cscs: ConfirmSaveComponentService) {
    }

    ngOnInit() {
        this.cscs.cfg$.subscribe(({message, ok, no, subject}) => {
            this.msg = message || 'Сохранить изменения?';
            this.ok = ok || 'Сохранить';
            this.no = no || 'Нет';
            this.subject$ = subject;
        });
    }

    done(ok = false) {
        this.subject$.next(ok);
        this.mc.close();
    }

    onClose() {
        this.subject$.next(null);
    }
}

