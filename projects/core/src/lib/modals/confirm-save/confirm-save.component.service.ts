import {Injectable} from '@angular/core';
import {NgxSmartModalComponent, NgxSmartModalService} from 'ngx-smart-modal';
import {Observable, Subject} from 'rxjs';
import {take} from 'rxjs/operators';

export type Config = { message: string, ok: string, no: string, subject: Subject<boolean> };

@Injectable({
    providedIn: 'root'
})
export class ConfirmSaveComponentService {
    private cfg = new Subject<Config>();
    readonly id = 'ModalId.ConfirmSave';

    constructor(private sms: NgxSmartModalService) {
    }

    open(message?: string, ok?: string, close = true, no = ''): Observable<boolean> {
        const subject = new Subject<boolean>();
        const modal = this.sms.getModal(this.id);
        const config = {message, ok, no, subject};
        if (modal.isVisible()) { // попытка открыть повторно
            modal.onCloseFinished.pipe(take(1)).subscribe(() => this.realOpen(modal, config));
        } else {
            this.realOpen(modal, config);
        }
        return subject.asObservable();
    }

    get cfg$() {
        return this.cfg.asObservable();
    }

    private realOpen(modal: NgxSmartModalComponent, config: Config) {
        this.cfg.next(config);
        modal.open();
    }
}

