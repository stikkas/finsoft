import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ModalComponent} from '../modal/modal.component';
import {LdFilter, LdTableRow, SortEvent} from '../../models';
import {SearchResultComponent} from '../../components/search-result/search-result.component';
import {PeopleSearchComponentService} from './people-search.component.service';
import {ShareSearchService} from '../../services/share-search.service';
import {context} from '../../utils';
import {ConfirmInfoComponentService} from '../confirm-info/confirm-info.component.service';

@Component({
    selector: 'fi-people-search',
    templateUrl: './people-search.component.html',
    styleUrls: ['./people-search.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class PeopleSearchComponent implements OnInit {
    private searchKey = 'PeopleSearchComponent';
    private sort: SortEvent;
    private page: number;
    private ctx: string;
    private initFilter: LdFilter;
    request = new LdFilter();

    @ViewChild('mc') mc: ModalComponent;
    @ViewChild(SearchResultComponent) resultTable;

    page$ = this.ss.getPeople(this.searchKey);
    current: LdTableRow;
    // если открыли с главного окна, то true
    fromMain: boolean;
    // Когда не нужна кнопка создать
    nocreate: boolean;
    id = this.pscs.id;
    withRelation: boolean;

    constructor(private ss: ShareSearchService, private cics: ConfirmInfoComponentService,
                private pscs: PeopleSearchComponentService) {
        this.ctx = context.aisUrl || context.app;
    }

    search(event?: SortEvent | number) {
        if (!event) {
            this.page = 1;
            this.sort = null;
            this.resultTable.clearSorts();
        } else if (typeof event === 'number') {
            this.page = event;
        } else {
            this.sort = event;
        }
        this.ss.findPeople(this.request, this.searchKey, this.page, this.sort);
    }

    accept(p?: LdTableRow) {
        const data = p || this.current;
        if (this.withRelation && !(this.request.relativeId || this.request.relationId)) {
            this.cics.open('Необходимо выбрать родственное отношение.');
        } else {
            if (this.fromMain) {
                window.open(`${window.location.origin}${this.ctx}/pf/${data.id}/man-info`, '_blank');
            } else {
                this.pscs.done({...data, role: (this.request.relativeId || this.request.relationId) as any});
                this.mc.close();
            }
        }
    }

    ngOnInit(): void {
        this.pscs.filter$.subscribe(res => {
            this.initFilter = res && res.f;
            this.request = (res && res.f && res.f.copy()) || new LdFilter();
            if (this.request.notEmpty()) {
                this.search();
            }
            this.fromMain = res && res.m;
            this.nocreate = res && res.n;
            this.withRelation = res && res.withRelation;
        });
    }

    actualChanged(act: boolean) {
        this.request.actual = act;
        this.search(this.page);
    }

    onClose() {
        this.pscs.done(null);
        this.resultTable.clear();
        this.ss.clear(this.searchKey);
        this.current = null;
    }

    onClear() {
        if (this.initFilter) {
            const newFilter = this.initFilter.copy();
            delete newFilter.birthDate;
            delete newFilter.lastName;
            delete newFilter.firstName;
            delete newFilter.middleName;
            delete newFilter.docNumber;
            delete newFilter.snils;
            this.request = newFilter;
        } else {
            this.request = new LdFilter();
        }
    }
}
