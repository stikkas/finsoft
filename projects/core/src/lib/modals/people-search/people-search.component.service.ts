import {Injectable} from '@angular/core';
import {NgxSmartModalService} from 'ngx-smart-modal';
import {LdFilter, Relation} from '../../models';
import {Observable, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PeopleSearchComponentService {
    private subject = new Subject<Relation>();
    filter$ = new Subject<any>();
    readonly id = 'ModalId.PeopleSearch';

    constructor(private sms: NgxSmartModalService) {
    }

    open(filter?: LdFilter, main = false, nocreate = false, withRelation = false): Observable<Relation> {
        this.filter$.next({f: filter && filter.copy(), m: main, n: nocreate, withRelation});
        this.sms.get(this.id).open();
        return this.subject.asObservable();
    }


    done(p: Relation) {
        this.subject.next(p);
    }
}

