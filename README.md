## Finsoft

Коллекция различных элементов для разных АИС, например, "АИС Опека", или "АИС Администратор".

## Использование в проекте

Для использования в проекте скомпилированной библиотеки достаточно прописать в *packages.json* своего проекта:

```json
"dependencies": {
....
"@insoft/core": "git@gitlab.com:insoft_employeers/finsoft-core.git#0.1.15"
...
}
```

, где версию можно посмотреть в `projects/core/package.json`

после чего нужно выполнить команду:

```shell
npm install
```

## Создание репозитория готового пакета

Необходимо клонировать два репозитория таким образом (название коневой папки можно выбирать любое):

```shell
mkdir our_dir
cd our_dir
mkdir finsoft-dist
cd finsoft-dist 
git clone git@gitlab.com:insoft_employeers/finsoft-core.git core
cd ..
git clone git@gitlab.com:insoft_employeers/finsoft.git
cp finsoft/scripts/set-version.sh finsoft-dist 
```

В результате должна получиться такая картина:

```shell
|__ our_dir
    |__ finsoft
    |__ finsoft-dist
        |__ core
        |__ set-version.sh
```

Перед сборкой пакету нужно поменять версию в `projects/core/package.json`.

Пакет собирается командой:

```bash
npm run build
```

Теперь необходимо его выложить в репозиторий, для этого, находясь в директории `finsoft-dist`, нужно выполнить команду

```shell
bash set-version.sh core
```

Все, после этого можно в своем проекте делать:

```bash
npm i
```

## Разработка

Для разработки можно пользоваться только этим репозиторием. Нужно выполнить команды:

```bash
npm run develop
npm run build:css
```

вторую команду нужно выполнить в отдельном терминале. ее нужно выполнять каждый раз когда изменяются стили в папке
`projects/core/styles`. затем из директории `dist/core`:

```bash
npm link
```

Теперь в проекте, которому нужна эта библиотека, в папке node_modules:

```bash
npm link @insoft/core
```

В `package.json` своего проекта ничего прописывать не надо.
Нужно только поменять `angular.json`:
```json
    "projects": {
        "bddsop": {
           ... 
            "architect": {
                "build": {
                    ....
                    "options": {
                      ... 
                        "preserveSymlinks": true,
...
```
где `bddsop` - название проекта
