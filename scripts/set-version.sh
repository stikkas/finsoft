#!/bin/bash

if [ "$1" = "" ]; then
    echo "Usage $0 package_name"
    exit 1
fi

cd $1
version=`grep version package.json|cut -d':' -f2|cut -d'"' -f2`
git add .
git commit -m 'new version'
git tag "$version" -m "version $version" 
git push --tags 


